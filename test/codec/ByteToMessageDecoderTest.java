package codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;

import com.me.netty.core.codec.MessageDecoder;
import com.me.netty.core.codec.MessageEncoder;
import com.me.netty.core.socket.MessageHandler;

/**
 * Embeadded test
 * 
 * @author lamhm
 *
 */
public class ByteToMessageDecoderTest {

	@Test
	public void testRemoveItself() {
		PropertyConfigurator.configure("configs/log4j.properties");
		EmbeddedChannel channel = new EmbeddedChannel(new MessageDecoder(), new MessageEncoder(), new MessageHandler());
		long current = System.currentTimeMillis();
		for (int i = 1; i < 500000; i++) {
			ByteBuf buf = Unpooled.buffer(4);
			buf.writeBytes(convertInteger2Bytes(i));
			channel.writeInbound(buf);
		}
		System.out.println(System.currentTimeMillis() - current);
	}


	public static Integer convertBytes2Integer(byte... data) {
		return new Integer(((data[0] & 0xFF) << 24) + ((data[1] & 0xFF) << 16) + ((data[2] & 0xFF) << 8) + ((data[3] & 0xFF) << 0));
	}


	public static byte[] convertInteger2Bytes(Integer value) {
		int v = value.intValue();
		return new byte[] { (byte) (v >>> 24 & 0xFF), (byte) (v >>> 16 & 0xFF), (byte) (v >>> 8 & 0xFF), (byte) (v >>> 0 & 0xFF) };
	}
}
