package com.me.services;

import java.util.List;

import javax.cache.Cache.Entry;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.cache.query.TextQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.junit.BeforeClass;
import org.junit.Test;

import com.me.model.cluster.ClusterRoom;

/**
 * Class test các lệnh query
 * 
 * @author lamhm
 *
 */
public class SQLQueryTest {
	private static Ignite grid;
	private static final String ROOM_CACHE_NAME = SQLQueryTest.class.getSimpleName() + "-room";


	@BeforeClass
	public static void init() {
		grid = Ignition.start("configs/gridgain.xml");
		CacheConfiguration<String, ClusterRoom> orgCacheCfg = new CacheConfiguration<>(ROOM_CACHE_NAME);

		orgCacheCfg.setCacheMode(CacheMode.REPLICATED);
		orgCacheCfg.setIndexedTypes(String.class, ClusterRoom.class);

		IgniteCache<String, ClusterRoom> roomCache = grid.createCache(orgCacheCfg);

		// init
		roomCache = Ignition.ignite().cache(ROOM_CACHE_NAME);
		// xóa trước khi start
		roomCache.clear();

		for (int i = 0; i < 100000; i++) {
			ClusterRoom room = new ClusterRoom("roomTest");
			room.setClusterId("clusterRoom" + i);
			room.setId(i);
			if (i % 2 == 0) {
				room.setOwner("lamha" + i);
			} else {
				room.setOwner("thuypm" + i);
			}
			String ipAddress = "192.168.1.1:8080";
			room.setIpAddress(ipAddress);
			room.setPassword("pass1");
			room.putProperty("attr1", "attr1Value");
			room.putProperty("attr2", "attr2Value");
			roomCache.put(room.getClusterId(), room);
		}
	}


	@Test
	public void testSqlLikeQuery() {
		IgniteCache<String, ClusterRoom> roomCache = Ignition.ignite().cache(ROOM_CACHE_NAME);
		SqlQuery<String, ClusterRoom> qry = new SqlQuery<>(ClusterRoom.class, "id > ? AND owner LIKE ? OR name LIKE ? LIMIT 10 OFFSET 0");
		List<Entry<String, ClusterRoom>> all = roomCache.query(qry.setArgs(1000, "%lam%", "%lam%")).getAll();

		for (Entry<String, ClusterRoom> entry : all) {
			ClusterRoom clusterRoom = entry.getValue();
			System.out.println("[ERROR]" + clusterRoom.getClusterId() + "/" + clusterRoom.getOwner() + "/pass:" + clusterRoom.getPassword());
		}

		System.out.println("--------------------------- AFTER REMOVE ----------------------");
		roomCache.remove("clusterRoom1010");
		qry = new SqlQuery<>(ClusterRoom.class, "id > ? AND owner LIKE ? LIMIT 10 OFFSET 0");
		all = roomCache.query(qry.setArgs(1000, "%lam%")).getAll();

		for (Entry<String, ClusterRoom> entry : all) {
			ClusterRoom clusterRoom = entry.getValue();
			System.out.println("[ERROR]" + clusterRoom.getClusterId() + "/" + clusterRoom.getOwner() + "/pass:" + clusterRoom.getPassword());
		}
	}

	@Test
	public void testTextQuery() {
		IgniteCache<String, ClusterRoom> roomCache = Ignition.ignite().cache(ROOM_CACHE_NAME);
		TextQuery<String, ClusterRoom> textQry = new TextQuery<>(ClusterRoom.class, "lamha1008");
		List<Entry<String, ClusterRoom>> all2 = roomCache.query(textQry).getAll();
		System.out.println("[FATAL] " + all2.size());
	}

}
