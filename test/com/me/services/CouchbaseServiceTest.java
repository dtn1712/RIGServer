package com.me.services;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.me.model.ServerConfig;
import com.me.netty.core.util.Utility;

/**
 * @author lamhm
 *
 */
public class CouchbaseServiceTest {
	private static CouchbaseService couchbaseService;


	@BeforeClass
	public static void init() {
		PropertyConfigurator.configure("configs/log4j.properties");
		ServerConfig.init(Utility.loadConfigFile("configs/serverconfig.properties"));
		couchbaseService = new CouchbaseService();
	}


	@Test
	public void readAndWriteTest() {
		String token = "lamha_accesstoken";
		int moboId = 1;
		couchbaseService.saveToken(token, moboId);
		try {
			int expMoboId = couchbaseService.getMoboIdByToken(token);
			Assert.assertEquals(expMoboId, moboId);
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Integer moboIdExp = couchbaseService.getMoboIdByToken(token);
		Assert.assertNull(moboIdExp);
		boolean existToken = couchbaseService.existToken(token);
		Assert.assertFalse(existToken);
	}
	
}
