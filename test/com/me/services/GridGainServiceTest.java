package com.me.services;

import java.util.Collection;
import java.util.Properties;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.me.model.ServerConfig;
import com.me.model.cluster.ClusterRoom;
import com.me.model.cluster.ClusterUser;
import com.me.netty.core.util.Utility;

/**
 * @author lamhm
 *
 */
public class GridGainServiceTest {
	private static IgniteService gridGainService;


	@BeforeClass
	public static void init() {
		Properties prop = Utility.loadConfigFile("configs/serverconfig.properties");
		ServerConfig.init(prop);
		gridGainService = new IgniteService();
	}


	@Test
	public void readAndWriteTest() {
		ClusterRoom room = new ClusterRoom("roomTest");
		room.setClusterId("clusterRoom1");
		int roomId = 100;
		room.setId(roomId);
		String ipAddress = "192.168.1.1:8080";
		room.setIpAddress(ipAddress);
		room.setPassword("pass");
		room.putProperty("attr1", "attr1Value");
		room.putProperty("attr2", "attr2Value");
		gridGainService.saveRoom(room);

		ClusterRoom room2 = gridGainService.getRoom(room.getClusterId());
		Assert.assertNotNull(room2);
		Assert.assertEquals(room2.getId(), roomId);
		Assert.assertEquals(room2.getIpAddress(), ipAddress);
		Assert.assertEquals(room2.getProperty("attr1"), "attr1Value");
		Assert.assertEquals(room2.getProperty("attr2"), "attr2Value");
	}


	public void testCacheLoader() {
		ClusterUser user = gridGainService.getUser(100);
		Assert.assertNull(user);
	}


	public void benchmark() {
		for (int i = 0; i < 100000; i++) {
			ClusterRoom room = new ClusterRoom("roomTest");
			room.setClusterId("clusterRoom" + i);
			int roomId = 100;
			room.setId(roomId);
			String ipAddress = "192.168.1.1:8080";
			room.setIpAddress(ipAddress);
			room.setPassword("pass");
			room.putProperty("attr1", "attr1Value");
			room.putProperty("attr2", "attr2Value");
			gridGainService.saveRoom(room);
		}

		long currentTimeMillis = System.currentTimeMillis();
		for (int i = 0; i < 100000; i++) {
			gridGainService.getRoom("clusterRoom" + i);
		}

		System.out.println(System.currentTimeMillis() - currentTimeMillis);
	}


	public void testSQLQuery() {
		for (int i = 0; i < 100000; i++) {
			ClusterRoom room = new ClusterRoom("roomTest");
			room.setClusterId("clusterRoom" + i);
			room.setId(i);
			if (i % 2 == 0) {
				room.setOwner("lamha" + i);
			} else {
				room.setOwner("thuypm" + i);
			}
			String ipAddress = "192.168.1.1:8080";
			room.setIpAddress(ipAddress);
			room.setPassword("pass1");
			room.putProperty("attr1", "attr1Value");
			room.putProperty("attr2", "attr2Value");
			gridGainService.saveRoom(room);
		}

		Collection<ClusterRoom> filterRoom = gridGainService.filterRoom(100);
		for (ClusterRoom clusterRoom : filterRoom) {
			System.out.println("[ERROR]" + clusterRoom.getOwner());
		}
	}
}
