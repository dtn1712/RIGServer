package com.me.netty;

import java.io.DataInputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Date;

import com.me.netty.core.util.ConverterUtil;
import com.me.netty.core.util.IOUtils;

public class JavaSocketClientTest {
	private static final String HOST = "10.8.15.126";
	private static final int PORT = 8080;


	public static void main(String[] args) {
		System.out.println("Creating socket to '" + HOST + "' on port " + PORT);
		try {
			Socket socket = new Socket(HOST, PORT);

			// send data
			OutputStream outputStream = socket.getOutputStream();
			byte[] messages = ConverterUtil.convertInteger2Bytes(123);
			outputStream.write(messages);
			outputStream.flush();

			DataInputStream inputStream = new DataInputStream(socket.getInputStream());
			Integer timeInt = ConverterUtil.convertBytes2Integer(IOUtils.toByteArray(inputStream));
			System.out.println(new Date((readUnsignedInt(timeInt) - 2208988800L) * 1000L));

			// outputStream.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private static long readUnsignedInt(int value) {
		return value & 0xFFFFFFFFL;
	}

}
