package com.rockitgaming.server.services;

import java.util.HashMap;
import java.util.Map;

import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.key.ErrorCode;
import com.rockitgaming.server.util.GsonUtils;

/**
 * @author lamhm
 *
 */
public class TokenFactory {
	/**
	 * Tạo message chuyển server.
	 * 
	 * @param user user chuyển server
	 * @param host host cần chuyển tới
	 * @param roomId room cần join vào
	 * @return
	 */
	public static Message createChangeServerMessage(User user, String host, int roomId) {
		Message errorMessage = DefaultMessageFactory.createErrorMessage(ErrorCode.CHANGE_SERVER, "Vui lòng chuyển server");
		errorMessage.putShort(IwinKey.KEYR_REQUEST_NEXT_CMD, SystemEventType.COMMAND_USER_CHANGE_SERVER);
		errorMessage.putString(SystemEventType.KEYS_IP_ADDRESS, host);

		CustomToken token = new CustomToken();
		// String device = user.getDevice().getClientDevice().getDevice();
		token.setDeviceId("deviceId");
		token.setUserId(user.getUserId());
		token.setGameId(user.getCurrentGameId());
		token.setRequestAction(SystemEventType.COMMAND_USER_JOIN_ROOM);
		Map<Object, Object> meta = new HashMap<Object, Object>();
		meta.put("roomId", roomId);
		token.setMetaData(meta);
		errorMessage.putString(SystemEventType.KEYS_ACCESS_TOKEN, GsonUtils.toGsonString(token));
		return errorMessage;
	}

	public static class CustomToken {
		private String deviceId;
		private int userId;
		private byte gameId;
		private short requestAction;
		private Map<Object, Object> metaData;


		public String getDeviceId() {
			return deviceId;
		}


		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}


		public int getUserId() {
			return userId;
		}


		public void setUserId(int userId) {
			this.userId = userId;
		}


		public byte getGameId() {
			return gameId;
		}


		public void setGameId(byte gameId) {
			this.gameId = gameId;
		}


		public short getRequestAction() {
			return requestAction;
		}


		public void setRequestAction(short requestAction) {
			this.requestAction = requestAction;
		}


		public Map<Object, Object> getMetaData() {
			return metaData;
		}


		public void setMetaData(Map<Object, Object> metaData) {
			this.metaData = metaData;
		}

	}
}
