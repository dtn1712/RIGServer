package com.rockitgaming.server.services;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.CouchbaseConnectionFactory;
import com.couchbase.client.CouchbaseConnectionFactoryBuilder;
import com.rockitgaming.server.model.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author lamhm
 *
 */
public class CouchbaseService {
	private static final Logger LOG = LoggerFactory.getLogger(CouchbaseService.class);
	private static final int TOKEN_SECOND_TTL = 86400;
	private CouchbaseClient couchbaseClient;


	public CouchbaseService() {
		try {
			LOG.info("---------------------- INIT COUCHBASE --------------------");
			CouchbaseConnectionFactoryBuilder cfb = new CouchbaseConnectionFactoryBuilder();
			cfb.setObsTimeout(ServerConfig.couchBaseOpTimeout);
			CouchbaseConnectionFactory connection = cfb.buildCouchbaseConnection(ServerConfig.cacheHosts, ServerConfig.bucketName, ServerConfig.cachePassword);
			couchbaseClient = new CouchbaseClient(connection);
			LOG.info("::::::::::::::::::::::::::::::::::::::::::::::::::::");
			LOG.info(">> COUCHBASE STARTED");
			LOG.info(String.format(">> [cacheHosts=%s, bucketName=%s]", ServerConfig.cacheHosts.toString(), ServerConfig.bucketName));
			LOG.info("::::::::::::::::::::::::::::::::::::::::::::::::::::");
		} catch (IOException e) {
			LOG.error("COUCHBASE START FAIL!", e);
		}
	}


	/**
	 * Lưu token
	 * 
	 * @param token
	 * @param moboId
	 */
	public void saveToken(String token, int moboId) {
		// set không quan tâm đã tồn tại token này hay chưa, add thực hiện check
		// xem đã tồn tại chưa
		couchbaseClient.set(token, TOKEN_SECOND_TTL, moboId);
	}


	/**
	 * Kiểm tra có tồn tại token của user này trên server không
	 * 
	 * @param token
	 * @return
	 */
	public boolean existToken(String token) {
		return couchbaseClient.get(token) != null;
	}


	public Integer getMoboIdByToken(String token) {
		return (Integer) couchbaseClient.get(token);
	}


	public void shutdown() {
		LOG.info("Shutdown Couchbase");
		if (couchbaseClient != null)
			couchbaseClient.shutdown();
	}

}
