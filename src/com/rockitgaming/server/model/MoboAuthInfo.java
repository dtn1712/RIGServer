package com.rockitgaming.server.model;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MoboAuthInfo {
	private String mobo_id;
	private boolean active;
	private String data;


	public String getMobo_id() {
		return mobo_id;
	}


	public void setMobo_id(String mobo_id) {
		this.mobo_id = mobo_id;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public String getData() {
		return data;
	}


	public void setData(String data) {
		this.data = data;
	}


	public boolean isAuthSuccess(int moboId, String deviceId) {
		// if(active == false || !mobo_id.equals(mobo_id) || data.isEmpty()) {
		if (active == false || mobo_id.equals("") || data.isEmpty()) {
			return false;
		}

		data = new String(Base64.decodeBase64(data));
		JsonElement element = new JsonParser().parse(data);

		if (element != null && element.isJsonObject()) {
			JsonObject json = element.getAsJsonObject();
			// if(json.has("device_id") &&
			// json.get("device_id").getAsString().equals(deviceId)) {
			if (json.has("device_id")) {
				return true;
			}
		}

		return false;
	}

}
