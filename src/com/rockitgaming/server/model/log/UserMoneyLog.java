package com.rockitgaming.server.model.log;

public class UserMoneyLog {
	private int _iwinId;
    private String _userName;
    /**
     * Tiền hiện tại của user
     */
    private long _money;
    /**
     * Tiền cộng thêm
     */
    private int _value;
    private int _serverId;
    private int _reasonId;
    private String _description = "";
    private String _reasonText = "";
    private int _boardId;

    /**
     * ngày ghi boardlog: ddMMyyyy
     */
    private String _boardLogDate = "";

    public UserMoneyLog() {
    }

    public String getBoardLogDate() {
        return _boardLogDate;
    }

    public void setBoardLogDate(String date) {
        this._boardLogDate = date;
    }

    public int getIwinId() {
        return _iwinId;
    }

    public void setIwinId(int _id) {
        this._iwinId = _id;
    }

    public long getMoney() {
        return _money;
    }

    public void setMoney(long _money) {
        this._money = _money;
    }

    public int getServerId() {
        return _serverId;
    }

    public void setServerId(int _serverId) {
        this._serverId = _serverId;
    }

    public String getUserName() {
        return _userName;
    }

    public void setUserName(String _userName) {
        this._userName = _userName;
    }

    public int getValue() {
        return _value;
    }

    public void setValue(int _value) {
        this._value = _value;
    }

    public int getReasonId() {
        return _reasonId;
    }

    public void setReasonId(int _reasonId) {
        this._reasonId = _reasonId;
    }

    public String getDescription() {
        if (_description != null && _description.length() > 250) {
            _description = _description.substring(0, 250);
        }
        return _description;
    }

    public void setDescription(String description) {
        this._description = description;
    }

    public String getReasonText() {
        return _reasonText;
    }

    public void setReasonText(String _reasonText) {
        this._reasonText = _reasonText;
    }
    
    public int getBoardId() {
		return _boardId;
	}

	public void setBoardId(int _boardId) {
		this._boardId = _boardId;
	}

    @Override
    public String toString() {
        return "UserMoneyLog{" + "_userId=" + _iwinId + "_userName=" + _userName + "_money=" + _money + "_value=" + _value + "_serverId=" + _serverId + "_reasonId=" + _reasonId + "description=" + _description + "_reasonText=" + _reasonText + "_boardLogDate=" + _boardLogDate + "_boardId" + _boardId + '}';
    }
}
