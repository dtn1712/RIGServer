package com.rockitgaming.server.model;

import com.rockitgaming.server.lang.AbstractProperties;

import java.util.Locale;

public class ClientMessage extends AbstractProperties {

    public static final String DEFAULT_FILE_NAME = "ClientMessage";
    
    public static final String BAO_TRI_CHUYEN_TIEN = "baoTriChuyenTien";
    public static final String DANG_NHAP_TU_MAY_KHAC = "dangNhapTuMayKhac";
    public static final String REQUIRE_LOGIN = "REQUIRE_LOGIN";
    public static final String RECEIVE_WIN1 = "receiveWin1";
    public static final String RECEIVE_WIN2 = "receiveWin2";
    public static final String SERVER_DOWN = "serverDown";
    public static final String NO_EMPTY_ROOM = "noEmptyRoom";
    public static final String LOGIN_2_ACC = "login2Account";
    public static final String LOGIN_3_MIN = "login3min";
    public static final String NICK_LENGTH = "nickLength";
    public static final String NICK_CONTAIN = "nickContain";
    public static final String SIGNUP_OK1 = "signupOK1";
    public static final String SIGNUP_OK2 = "signupOK2";
    public static final String SIGNUP_OK3 = "signupOK3";
    public static final String SIGNUP_FAIL = "signupFail";
    public static final String NO_MONEY_AVATAR = "noMoneyAvatar";
    public static final String NO_TABLE = "noAvailTable";
    public static final String ADMIN_LOGIN = "adminLogin";
    public static final String CARD_GATE_INVALID = "cardGateInvalid";
    public static final String CARD_MOBI_INVALID = "cardMobiInvalid";
    public static final String SERVER_UPDATE = "serverUpdate";
    public static final String RECEIVE_MONEY_FROM_AD1 = "receiveMoneyFromAd1";
    public static final String RECEIVE_MONEY_FROM_AD2 = "receiveMoneyFromAd2";
    public static final String WRONG_UPD_MONEY_COMMAND = "wrongCommandUpdMoney";
    public static final String KICK_OK1 = "kickOK1";
    public static final String KICK_OK2 = "kickOK2";
    public static final String KICK_FAIL = "kickFail";
    public static final String AD_CMD_OK = "adCmdOK";
    public static final String RELOAD_SERVICE_OK = "reloadServiceOK";
    public static final String LOAD_EVENT_OK = "loadEventOK";
    public static final String ACTION_COMPLETE = "actionComplete";
    public static final String STOP_SERVICE_OK = "stopServiceOK";
    public static final String START_SERVICE_OK = "startServiceOK";
    public static final String OK = "ok";
    public static final String FAIL = "fail";
    public static final String INVALID_CMD = "invalidCmd";
    public static final String ADD_ROOM_OK = "addRoomOK";
    public static final String ONLINE_DAIGIA = "onlineDaigia";
    public static final String ONLINE_CAOTHU1 = "onlineMaster1";
    public static final String ONLINE_CAOTHU2 = "onlineMaster2";
    //
    public static final String TELL_FRIEND = "TellFriend";
    public static final String COUPON0 = "Coupon0";
    public static final String COUPON1 = "Coupon1";
    public static final String COUPON2 = "Coupon2";
    public static final String COUPON3 = "Coupon3";
    public static final String COUPON_REASON = "CouponReason";
    public static final String TRANSFER_ERR1 = "TransferErr1";
    public static final String JUST_LOGIN = "JustLogin";
    public static final String ACC_LOCKED = "AccLocked";
    public static final String WRONG_USER = "WrongUser";
    public static final String LOCKING_NICK = "LockingNick";
    public static final String USE_MULTI_SERVICES = "UseMutiServices";
    public static final String TRY_AFTER_3_MIN = "TryAffer3Minutes";
    public static final String UPGRADE_GAME_MSG = "UpgradeGameMessage";
    public static final String EVENT_MSG = "EventMessage";
    public static final String NO_SUPPORT_SERVICE = "NoneSupportMessage";
    public static final String TOP_LIST_OFF = "TopListOff";
    public static final String SERVER_MAINTAIN = "ServerMantain";
    public static final String ANOTHER_SERVICE = "AnotherService";
    public static final String CHANGE_PASS_OK = "ChangePassOk";
    public static final String CHANGE_PASS_FAIL = "ChangePassFail";
    public static final String CLIENT_BG_NAME = "ClientBgName";
    public static final String TET_EVENT_MSG1 = "tetEventMsg1";
    public static final String TET_EVENT_MSG2 = "tetEventMsg2";
    public static final String BONUS_MSG = "bonusMsg";
    public static final String BONUS_WOMEN_DAY = "bonusWomenDay";
    public static final String BONUS_EVENT = "bonusEvent";
    public static final String EVENT_3004_1 = "event3004_1";
    public static final String EVENT_3004_2 = "event3004_2";
    public static final String ADMIN_NOTIFY = "AdminNotifies";
    public static final String WAP_URL = "WapUrl";
    public static final String VERSION_FILTER = "VersionFilter";
    public static final String CANNOT_START = "cannotStart";
    public static final String CANNOT_PLAY = "cannotPlayGame";
    public static final String INVITE_PLAY_GAME1 = "invitePlayGame1";
    public static final String INVITE_PLAY_GAME2 = "invitePlayGame2";
    public static final String INVITE_PLAYING_USER = "invitePlayingUser";
    public static final String INVITE_NOT_FOUND_USER = "inviteNotFoundUser";
    public static final String INVITE = "invite";
    public static final String INVITE_SUCCESS = "inviteSuccess";
    public static final String OLD_VERSION = "oldVersion";
    //quest
    public static final String QUEST_MAINTAIN = "questMaintain";
    public static final String QUEST_RECEIVE = "questReceive";
    public static final String QUEST_ERROR = "error";
    public static final String QUEST_ALREADY_CHOOSE = "questAlreadyReceive";
    public static final String QUEST_FINISH_TODAY = "questDoneToday";
    public static final String QUEST_NO_MONEY = "questNoMoney";
    public static final String QUEST_ENOUGH_TODAY = "questEnoughToday";
    public static final String QUEST_CANCEL = "questCancel";
    //khai thac thong tin user
    public static final String INFOR_MISSING = "inforMissing";
    public static final String INFOR_UPDATE = "inforUpdate";
    public static final String INFOR_NAME = "inforName";
    public static final String INFOR_GENDER = "inforGender";
    public static final String INFOR_BIRTHDAY = "inforBirthday";
    public static final String INFOR_ID = "inforID";
    public static final String INFOR_ADDRESS = "inforAddress";
    public static final String INFOR_PHONE = "inforPhone";
    //game chung
    public static final String POINT = "Point";
    public static final String NO_MONEY_OWNER = "NoMoneyOwner";
    public static final String NO_MONEY_USER = "NoMoneyUser";
    public static final String SET_USER_OK = "SetUserOK";
    public static final String SET_USER_FAIL = "SetUserFail";
    public static final String SET_MONEY_1 = "SetMoney1";
    public static final String SET_MONEY_2 = "SetMoney2";
    public static final String SET_MONEY_3 = "SetMoney3";
    public static final String SET_MONEY_4 = "SetMoney4";
    public static final String SET_MONEY_5 = "SetMoney5";
    public static final String SET_MONEY_6 = "SetMoney6";
    public static final String SET_MONEY_7 = "SetMoney7";
    public static final String SET_MONEY_8 = "SetMoney8";
    public static final String SET_MONEY_9 = "SetMoney9";
    //blackjack game
    public static final String BL_CHECK_1 = "blXetBai1";
    public static final String BL_CHECK_2 = "blXetBai2";
    public static final String QUAC = "quac";
    public static final String XIDACH = "xidach";
    public static final String XIBANG = "xibang";
    public static final String NGULINH = "ngulinh";
    //chess
    public static final String CHESS_CHECK = "chessCheck";
    //chinese chess
    public static final String CHINESE_CHESS_CHECK = "cChessCheck";
    //phom
    public static final String PHOM_CHECK_MONEY = "pCheckMoney";
    //version fitler
    public static final String FILTER_VER_222 = "filterVersion222";
    public static final String FILTER_VER1 = "filterVersion1";
    public static final String FILTER_VER2 = "filterVersion2";
    public static final String BAO_TRI_ITEMS = "baoTriItems";
    //item - avatar
    public static final String IT_MAX_BUY = "itMaxBuy";
    public static final String IT_CANNOT_DEL_INUSE = "itCannotDeleteInUse";
    public static final String IT_CANNOT_DEL = "itCannotDelete";
    public static final String IT_NOT_CHOOSE = "itNotChoose";
    public static final String IT_NOT_OWN = "itNotOwn";
    public static final String IT_NOT_ENOUGH_LEVEL = "itNotEnoughLevel";
    public static final String IT_UNRESOLVE = "itUnresolveError";
    public static final String IT_USE_TOO_MUCH = "itUseTooMuch";
    public static final String IT_BEING_USED = "itBeingUsed";
    public static final String IT_DUPLICATE_USING = "itDuplicateUsing";
    public static final String IT_NOT_ENOUGH_MONEY_BUY = "itNotEnoughMoneyBuy";
    public static final String IT_NOT_ENOUGH_LEVEL_BUY = "itNotEnoughLevelBuy";
    public static final String CANNOT_LOGIN_THIS_SERVER = "cannotLoginThisServer";
    public static final String CANNOT_BUY_AVATAR_IN_GAME = "cannotBuyAvatarInGame";
    public static final String ERR_TRANSFER_FROM = "err_activeFrom";
    public static final String ERR_TRANSFER_TO = "err_activeTo";
    //ngôn ngữ trong game
    public static final String BET_GAME = "betGame";
    public static final String GIVE_UP = "giveUp";
    public static final String LOSE = "lose";
    public static final String WIN = "win";
    public static final String DRAW = "draw";
    public static final String INVALID_CARD = "invalidCard";
    public static final String INVALID_MOVE = "invalidMove";
    //thông báo lúc chuyển tiền
    public static final String TRANSFER_TO_PLAYING_USER = "transferToPlayingUser";
    public static final String TRANSFER_INVALID_UNIT = "transferInvalidUnit";
    public static final String TRANSFER_INVALID_AMOUNT = "transferInvalidAmount";
    public static final String TRANSFER_INVALID_USER = "transferInvalidUser";
    public static final String TRANSFER_INVALID_USER_ACTIVE = "transferInvalidUserActive";
    public static final String TRANSFER_INVALID_USER_PROVIDER = "transferInvalidUserProvider";
    public static final String TRANSFER_SUCCESS1 = "transferSuccess1";
    public static final String TRANSFER_SUCCESS2 = "transferSuccess2";
    public static final String TRANSFER_SUCCESS3 = "transferSuccess3";
    public static final String TRANSFER_SUCCESS4 = "transferSuccess4";
    public static final String TRANSFER_SUCCESS5 = "transferSuccess5";
    //thông báo lúc join board
    public static final String JOIN_ERR1 = "join_err1";
    public static final String JOIN_ERR2 = "join_err2";
    public static final String JOIN_ERR3 = "join_err3";
    public static final String JOIN_ERR4 = "join_err4";
    public static final String JOIN_ERR5 = "join_err5";
    public static final String JOIN_ERR6 = "join_err6";
    public static final String JOIN_ERR7 = "join_err7";
    public static final String JOIN_ERR8 = "join_err8";
    public static final String JOIN_ERR9 = "join_err9";
    //thông báo trong phỏm
    public static final String EAT_CARD = "eat_card";
    public static final String CARD_EATEN = "card_eaten";
    public static final String DEN = "den"; //đền bài
    public static final String CHAY = "chay"; //cháy bài
    //thong bao khi server qua tai
    public static final String SERVER_FULL = "server_full";
    public static final String GET_LIXI = "get_lixi";
    public static final String GET_CHOCOLATE = "get_chocolate";
    public static final String WOMAN_DAY_GIFT = "woman_day_gift";
    public static final String HUNG_VUONG_DAY_GIFT = "hung_vuong_day_gift";
    //Avatar
    public static final String AVATAR1 = "avatar1";
    public static final String AVATAR2 = "avatar2";
    public static final String AVATAR3 = "avatar3";
    //register
    public static final String REGISTER_FREE = "registerFree";
    public static final String REGISTER_CHARGE_WIN = "registerChargeWin";
    public static final String REGISTER_CHARGE = "registerCharge";
    public static final String CONFIRM_SMS = "confirmSMS";
    //other
    public static final String HOUR = "hour";
    public static final String MINUTE = "minute";
    public static final String SECOND = "second";

    public static final String ELO_RESET = "eloReset";
    
    public static final String GAME_CREATE_BOARD = "game_create_board";
    public static final String NOT_ENOUGH_MONEY_TO_CREATE_BOARD = "not_enough_money_to_create_board";
    public static final String CREATE_NO_MONEY_BOARD = "create_no_money_board";
    public static final String IWIN_FRIEND_AND_CHAT_API_MAINTAIN = "iwin.friend.chat.api.maintain";
    
    public static final String DATA_UNAVAILABLE = "data_unavailable";
    public static final String BENEFIT = "benefit";

    public ClientMessage(Locale locale) {
        super(DEFAULT_FILE_NAME, locale);
    }
    
}
