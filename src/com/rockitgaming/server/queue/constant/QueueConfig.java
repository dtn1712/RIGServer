package com.rockitgaming.server.queue.constant;

import java.util.Properties;

import com.rockitgaming.server.netty.core.util.Reporter;

/**
*
* @author phuongla
*/
public class QueueConfig {

   public String host;
   public int port;
   public String username;
   public String pass;

   public int poolSize;

   public QueueConfig(Properties props){       
       init(props);
   }

   private void init(Properties props){	    
       host = props.getProperty("host");
       port = Integer.parseInt(props.getProperty("port"));
       username = props.getProperty("username");
       pass = props.getProperty("password");

       poolSize = Integer.parseInt(props.getProperty("poolSize"));
   
       	   
	   props.clear();       

       Reporter.serverInfo(new StringBuilder("RabbitMQ Service config: ")
               .append(host)
               .append(", port:").append(port)
               .append(", user:").append(username)
               .append(", pass:").append(pass).toString());
   }
   
}