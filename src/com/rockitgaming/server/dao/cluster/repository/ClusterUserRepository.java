package com.rockitgaming.server.dao.cluster.repository;

import org.apache.ignite.IgniteCache;

import com.rockitgaming.server.dao.cluster.IgniteRepository;
import com.rockitgaming.server.model.cluster.ClusterUser;

/**
 * @author lamhm
 *
 */
public class ClusterUserRepository extends IgniteRepository<ClusterUser, Integer> {

	public ClusterUserRepository(IgniteCache<Integer, ClusterUser> igniteCache) {
		super(igniteCache);
	}

}
