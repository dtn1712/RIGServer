package com.rockitgaming.server.dao;

import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.LoggerFactory;

import com.rockitgaming.server.asyntask.UserMoneyLogTask;
import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.model.log.UserMoneyLog;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.util.IwinLoggerNames;
import com.rockitgaming.server.netty.core.util.Reporter;
import com.rockitgaming.server.pools.ThreadPoolManager;

/**
 * Class xử lý tất cả truy vấn đến Database
 * 
 * @author vutp
 *
 */

public class DataManager extends BaseDatabase {
	private final HashMap<String, Integer> dbCounter;

	private static DataManager instance;

	public static DataManager getInstance() {
		if (instance == null) {
			instance = new DataManager();
			ConnPool.init(ServerConfig.DATABASE_CONFIG_FILE_NAME);
		}
		return instance;
	}

	private DataManager() {
		dbCounter = new HashMap<String, Integer>();
		Method[] methods = DataManager.class.getDeclaredMethods();
		for (Method m : methods) {
			dbCounter.put(m.getName(), 0);
		}
	}

	public static HashMap<String, Integer> getDbCounter() {
		return instance.dbCounter;
	}

	public static String dumpDbCounterLog() {
		StringBuilder dump = new StringBuilder();
		try {
			instance.countDbLog("dumpDbCounterLog");
			synchronized (instance.dbCounter) {
				Iterator<String> keys = instance.dbCounter.keySet().iterator();
				while (keys.hasNext()) {
					String funName = (String) keys.next();
					dump.append(funName);
					dump.append("\t");
					dump.append(instance.dbCounter.get(funName));
					dump.append("\t");
				}
			}
		} catch (Exception e) {
			Reporter.getErrorLogger().error("error when dump DB counter log", e);
		}
		return dump.toString();
	}

	public static void logDbCounter() {
		try {
			instance.countDbLog("logDbCounter");
			Reporter.getDbMonitorLogger().info(dumpDbCounterLog());
			synchronized (instance.dbCounter) {
				instance.dbCounter.clear();
				Method[] methods = DataManager.class.getDeclaredMethods();
				for (Method m : methods) {
					instance.dbCounter.put(m.getName(), 0);
				}
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("logDbCounter got exception ", e);
		}
	}

	/**
	 * Dung de dem so lan 1 function duoc call.
	 *
	 * @param functionName
	 */
	protected void countDbLog(String functionName) {
		try {
			synchronized (dbCounter) {
				int count = dbCounter.get(functionName) == null ? 0 : dbCounter.get(functionName);
				dbCounter.put(functionName, ++count);
			}
		} catch (Exception e) {
			// // Ignore this error.
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("logDbCounter got exception ", e);
		}
	}

	// TEST truy vấn DB
	public boolean checkUserNameExist(String userName) {
		countDbLog("checkUserExist");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;

		try {
			conn = ConnPool.getInstance().getConnectionRead();
			cs = conn.prepareCall("{ call sp_user_checkUsernameExists(?) }");
			cs.setString(1, userName);
			rs = cs.executeQuery();

			while (rs.next()) {
				return (rs.getInt(1) == 1) ? true : false;
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("checkUserExist", e);
		} finally {
			closePreparedStatement(cs);
			free(conn);
		}

		return false;
	}
	
	public User getUserInfoByUserName(String userName) {
		countDbLog("getUserInfoByUserName");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = ConnPool.getInstance().getConnectionRead();
			cs = conn.prepareCall("{ call sp_user_getInfoByUserName(?) }");
			cs.setString(1, userName);
			rs = cs.executeQuery();
			while (rs.next()) {
				User userProfile = new User();
				userProfile.setUserId(rs.getInt(DataKey.user_id));
				userProfile.setUserName(rs.getString(DataKey.username));
				userProfile.setMoney(rs.getLong(DataKey.coin));
				userProfile.setAvatar(rs.getString(DataKey.avatar));
				userProfile.setLanguage(rs.getByte(DataKey.lang));
				return userProfile;
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("getUserInfoByUserName", e);
		} finally {
			closePreparedStatement(cs);
			free(conn);
		}
		return null;
	}
	
	public User getUserInfoByMoboId(int moboId) {
		countDbLog("getUserInfoByMoboId");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = ConnPool.getInstance().getConnectionRead();
			cs = conn.prepareCall("{ call sp_user_getInfoByMoboID(?) }");
			cs.setInt(1, moboId);
			rs = cs.executeQuery();
			while (rs.next()) {
				User userProfile = new User();
				userProfile.setUserId(rs.getInt(DataKey.user_id));
				userProfile.setUserName(rs.getString(DataKey.username));
				userProfile.setMoney(rs.getLong(DataKey.coin));
				userProfile.setAvatar(rs.getString(DataKey.avatar));
				userProfile.setLanguage(rs.getByte(DataKey.lang));
				return userProfile;
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("getUserInfoByMoboId", e);
		} finally {
			closePreparedStatement(cs);
			free(conn);
		}
		return null;
	}

	public User getUserInfoByUserId(int userId) {
		countDbLog("getUserInfoByUserId");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = ConnPool.getInstance().getConnectionRead();
			cs = conn.prepareCall("{ call sp_user_getInfo(?) }");
			cs.setInt(1, userId);
			rs = cs.executeQuery();
			while (rs.next()) {
				User userProfile = new User();
				userProfile.setUserId(rs.getInt(DataKey.user_id));
				userProfile.setUserName(rs.getString(DataKey.username));
				userProfile.setMoney(rs.getLong(DataKey.coin));
				userProfile.setAvatar(rs.getString(DataKey.avatar));
				userProfile.setLanguage(rs.getByte(DataKey.lang));
				return userProfile;
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("getUserInfoByUserId", e);
		} finally {
			closePreparedStatement(cs);
			free(conn);
		}
		return null;
	}
	
	//TODO không còn dùng 2 params: deviceId, moboId
	public User insertUser(String deviceId, int moboId, String userName, int serverId, String password) {
		countDbLog("insertUser");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = ConnPool.getInstance().getConnectionWrite();
			cs = conn.prepareCall("{ call sp_user_insert(?, ?, ?, ?, ?) }");
			cs.setString(1, deviceId);
			cs.setString(2, userName);
			cs.setInt(3, moboId);
			cs.setInt(4, serverId);
			cs.setString(5, password);
			rs = cs.executeQuery();
			while (rs.next()) {
				User userProfile = new User();
				userProfile.setUserId(rs.getInt(DataKey.user_id));
				userProfile.setUserName(rs.getString(DataKey.username));
				userProfile.setMoney(rs.getLong(DataKey.coin));
				userProfile.setAvatar(ServerConfig.DEFAULT_AVATAR);
				return userProfile;
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("insertUser", e);
		} finally {
			closePreparedStatement(cs);
			free(conn);
		}
		return null;
	}
	
	/**
	 * Hàm này để test, sau này gọi sang hệ thông khác để kiểm tra
	 * @return 0: ko tồn tại, 1: đúng tài khoản, -1: sai mật khẩu
	 */
	public int checkLogin(String username, String password) {
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = ConnPool.getInstance().getConnectionRead();
			cs = conn.prepareCall("{ call sp_account_test_checkLogin(?,?) }");
			cs.setString(1, username);
			cs.setString(2, password);
			rs = cs.executeQuery();
			while (rs.next()) {
				return rs.getInt("result");
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("getUserInfoByDeviceId", e);
		} finally {
			closePreparedStatement(cs);
			free(conn);
		}
		return 0;
	}
	
	/**
	 * 
	 * @param userId
	 * @param serviceId
	 * @param timeOnline
	 */
	public void checkLogout(int userId, int serviceId, long timeOnline) {
		countDbLog("checkLogout");
		Connection conn = null;
		CallableStatement cs = null;

		try {
			conn = ConnPool.getInstance().getConnectionWrite();
			cs = conn.prepareCall("{ call sp_board_user_logoutServer(?, ?) }");
			cs.setInt(1, userId);
			cs.setInt(2, serviceId);
			cs.executeUpdate();

			cs = conn.prepareCall("{ call sp_user_profile_insert(?, ?) }");
			cs.setInt(1, userId);
			cs.setLong(2, timeOnline);
			cs.executeUpdate();
		} catch (SQLException ex) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("error when update time online", ex);
		} finally {
			closePreparedStatement(cs);
			free(conn);
		}
	}
	
	/**
	 * Đếm số lần login thất bại
	 * @param ip
	 * @param increase
	 * @return
	 */
	public boolean countLoginFail(String ip, int increase) {
        countDbLog("countLoginFail");
        Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
        try {
        	conn = ConnPool.getInstance().getConnectionRead();
        	cs = conn.prepareCall("{ call sp_count_login_fail(?, ?, ?) }");
            cs.setString(1, ip);
            cs.setInt(2, increase);
            cs.setInt(3, increase);
            rs = cs.executeQuery();
            
            while (rs.next()) {
				return true;
			}
        } catch (Exception e) {
            LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("insertDeviceWithUsername", e);
        } finally {            
        	closePreparedStatement(cs);
			free(conn);
        }

        return false;
    }
	
	/**
	 * Insert log ccu theo game
	 * @param serverId
	 * @param serviceId
	 * @param maxCCU
	 * @param minCCU
	 */
	public void insertGameCCULog(int serverId, int serviceId, int maxCCU, int minCCU) {
		countDbLog("insertGameCCULog");
    	Connection conn = null;
        CallableStatement cs = null;
        
        try {
        	conn = ConnPool.getInstance().getConnectionLog();
        	cs = conn.prepareCall("{ call sp_ervice_user_log_insert(?, ?, ?, ?) }");
        	cs.setInt(1, serverId);            
        	cs.setInt(2, serviceId);
        	cs.setLong(3, minCCU);
        	cs.setInt(4, maxCCU);            
            cs.executeQuery();
        } catch (SQLException ex) {
            LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("insertGameCCULog", ex);
        } finally {
            closePreparedStatement(cs);
            free(conn);            
        }
    }
	
	public boolean updateUserMoney(UserMoneyLog moneyLog, AtomicLong currentMoney) {
		countDbLog("updateUserMoney");
		Connection conn = null;
		CallableStatement statement = null;
		boolean result = false;
		currentMoney.set(-1);

		try {
			conn = ConnPool.getInstance().getConnectionMoney();
			statement = conn.prepareCall("{ call sp_user_money_update(?, ?, ?) }");
			statement.setInt(1, moneyLog.getIwinId());
			statement.setInt(2, moneyLog.getValue());
			statement.registerOutParameter(3, Types.BIGINT);

			result = statement.executeUpdate() > 0;

			if (result) {
				currentMoney.set(statement.getLong(3));
			}

		} catch (Exception e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("", e);
		} finally {
			closePreparedStatement(statement);
			free(conn);
		}

		ThreadPoolManager.getInstance().executeBoardMoneyLog(new UserMoneyLogTask(moneyLog));
		return result;
	}
}
