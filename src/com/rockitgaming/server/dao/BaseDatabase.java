package com.rockitgaming.server.dao;

import com.rockitgaming.server.netty.core.util.IwinLoggerNames;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseDatabase {
	
	protected void closeStatement(Statement st, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
		} catch (SQLException e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error(null, e);
		}
	}

	protected void closePreparedStatement(PreparedStatement ps, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
		} catch (SQLException e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error(null, e);
		}
	}

	protected void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error(null, e);
		}
	}

	protected void closePreparedStatement(PreparedStatement ps) {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (SQLException e) {
			LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error(null, e);
		}
	}
	
	protected void free(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
        	LoggerFactory.getLogger(IwinLoggerNames.ERROR_DB_LOG_NAME).error("error close connection.", e);
        }
    }	
}
