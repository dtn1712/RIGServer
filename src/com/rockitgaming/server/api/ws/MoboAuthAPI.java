package com.rockitgaming.server.api.ws;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rockitgaming.server.model.MoboAuthInfo;
import com.rockitgaming.server.netty.core.util.Reporter;

public class MoboAuthAPI extends AbstractAPI {

	private MoboAuthInfo authInfo;


	public MoboAuthInfo getMoboAuthInfo() {
		return authInfo;
	}


	public boolean authen(int moboId, String deviceId, String accesstoken) {
		try {
			String response = request(new ApiParam("access_token", accesstoken, false));
			JsonElement element = new JsonParser().parse(response);
			if (element.isJsonObject()) {
				JsonObject jObject = element.getAsJsonObject();
				if (jObject.has("code") && jObject.get("code").toString().equals("500040") && jObject.has("data")) {
					authInfo = gson.fromJson(jObject.get("data").toString(), MoboAuthInfo.class);
					return authInfo.isAuthSuccess(moboId, deviceId);
				}
			}
		} catch (Exception ex) {
			Reporter.getErrorLogger().error("", ex);
		}
		return false;
	}

}
