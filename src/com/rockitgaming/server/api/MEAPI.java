package com.rockitgaming.server.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.entities.CreateRoomSettings;
import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.entities.manager.IUserManager;
import com.rockitgaming.server.event.CoreEvent;
import com.rockitgaming.server.event.CoreEventParam;
import com.rockitgaming.server.event.ICoreEventParam;
import com.rockitgaming.server.exceptions.MECreateRoomException;
import com.rockitgaming.server.exceptions.MEErrorCode;
import com.rockitgaming.server.exceptions.MEErrorData;
import com.rockitgaming.server.exceptions.MEJoinRoomException;
import com.rockitgaming.server.exceptions.MERoomException;
import com.rockitgaming.server.exceptions.MERuntimeException;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.key.ErrorCode;
import com.rockitgaming.server.netty.core.socket.SocketServer;

public class MEAPI implements IMEApi {

	protected final SocketServer sfs;
	protected final Logger log;
	protected IUserManager globalUserManager;


	public MEAPI(SocketServer sfs) {
		this.log = LoggerFactory.getLogger(getClass());
		this.sfs = sfs;
		this.globalUserManager = sfs.getUserManager();
	}


	@Override
	public void logout(User user) {
		leaveRoom(user, user.getLastJoinedRoom(), false, false);
		globalUserManager.removeUser(user);
		//remove channel
		sfs.getMessageHandler().removeUser(user);
	}


	@Override
	public void kickUser(User Owner, User kickedUser, String paramString, int paramInt) {
		// TODO Auto-generated method stub

	}


	@Override
	public void disconnectUser(User user) {
		// TODO Auto-generated method stub

	}


	@Override
	public Room createRoom(CreateRoomSettings setting, User owner) throws MECreateRoomException {
		return createRoom(setting, owner, false, null, true, true);
	}


	@Override
	public Room createRoom(CreateRoomSettings setting, User owner, boolean joinAfterCreate, Room roomToLeave) throws MECreateRoomException {
		return createRoom(setting, owner, joinAfterCreate, roomToLeave, true, true);
	}


	@Override
	public Room createRoom(CreateRoomSettings setting, User owner, boolean joinAfterCreated, Room roomToLeave, boolean fireClientEvent, boolean fireServerEvent)
			throws MECreateRoomException {
		Room newRoom = null;
		try {
			String groupId = setting.getGroupId();
			if ((groupId == null) || (groupId.length() == 0)) {
				setting.setGroupId("default");
			}
			newRoom = sfs.getRoomManager().createRoom(setting, owner);
			if (owner != null) {
				owner.updateLastRequestTime();
			}

			if (fireServerEvent) {
				// notify to Room Extension
				Map<ICoreEventParam, Object> params = new HashMap<ICoreEventParam, Object>();
				params.put(CoreEventParam.USER, owner);
				params.put(CoreEventParam.ROOM, newRoom);
				CoreEvent event = new CoreEvent(SystemEventType.COMMAND_USER_CREATE_ROOM, params);
				newRoom.getExtension().handleServerEvent(event);
			}
		} catch (MECreateRoomException err) {
			String message = String.format("Room creation error. %s, %s", new Object[] { err.getMessage(), owner });
			Message msg = DefaultMessageFactory.createErrorMessage(ErrorCode.CREATE_ROOM_FAILED, message);
			sendExtensionResponse(msg, owner);
			throw new MECreateRoomException(message);
		}
		if ((newRoom != null) && (owner != null) && (joinAfterCreated)) {
			try {
				joinRoom(owner, newRoom, false, newRoom.getPassword());
			} catch (MEJoinRoomException e) {
				this.log.warn("Unable to join the just created Room: " + newRoom + ", reason: " + e.getMessage());
			}
		}
		return newRoom;
	}


	@Override
	public User getUserById(int userId) {
		return this.globalUserManager.getUserById(userId);
	}


	@Override
	public User getUserByName(String name) {
		return this.globalUserManager.getUserByName(name);
	}


	@Override
	public void joinRoom(User user, Room roomToJoin) throws MEJoinRoomException {
		joinRoom(user, roomToJoin, true, "");
	}


	@Override
	public void joinRoom(User user, Room roomToJoin, boolean joinAsSpectator, String password) throws MEJoinRoomException {
		try {
			Room roomToLeave = user.getLastJoinedRoom();

			if (user.isJoining()) {
				throw new MERuntimeException("Join request discarded. User is already in a join transaction: " + user);
			}
			user.setJoining(true);
			if (roomToJoin == null) {
				throw new MEJoinRoomException("Requested room doesn't exist", new MEErrorData(MEErrorCode.JOIN_BAD_ROOM));
			}
			if (!roomToJoin.isActive()) {
				String message = String.format("Room is currently locked, %s", new Object[] { roomToJoin });
				MEErrorData errData = new MEErrorData(MEErrorCode.JOIN_ROOM_LOCKED);
				errData.addParameter(roomToJoin.getName());

				throw new MEJoinRoomException(message, errData);
			}

			boolean doorIsOpen = true;
			if (roomToJoin.isPasswordProtected()) {
				doorIsOpen = roomToJoin.getPassword().equals(password);
			}
			if (!doorIsOpen) {
				String message = String.format("Room password is wrong, %s", new Object[] { roomToJoin });
				MEErrorData data = new MEErrorData(MEErrorCode.JOIN_BAD_PASSWORD);
				data.addParameter(roomToJoin.getName());

				throw new MEJoinRoomException(message, data);
			}
			if (roomToLeave != null) {
				leaveRoom(user, roomToLeave);
			}

			roomToJoin.addUser(user, joinAsSpectator);

			user.updateLastRequestTime();

			// trả về join success client biet
			Message message = DefaultMessageFactory.createMessage(roomToJoin.isGame() ? SystemEventType.COMMAND_USER_JOIN_ROOM
					: SystemEventType.COMMAND_USER_JOIN_GAME);
			message.putByte(SystemEventType.KEYB_STATUS, (byte) 1);
			sendExtensionResponse(message, user);

			// fireEvent cho extension xu ly tiep
			Map<ICoreEventParam, Object> params = new HashMap<ICoreEventParam, Object>();
			params.put(CoreEventParam.USER, user);
			params.put(CoreEventParam.ROOM, roomToJoin);
			CoreEvent event = new CoreEvent(roomToJoin.isGame() ? SystemEventType.COMMAND_USER_JOIN_ROOM : SystemEventType.COMMAND_USER_JOIN_GAME, params);
			roomToJoin.getExtension().handleServerEvent(event);

		} catch (MEJoinRoomException err) {
			String message = String.format("Join Error - %s", new Object[] { err.getMessage() });
			Message msg = DefaultMessageFactory.createErrorMessage(ErrorCode.JOIN_ROOM_FAILED, message);
			sendExtensionResponse(msg, user);

			throw new MEJoinRoomException(message);
		} finally {
			user.setJoining(false);
		}
	}


	public void leaveRoom(User user, Room room) {
		leaveRoom(user, room, true, true);
	}


	@Override
	public void leaveRoom(User user, Room room, boolean fireClientEvent, boolean fireServerEvent) {
		if (room == null) {
			room = user.getLastJoinedRoom();
			if (room == null) {
				throw new MERuntimeException("LeaveRoom failed: user is not joined in any room. " + user);
			}
		}
		if (!room.containsUser(user)) {
			return;
		}

		// int playerId = user.getPlayerId(room);
		room.removeUser(user);
		user.updateLastRequestTime();
		// fire client/server event khi user leave room
		if (fireClientEvent) {
			Message message = DefaultMessageFactory.createMessage(SystemEventType.COMMAND_USER_LEAVE_ROOM);
			message.putByte(SystemEventType.KEYB_STATUS, (byte) 1);
			sendExtensionResponse(message, user);
		}
		if (fireServerEvent) {
			// fireEvent cho extension xu ly tiep
			Map<ICoreEventParam, Object> params = new HashMap<ICoreEventParam, Object>();
			params.put(CoreEventParam.USER, user);
			params.put(CoreEventParam.ROOM, room);
			CoreEvent event = new CoreEvent(SystemEventType.COMMAND_USER_LEAVE_ROOM, params);
			room.getExtension().handleServerEvent(event);
		}

	}


	@Override
	public void removeRoom(Room room) {
		removeRoom(room, true, true);
	}


	public void removeRoom(Room room, boolean fireClientEvent, boolean fireServerEvent) {
		sfs.getRoomManager().removeRoom(room);

		// TODO xu ly fire client, server khi remove room
		if (fireClientEvent) {

		}
		if (fireServerEvent) {

		}
	}


	@Override
	public void changeRoomName(User owner, Room room, String newName) throws MERoomException {
		// TODO Auto-generated method stub

	}


	@Override
	public void changeRoomPassword(User owner, Room room, String newPassword) throws MERoomException {
		// TODO Auto-generated method stub

	}


	@Override
	public void changeRoomCapacity(User owner, Room room, int maxUsers, int maxSpectators) throws MERoomException {
		// TODO Auto-generated method stub

	}


	@Override
	public void spectatorToPlayer(User user, Room room) throws MERoomException {
		if (room == null) {
			throw new IllegalArgumentException("A target room was not specified (null)");
		}
		if (user == null) {
			throw new IllegalArgumentException("A user was not specified (null)");
		}
		user.updateLastRequestTime();
		try {
			room.switchSpectatorToPlayer(user);
			// notify to Room Extension
			Map<ICoreEventParam, Object> params = new HashMap<ICoreEventParam, Object>();
			params.put(CoreEventParam.USER, user);
			params.put(CoreEventParam.ROOM, room);
			CoreEvent event = new CoreEvent(SystemEventType.COMMAND_SPECTATOR_TO_PLAYER, params);
			room.getExtension().handleServerEvent(event);

		} catch (MERoomException err) {
			String message = String.format("SpectatorToPlayer Error - %s", new Object[] { err.getMessage() });
			throw new MERoomException(message, err.getErrorData());
		}
	}


	@Override
	public void playerToSpectator(User user, Room room) throws MERoomException {
		if (room == null) {
			throw new IllegalArgumentException("A target room was not specified (null)");
		}
		if (user == null) {
			throw new IllegalArgumentException("A user was not specified (null)");
		}
		user.updateLastRequestTime();
		try {
			room.switchPlayerToSpectator(user);

			// notify to Room Extension
			Map<ICoreEventParam, Object> params = new HashMap<ICoreEventParam, Object>();
			params.put(CoreEventParam.USER, user);
			params.put(CoreEventParam.ROOM, room);
			CoreEvent event = new CoreEvent(SystemEventType.COMMAND_PLAYER_TO_SPECTATOR, params);
			room.getExtension().handleServerEvent(event);

		} catch (MERoomException err) {
			String message = String.format("PlayerToSpectator Error - %s", new Object[] { err.getMessage() });
			throw new MERoomException(message, err.getErrorData());
		}
	}


	@Override
	public void sendExtensionResponse(IMessage message, List<User> recipients) {
		sfs.getMessageHandler().send(recipients, message);
	}


	@Override
	public void sendExtensionResponse(IMessage message, User recipient) {
		sfs.getMessageHandler().send(recipient, message);
	}

}