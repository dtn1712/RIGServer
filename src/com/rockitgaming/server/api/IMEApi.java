package com.rockitgaming.server.api;

import java.util.List;

import com.rockitgaming.server.entities.CreateRoomSettings;
import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.exceptions.MECreateRoomException;
import com.rockitgaming.server.exceptions.MEJoinRoomException;
import com.rockitgaming.server.exceptions.MERoomException;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

public abstract interface IMEApi {

	public abstract void logout(User user);


	public abstract void kickUser(User Owner, User kickedUser, String paramString, int paramInt);


	public abstract void disconnectUser(User user);


	public abstract Room createRoom(CreateRoomSettings setting, User owner) throws MECreateRoomException;


	public abstract Room createRoom(CreateRoomSettings setting, User owner, boolean joinAfterCreate, Room roomToLeave) throws MECreateRoomException;


	public abstract Room createRoom(CreateRoomSettings setting, User owner, boolean joinAfterCreated, Room roomToLeave, boolean fireClientEvent,
			boolean fireServerEvent) throws MECreateRoomException;


	public abstract User getUserById(int userId);


	public abstract User getUserByName(String name);

	
	public abstract void joinRoom(User user, Room roomToJoin) throws MEJoinRoomException;
	

	public abstract void joinRoom(User user, Room roomToJoin,boolean joinAsSpectator, String password) throws MEJoinRoomException;


	public abstract void leaveRoom(User user, Room roomToLeave);


	public abstract void leaveRoom(User user, Room room, boolean fireClientEvent, boolean fireServerEvent);


	public abstract void removeRoom(Room room);


	public abstract void removeRoom(Room room, boolean fireClientEvent, boolean fireServerEvent);


	public abstract void changeRoomName(User owner, Room room, String newName) throws MERoomException;


	public abstract void changeRoomPassword(User owner, Room room, String newPassword) throws MERoomException;


	public abstract void changeRoomCapacity(User owner, Room room, int maxUsers, int maxSpectators) throws MERoomException;


	public abstract void spectatorToPlayer(User user, Room room) throws MERoomException;


	public abstract void playerToSpectator(User user, Room room) throws MERoomException;


	public abstract void sendExtensionResponse(IMessage message, List<User> recipients);


	public abstract void sendExtensionResponse(IMessage message, User recipient);

}
