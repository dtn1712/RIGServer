package com.rockitgaming.server.api;

import com.rockitgaming.server.netty.core.socket.SocketServer;

public class APIManager {
	private final String serviceName = "APIManager";
	private SocketServer sfs;
	private IMEApi sfsApi;


	public APIManager(SocketServer socketServer) {
		this.sfs = socketServer;
		this.sfsApi = new MEAPI(this.sfs);
	}

	public IMEApi getMEApi() {
		return this.sfsApi;
	}


	public void destroy(Object arg0) {
	}


	public String getName() {
		return serviceName;
	}


	public void handleMessage(Object msg) {
		throw new UnsupportedOperationException("Not supported");
	}


	public void setName(String arg0) {
		throw new UnsupportedOperationException("Not supported");
	}
}
