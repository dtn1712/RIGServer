package com.rockitgaming.server.asyntask;

import com.rockitgaming.server.netty.core.device.ClientDevice;
import com.rockitgaming.server.queue.QueueService;
import com.rockitgaming.server.queue.constant.QueueLogKey;

public class DevicesMapLogTask implements Runnable {

	private String userName;
    private ClientDevice clientDevice;

    public DevicesMapLogTask(String userName, ClientDevice clientDevice) {
        this.userName = userName;
        this.clientDevice = clientDevice;
    }
    
    @Override
    public void run() {
    	if (clientDevice == null || clientDevice.getNotifyKey() == null || clientDevice.getNotifyKey().isEmpty()) {
            return;
        }
        String dataLogs = userName + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getNotifyKey() + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getOs() + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getCarrier() + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getNet() + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getLang();
        QueueService.getInstance().sendLog(QueueLogKey.GENERAL_EXCHANGE, QueueLogKey.DevicesMap, false, dataLogs);
    }
}