package com.rockitgaming.server.asyntask;

import com.rockitgaming.server.netty.core.device.ClientDevice;
import com.rockitgaming.server.queue.QueueService;
import com.rockitgaming.server.queue.constant.QueueLogKey;

public class DevicesLogTask implements Runnable {

	private ClientDevice clientDevice;
	
	public DevicesLogTask(ClientDevice clientDevice) {
		this.clientDevice = clientDevice;
	}
	
	@Override
	public void run() {
	    String dataLogs = clientDevice.getNotifyKey() + QueueLogKey.SEPERATE_CHARS +
        		clientDevice.getPhone() + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getDevice() + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getOs() + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getOsVersion() + QueueLogKey.SEPERATE_CHARS +
                clientDevice.getVersion() + QueueLogKey.SEPERATE_CHARS +
                String.valueOf(clientDevice.getIsJaibreak());
        QueueService.getInstance().sendLog(QueueLogKey.GENERAL_EXCHANGE, QueueLogKey.Devices, false, dataLogs);
    }
}