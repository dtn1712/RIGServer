package com.rockitgaming.server.asyntask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;

import com.rockitgaming.server.dao.BaseDatabase;
import com.rockitgaming.server.dao.ConnPool;
import com.rockitgaming.server.model.log.BoardLog;
import com.rockitgaming.server.netty.core.util.Reporter;
import com.rockitgaming.server.queue.QueueService;
import com.rockitgaming.server.queue.constant.QueueLogKey;

public class InsertBoard extends BaseDatabase implements Runnable{

	private BoardLog board;
	
	public InsertBoard(BoardLog board) {
		this.board = board;
	}	
	
	@Override
	public void run() {
		
        Connection conn = null;
        CallableStatement statement = null;
        
        try {        	
            conn = ConnPool.getInstance().getConnectionLog();
            statement = conn.prepareCall("{ call sp_board_log_insert(?, ?, ?, ?, ?) }");
            statement.setInt(1, board.getRoomId());            
            statement.setInt(2, board.getTotalUserInBoard());
            statement.setLong(3, board.getMoney());
            statement.setInt(4, board.getServiceId());            
            statement.registerOutParameter(5, Types.INTEGER);
            
            statement.executeUpdate();
            
            board.setId(statement.getInt(5));
            
            if(board.getId() != -1 && board.getPlayersInBoard().size() > 0) {
            	StringBuilder data = new StringBuilder().append(board.getId()).append(QueueLogKey.SEPERATE_CHARS);
        		int size = board.getPlayersInBoard().size();
        		for (int i = 0; i < size; i++) {
        			data.append(board.getPlayersInBoard().get(i));
        			if (i < size - 1) {
        				data.append(QueueLogKey.SEPERATE_CHARS);
        			}
        		}
        		QueueService.getInstance().sendLog(QueueLogKey.BOARD_EXCHANGE, QueueLogKey.UserBoard, true, data.toString());
            } else {
            	Reporter.getDatabaseLogger().warn("Board info is not valid: " + board.toString());
            }            
            
        } catch (Exception e) {
        	Reporter.getDatabaseLogger().error("", e);
        } finally {
            closePreparedStatement(statement);
            free(conn);
        }        
	}
}