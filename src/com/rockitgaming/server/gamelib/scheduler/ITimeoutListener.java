package com.rockitgaming.server.gamelib.scheduler;

public interface ITimeoutListener {
	/**
	 * Xử lý kết thúc game khi hết giờ
	 */
	void timeout();
}
