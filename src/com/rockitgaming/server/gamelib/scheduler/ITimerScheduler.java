package com.rockitgaming.server.gamelib.scheduler;


public interface ITimerScheduler {
	String getId();
	
	long getTimeMillis();
	
	void startCountdown(long millis);
	
	void timeout();
	
	void cancelTimeout();
	
	void registerListener(ITimeoutListener listener);
}
