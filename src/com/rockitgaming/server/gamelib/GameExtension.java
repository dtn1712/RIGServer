package com.rockitgaming.server.gamelib;

import java.util.List;

import com.rockitgaming.server.extensions.MEExtension;
import com.rockitgaming.server.gamelib.eventhandler.AutoJoinRoomRequestHandler;
import com.rockitgaming.server.gamelib.eventhandler.ChatRoomRequestHandler;
import com.rockitgaming.server.gamelib.eventhandler.GetRoomListHandler;
import com.rockitgaming.server.gamelib.eventhandler.InGameRequestHandler;
import com.rockitgaming.server.gamelib.eventhandler.UserJoinRoomExtensionHandler;
import com.rockitgaming.server.gamelib.eventhandler.UserLeaveRoomExtensionHandler;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.key.ErrorCode;
import com.rockitgaming.server.netty.core.util.Reporter;

public abstract class GameExtension extends MEExtension {

	public GameController gameController;
	// public IService service;

	private static GameExtension _instance;


	public static GameExtension getExtension() {
		return _instance;
	}


	public GameExtension() {
		if (_instance == null) {
			_instance = this;
		}
	}


	@Override
	public void init() {
		Reporter.infor("**************** GameLIB Extension init ****************");
		gameController = new GameController(this.getCurrentRoom(), initInterface());

		addEventHandler();
		Reporter.infor("**************** GameLIB Extension init complete ****************");
	}


	private void addEventHandler() {
		addRequestHandler(IwinCommand.COMMAND_REQUEST_IN_GAME, InGameRequestHandler.class);
		addRequestHandler(IwinCommand.COMMAND_AUTO_JOIN_ROOM, AutoJoinRoomRequestHandler.class);
		addRequestHandler(IwinCommand.COMMAND_CHAT_ROOM, ChatRoomRequestHandler.class);
		addRequestHandler(IwinCommand.COMMAND_GET_ROOM_LIST, GetRoomListHandler.class);

		addEventHandler(SystemEventType.COMMAND_USER_JOIN_ROOM, UserJoinRoomExtensionHandler.class);
		addEventHandler(SystemEventType.COMMAND_USER_LEAVE_ROOM, UserLeaveRoomExtensionHandler.class);
	}


	/**
	 * Khởi tạo RoomLogic bàn do hệ thống tạo
	 */
	public abstract GameInterface initInterface();


	/**
	 * Handle message forward from CoreExtension
	 */
	public void handleInternalMessage() {

	}


	// @Override
	// public boolean isPlaying() {
	// return gameController.getStatus() == STATUS.GAME_PLAYING;
	// }
	//
	// @Override
	// public void setService(IService iService) {
	// this.service = iService;
	// }

	/**
	 * Send response cho client phai di qua ham nay để encrypt Data
	 * 
	 * @param msgId
	 * @param message
	 * @param user
	 */
	public void sendResponse(Message message, short errorCode, User user) {
		message.putShort(SystemEventType.KEYR_CODE, errorCode);
		getExtension().getApi().sendExtensionResponse(message, user);
	}


	public void sendResponse(Message message, User user) {
		sendResponse(message, ErrorCode.SUCCESS, user);
	}


	public void sendResponseForListUser(Message message, List<User> users) {
		message.putShort(SystemEventType.KEYR_CODE, ErrorCode.SUCCESS);
		getExtension().getApi().sendExtensionResponse(message, users);
	}

}
