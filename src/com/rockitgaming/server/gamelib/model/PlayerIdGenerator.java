package com.rockitgaming.server.gamelib.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.rockitgaming.server.exceptions.MERoomException;
import com.rockitgaming.server.netty.core.gate.wood.User;

public class PlayerIdGenerator {
	private volatile long[] playerSlots;

	public PlayerIdGenerator(int maxPlayer) {
		init(maxPlayer);
	}

	private void init(int maxPlayer) {
		this.playerSlots = new long[maxPlayer + 1];
		Arrays.fill(this.playerSlots, 0L);
	}

	/**
	 * Tìm 1 chỗ ngồi free
	 * @return
	 */
	public int getOneFreeSlot() {
		int playerId = 0;

		synchronized (this.playerSlots) {
			for (int ii = 1; ii < this.playerSlots.length; ii++) {
				if (this.playerSlots[ii] == 0) {
					playerId = ii;
					break;
				}
			}
		}

		return playerId;
	}

	/**
	 * Cho user ngồi vào vị trí free trong bàn
	 * @param playerId
	 * @param user
	 * @return
	 * @throws SFSRoomException
	 */
	public int sitDownSlot(int playerId, User user) throws MERoomException {
		if (playerId == -1 || playerId >= this.playerSlots.length) {
			return 0;
		}
		synchronized (this.playerSlots) {
			if (this.playerSlots[playerId] != 0) {
				throw new MERoomException("This slot already have user");
			}
			this.playerSlots[playerId] = user.getUserId();
		}
		return playerId;
	}

	/**
	 * Giải phóng position khi user leave
	 * @param playerId
	 */
	public void freePlayerSlot(int playerId) {
		if (playerId == -1 || playerId >= this.playerSlots.length) {
			return;
		}
		synchronized (this.playerSlots) {
			this.playerSlots[playerId] = 0L;
		}
	}

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public int freeUserSlot(long userId) {
		int result = 0;
		synchronized (this.playerSlots) {
			for (int ii = 1; ii < this.playerSlots.length; ii++) {
				if (this.playerSlots[ii] == userId) {
					this.playerSlots[ii] = 0L;
					result = ii;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Lấy userId theo position
	 * @param playerId
	 * @return
	 */
	public long getUserIdByPlayerID(int playerId) {
		if (playerId == -1 || playerId >= this.playerSlots.length) {
			return 0;
		}
		synchronized (this.playerSlots) {
			return this.playerSlots[playerId];
		}
	}

	/**
	 * Lấy danh sách position
	 * @return
	 */
	public List<Long> getUserIdList() {
		List<Long> result = new ArrayList<Long>();
		synchronized (this.playerSlots) {
			for (int ii = 1; ii < this.playerSlots.length; ii++) {
				if (this.playerSlots[ii] != 0) {
					result.add(this.playerSlots[ii]);
				}
			}
		}
		return result;
	}

	/**
	 * Lấy position theo user
	 * @param user
	 * @return
	 */
	public int getPlayersIdOf(User user) {

		synchronized (this.playerSlots) {
			for (int ii = 1; ii < this.playerSlots.length; ii++) {
				if (user.getUserId() == this.playerSlots[ii]) {
					return ii;
				}
			}
		}
		return 0;
	}
}
