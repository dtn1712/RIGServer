/**
 * 
 */
package com.rockitgaming.server.gamelib.model;


/**
 * Đối tượng chứa thông tin của 1 Room
 * Level, MoneyBet, TimeOut, Owner
 * 
 * @author thuctvd
 *
 */
public class RoomInfo {
	private byte gameId;
	private byte levelId;
	private String levelName;
	private int betCoin;
	private int timeOut;
	private String rules;

	public RoomInfo(byte gameId) {
		this.gameId = gameId;
	}
	
	public RoomInfo(byte id, String name, byte gameId) {
		this.levelId = id;
		this.levelName = name;
		this.gameId = gameId;
	}

	public byte getlevelId() {
		return levelId;
	}

	public void setlevelId(byte id) {
		this.levelId = id;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public int getBetCoin() {
		return betCoin;
	}

	public void setBetCoin(int betCoin) {
		this.betCoin = betCoin;
	}

	public int getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}

	public void setLevelRoom(byte level) {
		this.levelId = level;
	}

	public byte getGameId() {
		return gameId;
	}

	public void setGameId(byte gameId) {
		this.gameId = gameId;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public void updateRoomInfo(Integer betCoin, Integer timeout, String rules) {
		if (betCoin != null) {
			setBetCoin(betCoin);
		}

		if (timeout != null) {
			setTimeOut(timeout);
		}

		if (rules != null) {
			setRules(rules);
		}
	}

//	public Message toSFSObject(int roomId, String roomName) {
//		ISFSObject isfsObject = new SFSObject();
//
//		isfsObject.putInt(IwinKey.ROOM_ID, roomId);
//		isfsObject.putUtfString(IwinKey.ROOM_NAME, roomName);
//		isfsObject.putInt(IwinKey.BETCOIN, getBetCoin());
//		isfsObject.putInt(IwinKey.TIMEOUT, getTimeOut());
//
//		return isfsObject;
//	}

}
