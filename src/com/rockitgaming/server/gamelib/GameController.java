/**
 * 
 */
package com.rockitgaming.server.gamelib;

import java.util.List;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.gamelib.api.GameAPI;
import com.rockitgaming.server.gamelib.model.PlayerIdGenerator;
import com.rockitgaming.server.gamelib.result.IPlayMoveResult;
import com.rockitgaming.server.gamelib.result.StartGameResult;
import com.rockitgaming.server.gamelib.scheduler.ITimeoutListener;
import com.rockitgaming.server.gamelib.scheduler.SchedulerManager;
import com.rockitgaming.server.gamelib.scheduler.TimeoutScheduler;
import com.rockitgaming.server.model.log.BoardLog;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * Đối tượng điều phối workflow khi user chơi game
 * 
 * @author thuctvd
 *
 */
public class GameController implements ITimeoutListener {

	public static enum STATUS {
		WAIT_USER_READY, WAIT_START_REQUEST, GAME_STARTING, GAME_PLAYING;
	}

	private final Room room;
	private final GameInterface iGame;
	private final GameAPI api;
	private STATUS status;
	private BoardLog boardLog;
	private final PlayerIdGenerator playerIdGenerator;


	public GameController(Room room, GameInterface gInterface) {
		this.room = room;
		this.iGame = gInterface;
		this.api = new GameAPI(this);
		this.iGame.setApi(this.api);
		this.status = STATUS.WAIT_USER_READY;
		this.playerIdGenerator = new PlayerIdGenerator(room.getMaxUsers());

		// Đăng ký trận đấu với timeout scheduler
		// (hiện tại chỉ có kiểu đếm ngược đến timeout)
		TimeoutScheduler timeoutScheduler = new TimeoutScheduler(TimeoutScheduler.generateId(room));
		timeoutScheduler.registerListener(this);
		SchedulerManager.getInstance().registerScheduler(timeoutScheduler);
	}


	public Room getRoom() {
		return this.room;
	}


	public GameAPI getApi() {
		return this.api;
	}


	public GameInterface getGameInterface() {
		return this.iGame;
	}


	public STATUS getStatus() {
		return this.status;
	}


	public void onUserJoinBoard(User userJoin) {
		this.iGame.onUserJoinBoard(userJoin);
	}


	public void onUserLeaveBoard(User userLeave) {
		this.iGame.onUserLeaveBoard(userLeave);
	}


	public void onPlayMove(User sender, Message data) {
		IPlayMoveResult result = this.iGame.onPlayMoveHandle(sender, data);
		if (result != null) {
			// Xử lý kết quả sau khi thực hiện 1 nước đi trong game
			result.handleResult(this);
		}
	}


	public void getGameData(Message message) {
		this.iGame.getGameData(message);
	}


	public void onStartGame(User sender, Message message) {
		this.status = STATUS.GAME_STARTING;
		initBoardLog();

		StartGameResult result = this.iGame.onStartGame(getRoom().getOwner(), getRoom().getPlayersList(), message);
		if (result != null) {
			result.handleResult(this);

			if (result.isSuccess()) {
				this.status = STATUS.GAME_PLAYING;
			}
		}
	}


	public void startCountdownStartGame(long timeOut) {
		iGame.startCountdownStartGame(timeOut);
	}


	/**
	 * Refresh tiền từ DB khi bắt đầu ván - Chống hack
	 * 
	 * @param user
	 */
	// private void refreshUserMoneyInfo(User user) {
	// // TODO Refresh tiền của User từ DB
	//
	// }


	private void initBoardLog() {
		boardLog = new BoardLog();
		boardLog.setRoomId(getRoom().getId());
		List<User> users = getRoom().getPlayersList();
		boardLog.setTotalUserInBoard(users.size());
		for (User user : users) {
			boardLog.addPlayerId(user.getUserId());
		}
		// RoomInfo roomInfo = (RoomInfo)
		// getRoom().getProperty(IwinKey.ROOM_INFO);
		// boardLog.setMoney(roomInfo.getBetCoin());
		// boardLog.setServiceId(roomInfo.getServiceId());
		// ThreadPoolManager.getInstance().executeBoardMoneyLog(new
		// InsertBoard(boardLog));
	}


	/**
	 * Xử lý khi hết thời gian của 1 lượt đi
	 */
	public void timeout() {
		IPlayMoveResult result = this.iGame.timeout();
		if (result != null) {
			// Xử lý kết quả sau khi thực hiện 1 nước đi trong game
			result.handleResult(this);
		}
	}


	/**
	 * Gửi thông báo cho Client - Ván đấu không thể Bắt đầu
	 * 
	 * @param errorCode
	 */
	public void notifyStartMatchError(int errorCode) {

	}


	public BoardLog getBoardLog() {
		return boardLog;
	}


	public PlayerIdGenerator getPlayerIdGenerator() {
		return playerIdGenerator;
	}

}
