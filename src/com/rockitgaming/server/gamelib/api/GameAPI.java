/**
 * 
 */
package com.rockitgaming.server.gamelib.api;

import com.rockitgaming.server.dao.DataManager;
import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.exceptions.MERoomException;
import com.rockitgaming.server.gamelib.GameController;
import com.rockitgaming.server.gamelib.GameExtension;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.gamelib.model.RoomInfo;
import com.rockitgaming.server.gamelib.scheduler.ITimerScheduler;
import com.rockitgaming.server.gamelib.scheduler.SchedulerManager;
import com.rockitgaming.server.gamelib.scheduler.TimeoutScheduler;
import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.model.log.UserMoneyLog;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.key.ErrorCode;
import com.rockitgaming.server.netty.core.util.Reporter;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Các API cung cấp cho các tính năng Game cần gọi ngược ra GAMELIB xử lý
 * 
 * @author thuctvd
 *
 */
public class GameAPI {
	private final GameController controller;


	public GameAPI(GameController gameLogic) {
		this.controller = gameLogic;
	}


	public GameController getController() {
		return this.controller;
	}


	public void startTimeOut(long timeMillis, Message params) {
		if (timeMillis > 0) {
			String scheduleId = TimeoutScheduler.generateId(controller.getRoom());
			ITimerScheduler timeoutScheduler = SchedulerManager.getInstance().getTimerScheduler(scheduleId);
			if (timeoutScheduler != null) {
				timeoutScheduler.cancelTimeout();
				timeoutScheduler.startCountdown(timeMillis + ServerConfig.delayTimeMillis);
			}
		}
	}


	public long getRemainTimeOut() {
		String scheduleId = TimeoutScheduler.generateId(controller.getRoom());
		ITimerScheduler timerScheduler = SchedulerManager.getInstance().getTimerScheduler(scheduleId);
		if (timerScheduler != null)
			return timerScheduler.getTimeMillis();

		return 0;
	}


	public void cancelTimeOut() {
		String scheduleId = TimeoutScheduler.generateId(controller.getRoom());
		ITimerScheduler timerScheduler = SchedulerManager.getInstance().getTimerScheduler(scheduleId);
		if (timerScheduler != null)
			timerScheduler.cancelTimeout();
	}


	public void sendToUser(Message message, short errorCode, User user) {
		((GameExtension) (controller.getRoom().getExtension())).sendResponse(message, errorCode, user);
	}


	public void sendAllInRoomExceptUser(Message data, User user) {
		List<User> users = this.controller.getRoom().getUserList();
		users.remove(user);
		sendResponseForListUser(data, users);
	}


	public void sendAllInRoomExceptUsers(Message data, List<Integer> userIds) {
		List<User> users = this.controller.getRoom().getUserList();
		for (int userId : userIds) {
			users.remove(this.controller.getRoom().getUserById(userId));
		}

		sendResponseForListUser(data, users);
	}


	public void sendAllInRoom(Message message) {
		List<User> users = this.controller.getRoom().getUserList();
		sendResponseForListUser(message, users);
	}


	public void sendResponseForListUser(Message message, List<User> users) {
		message.putShort(SystemEventType.KEYR_CODE, ErrorCode.SUCCESS);
		GameExtension gameExtension = (GameExtension) (controller.getRoom().getExtension());
		gameExtension.sendResponseForListUser(message, users);
	}


	public long getBetCoin() {
		return ((RoomInfo) controller.getRoom().getProperty(IwinKey.ROOM_INFO)).getBetCoin();
	}


	public int getTimeOut() {
		return ((RoomInfo) controller.getRoom().getProperty(IwinKey.ROOM_INFO)).getTimeOut();
	}


	public String getRules() {
		return ((RoomInfo) controller.getRoom().getProperty(IwinKey.ROOM_INFO)).getRules();
	}


	/**
	 * Cập nhật tiền của iwinUser ở server, db, và ghi log lịch sử tiền
	 * 
	 * @param user
	 * @param value
	 * @param reasonId
	 */
	public void updateUserMoney(User user, int value, int reasonId, String description) {
		UserMoneyLog moneyLog = new UserMoneyLog();
		moneyLog.setIwinId(user.getUserId());
		moneyLog.setUserName(user.getUserName());
		moneyLog.setMoney(user.getMoney());
		moneyLog.setValue(value);
		moneyLog.setServerId(ServerConfig.serverId);
		moneyLog.setReasonId(reasonId);
		moneyLog.setReasonText("");
		moneyLog.setDescription(description);
		moneyLog.setBoardId(getController().getBoardLog().getId());
		AtomicLong currentMoney = new AtomicLong();
		boolean success = DataManager.getInstance().updateUserMoney(moneyLog, currentMoney);
		if (success) {
			user.setMoney(currentMoney.get());
		}
	}


	/**
	 * Cập nhật level của iwinUser ở server, db khi end game
	 * 
	 * @param iwinUser
	 * @param resultType
	 * @param newXp
	 * @param extraData
	 */
	public void updateUserLevel(User iwinUser, byte resultType, int newXp, String extraData) {
		// Level userLevel = iwinUser.getLevel();
		// userLevel.updateResult(resultType);
		// userLevel.setXp(newXp);
		// userLevel.setExtraData(extraData);
		// DataManager.instance.updateUserData(iwinUser.getIwinId(), userLevel);
	}


	public void switchPlayerToSpectator(int userId) {
		User player = this.controller.getRoom().getUserById(userId);
		if (player.isPlayer()) {
			try {
				this.controller.getRoom().switchPlayerToSpectator(player);
			} catch (MERoomException e) {
				Reporter.debug("GameAPI - switchPlayerToSpectator ERROR", e);
			}
		}
	}


	public void switchSpectatorToPlayer(int userId) {
		User spectator = this.controller.getRoom().getUserById(userId);
		if (spectator.isSpectator()) {
			try {
				this.controller.getRoom().switchSpectatorToPlayer(spectator);
				if (getOwner() == null) {
					controller.getRoom().setOwner(spectator);
				}

				Message message = DefaultMessageFactory.createMessage(IwinCommand.COMMAND_SWITCH_SPECTATOR_TO_PLAYER);
				message.putInt(SystemEventType.KEYI_USER_ID, spectator.getUserId());
				message.putString(SystemEventType.KEYS_USERNAME, spectator.getUserName());
				message.putString(SystemEventType.KEYS_AVATAR, spectator.getAvatar());
				message.putLong(SystemEventType.KEYL_MONEY, spectator.getMoney());
				message.putByte(IwinKey.KEYB_IS_OWNER, (spectator == getOwner()) ? (byte) 1 : (byte) 0);
				message.putInt(IwinKey.KEYI_PLAYER_ID, spectator.getPlayerId());
				sendAllInRoom(message);
			} catch (MERoomException e) {
				Reporter.debug("GameAPI - switchSpectatorToPlayer ERROR", e);
			}
		}
	}


	public void leaveRoom(int sfsId) {
		Room room = controller.getRoom();
		User user = room.getUserById(sfsId);
		if (user != null) {
			GameExtension gameExtension = (GameExtension) (room.getExtension());
			gameExtension.getApi().leaveRoom(user, room);
		}
	}


	public User getOwner() {
		return controller.getRoom().getOwner();
	}

}