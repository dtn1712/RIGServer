package com.rockitgaming.server.gamelib.common;

/**
 * Dùng để xác định kết quả ván đấu: thắng, thua, hòa, bỏ cuộc
 * @author binhlt
 *
 */
public class ResultType {
	
	public static final byte WIN = 1;
	public static final byte LOSE = 2;
	public static final byte DRAW = 3; 
	public static final byte GIVE_UP = 4;
	
	public static final int getMoneyReason(byte resultType) {
		switch (resultType) {
			case WIN:
				return MoneyReason.WIN;
			case LOSE:
				return MoneyReason.LOSE;
			case DRAW:
				return MoneyReason.DRAW;
			case GIVE_UP:
				return MoneyReason.GIVE_UP;
			default:
				return MoneyReason.UNDEFINED;
		}
	}
	
}