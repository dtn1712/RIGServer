package com.rockitgaming.server.gamelib.common;

public class MoneyReason {
	
	public static final int UNDEFINED = -1;
	
	public static final int WIN = 1;
	public static final int LOSE = 2;
	public static final int DRAW = 3;
	public static final int GIVE_UP = 4;
	
}