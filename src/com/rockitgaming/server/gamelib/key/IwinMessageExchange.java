package com.rockitgaming.server.gamelib.key;

/**
 * CLASS Định nghĩa trao đổi message giữa Client - Server
 * 
 * @author vutp
 *
 */

// TODO nhập chung với class SystemEventType
public class IwinMessageExchange {

	public class IwinCommand {
		/**
		 * Gửi tin nhắn đến người chơi trong bàn <br>
		 * C2S : <br>
		 * <code>{ Message: MESSAGE_ID = SEND_PUBLIC_CHAT,<br>
		 * 					 DATA       = SENDER_ID, BYTE_MSG_TYPE, STRING_PARAM_1, STRING_ARRAY_PARAM_1(*opt) 
		 * 					}<code>
		 * <br>S2C: <br><code>{ Message: MESSAGE_ID = SEND_PUBLIC_CHAT,<br> 
		 * 								 DATA = SENDER_ID, STRING_PARAMETER_1
		 * 					}<code>
		 */
		public static final short COMMAND_CHAT_ROOM = 0x64;

		public static final short COMMAND_GET_ALL_ROOM = 0x65;
		/*
		 * Lấy danh sách room của 1 service C2S : { Message: MESSAGE_ID =
		 * GET_ALL_ROOM, DATA = SERVICE_ID, PAGE_ORDER } S2C : { Message:
		 * MESSAGE_ID = GET_ALL_ROOM, DATA = PAGE_SIZE, ROOM_LIST, ERROR_CODE }
		 * 
		 * Nếu client join room theo id hoặc name thì dùng cơ chế của smartfox
		 * 
		 * Lỗi: Không còn phòng trống
		 */

		public static final short COMMAND_JOIN_ROOM = 0x66;
		/*
		 * Client chơi nhanh C2S : { Message: MESSAGE_ID = JOIN_ROOM, DATA =
		 * SERVICE_ID, ROOM_ID } S2C : { Message: MESSAGE_ID = JOIN_ROOM, DATA =
		 * ERROR_CODE }
		 * 
		 * - Server sẽ cho Client join vào phòng được chỉ định - Nếu hết bàn
		 * Server trả về ERROR_CODE mã lỗi
		 */

		public static final short COMMAND_AUTO_JOIN_ROOM = 0x67;
		/*
		 * Client chơi nhanh C2S : { Message: MESSAGE_ID = AUTO_JOIN_ROOM, DATA
		 * = SERVICE_ID } S2C : { Message: MESSAGE_ID = AUTO_JOIN_ROOM, DATA =
		 * ERROR_CODE }
		 * 
		 * - Server sẽ tìm 1 bàn trống cho Client join vào - Nếu hết bàn Server
		 * trả về ERROR_CODE mã lỗi
		 */

		public static final short COMMAND_CREATE_ROOM = 0x68;
		/*
		 * Client gửi lệnh Tạo Bàn C2S : { Message: MESSAGE_ID = CREATE_ROOM
		 * DATA = SERVICE_ID, BETCOIN, TIMEOUT, RULES_IN_GAME } S2C : { Message:
		 * MESSAGE_ID = CREATE_ROOM DATA = ErrorCode } - Tạo bàn thành công:
		 * Server sẽ cho Client join vào bàn vừa tạo ; Client bắt sự kiện
		 * JOIN_ROOM của smartfox và xử lý tiếp. - Tạo bàn thất bại: Server trả
		 * về ErrorCode mã lỗi
		 */

		public static final short COMMAND_GET_ROOM_LIST = 0x69;
		/*
		 * Client gửi lệnh lấy danh sách bàn chơi phù hợp với level, bet C2S : {
		 * Message: MESSAGE_ID = GET_ROOM_LIST, DATA = SERVICE_ID, MONEY,
		 * PAGE_ORDER } S2C : { Message: MESSAGE_ID = GET_ROOM_LIST, DATA =
		 * PAGE_SIZE, ROOM_LIST, ERROR_CODE }
		 * 
		 * Client không gửi lên MONEY thì tìm theo tiền của user Server trả
		 * ROOM_LIST rỗng khi không có room nào đang chờ
		 */

		public static final short COMMAND_ON_RECEIVE_DATA_AFTER_RECONNECT = 0x6A;
		/*
		 * Server trả về thông tin bàn chơi hiện tại cho Client khi Client
		 * reconnect C2S : { } S2C : { Message: MESSAGE_ID = BOARD_INFO, DATA =
		 * ROOM_INFO, PLAYER_LIST, ERROR_CODE }
		 */

		public static final short COMMAND_JOIN_BOARD = 0x6B;
		/*
		 * Server trả thông tin bàn chơi cho user vừa join (bao gồm thông tin tất cả player, bàn chơi)
		 * C2S : { } S2C : { Message: MESSAGE_ID = UPDATE_BOARD_GAME, DATA =
		 * PLAYER_LIST, GAME_DATA, ERROR_CODE }
		 */

		public static final short COMMAND_UPDATE_ROOM_LIST = 0x6C;
		/*
		 * Server trả thông tin user vừa join cho tất car user đang ở trong bàn
		 * C2S : { } S2C : { Message: MESSAGE_ID = UPDATE_ROOM_LIST, DATA =
		 * ROOM_INFO, PLAYER_LIST, ERROR_CODE }
		 */

		public static final short COMMAND_GET_USERS_IN_LOBBY = 0x6D;
		/*
		 * Server trả thông tin của tất cả user đang ở phòng chờ C2S : {
		 * Message: MESSAGE_ID = GET_USERS_IN_LOBBY, DATA = SERVICE_ID,
		 * PAGE_ORDER } S2C : { Message: MESSAGE_ID = GET_USERS_IN_LOBBY, DATA =
		 * PAGE_SIZE, PLAYER_LIST, ERROR_CODE }
		 */

		public static final short COMMAND_INVITE_TO_PLAY = 0x6E;
		/*
		 * Server response nội dung popup cho user gửi lời mời và user nhận lời
		 * mời. C2S : { Message: MESSAGE_ID = INVITE_FRIEND, DATA = INVITING_ID,
		 * INVITED_ID } S2C : { Message: MESSAGE_ID = INVITE_FRIEND, DATA =
		 * IS_INVITING, ERROR_CODE }
		 */

		public static final short COMMAND_INVITE_TO_PLAY_CONFIRM = 0x6F;
		/*
		 * Client gửi request xác nhận lời mời chơi. Server sẽ cho 2 user đó
		 * join vào phòng chơi C2S : { Message: MESSAGE_ID =
		 * INVITE_FRIEND_CONFIRM, DATA = SERVICE_ID, INVITING_ID, INVITED_ID }
		 * S2C : { Message: MESSAGE_ID = INVITE_FRIEND_CONFIRM, DATA =
		 * ERROR_CODE }
		 */

		public static final short COMMAND_CHANGE_LANGUAGE = 0x70;
		/*
		 * Client gửi request xác nhận lời mời chơi. Server sẽ cho 2 user đó
		 * join vào phòng chơi C2S : { Message: MESSAGE_ID = CHANGE_LANGUAGE,
		 * DATA = LANGUAGE_TYPE } S2C : { Message: MESSAGE_ID = CHANGE_LANGUAGE,
		 * DATA = ERROR_CODE }
		 */
		
		// TODO đụng key với command COMMAND_UPDATE_ROOM_INFO
		public static final short COMMAND_SWITCH_SPECTATOR_TO_PLAYER = 0x71;
		/*
		 * Server trả thông tin user vừa join cho tất cả user đang ở trong bàn
		 * C2S : { } S2C : { Message: MESSAGE_ID = COMMAND_SWITCH_SPECTATOR_TO_PLAYER, DATA =
		 * PLAYER_LIST, GAME_DATA, ERROR_CODE }
		 */

		/**
		 * Cập nhật thông tin bàn chơi. Chỉ được cập nhật khi game chưa bắt đầu,
		 * và thỏa số tiền bet. <br>
		 * C2S : <br>
		 * <code>{ Message: MESSAGE_ID = UPDATE_ROOM_INFO,<br>
		 * 					 DATA       = SERVICE_ID, ROOM_ID, BETCOIN, TIMEOUT,RULES_IN_GAME
		 * 					}<code>
		 * <br>S2C: <br><code>{ Message: MESSAGE_ID = UPDATE_ROOM_INFO,<br> 
		 * 								 DATA = SERVICE_ID, ROOM_ID, BETCOIN, TIMEOUT,RULES_IN_GAME
		 * 					}<code>
		 */
		public static final short COMMAND_UPDATE_ROOM_INFO = 0x71;

		/************* MESSAGE IN GAME **************/
		public static final short COMMAND_REQUEST_IN_GAME = 0x72;
		/*
		 * Client gửi request trong game C2S : { Message: MESSAGE_ID =
		 * REQUEST_IN_GAME, DATA = SERVICE_ID } S2C : { Message: MESSAGE_ID =
		 * REQUEST_IN_GAME, DATA = ERROR_CODE, ROOM_LIST, USER_PROFILE}
		 */

		public static final short COMMAND_START_GAME = 0x73;
		/*
		 * Client gửi lệnh START_GAME C2S : { Message: MESSAGE_ID = START_GAME
		 * DATA = BETCOIN, TIMEOUT, RULES_IN_GAME } S2C : { Message: MESSAGE_ID
		 * = START_GAME DATA = } - Start Game thành công: Server sẽ cho Client
		 * ..; - Start Game thất bại: Server trả về ErrorCode mã lỗi
		 */
		public static final short COMMAND_PLAY_MOVE = 0x74;
		public static final short COMMAND_END_GAME = 0x75;
		public static final short COMMAND_DOWN_CHESS = 0x76;

		public static final short COMMAND_READY = 0x77;
		/*
		 * Client gửi lệnh READY để chơi có thể bắt đầu chơi C2S : { Message:
		 * MESSAGE_ID = READY DATA = BETCOIN, TIMEOUT, RULES_IN_GAME } S2C : {
		 * Message: MESSAGE_ID = START_GAME DATA = }
		 */

		public static final short COMMAND_BOARD_INFO = 0x78;

	}

	public class IwinKey {
		// TODO move sang file khác. ROOM_INFO là object property của Room
		public static final short ROOM_INFO = 5;

		public static final short KEYI_TIMEOUT = 0x23;
		public static final short KEYI_BETCOIN = 0x24;
		public static final short KEYB_IS_OWNER = 0x25; // 1 : có || 0 : không
		public static final short KEYI_PLAYER_ID = 0x26;
		public static final short KEYR_SUB_COMMAND_IN_GAME = 0x27;
		public static final short KEYR_ACTION_IN_GAME = 0x28;
		public static final short KEYB_REMAIN_TIME = 0x29;				// (byte) thời gian còn lại
		public static final short KEYI_NEXT_PAGE = 0x2A;				
		public static final short KEYB_ON_CLUSTER = 0x2B;				
		public static final short KEYI_MAX_USER = 0x2C;				
		public static final short KEYI_COUNT = 0x2D;				
		public static final short KEYL_CASH = 0x2E;				
		public static final short KEYR_REQUEST_NEXT_CMD = 0x2F;				
		

		public static final String USER_PROFILE = "upf"; // (Object) {
															// USER_INFO,
															// FULL_LEVEL }
		public static final String FULL_LEVEL = "fl"; // (Object) { WIN, LOSE,
														// DRAW, GIVE_UP,
														// USER_LEVEL,
														// SERVICE_ID }
		public static final String WIN = "lw"; // (int) số ván thắng
		public static final String LOSE = "ll"; // (int) số ván thua
		public static final String DRAW = "ld"; // (int) số ván hòa
		public static final String GIVE_UP = "lgu"; // (int) số ván bỏ cuộc
		public static final String LANGUAGE_TYPE = "langt"; // (int) loại ngôn
															// ngữ (vietnames =
															// 1, english = 2)
		public static final String PLAYER_INFOS = "pifs"; // (Array) thông tin
															// của player trong
															// bàn chơi [ {
															// USER_ID,
															// PLAYER_NO,
															// POSITION, MONEY
															// },... ]

		public static final String SENDER_ID = "sid"; // (String) định danh
														// người gửi
		public static final String SENDER_NAME = "sn"; // (String) tên người gửi

		public static final String STRING_PARAM_1 = "spr1"; // (String) tham số
															// kiểu String
		public static final String STRING_ARRAY_PARAM_1 = "sap1";// (String)
																	// tham số
																	// kiểu
																	// String
																	// Array
		public static final String BYTE_MSG_TYPE = "st"; // (byte) kiểu message
															// (P2P,P2Group,P2World)

		public static final String IS_INVITING = "iiv"; // (int) dùng để phân
														// biệt user gửi hoặc
														// nhận lời mời ( 0: gửi
														// lời mời || 1: nhận
														// được lời mời )
		public static final String INVITING_ID = "iingid"; // (int) id của user
															// gừi lời mời
		public static final String INVITED_ID = "iedid"; // (int) id của user
															// được gửi lời mời
		public static final String POPUP_CONTENT = "cont"; // (String) nội dung
															// để show lên popup
		public static final String IS_READY = "ird"; // (Boolean) Trạng thái
														// ready / not ready

		// key of properties client
		public static final String DEVICE = "dev"; // (Object) (Dành cho Server)
		public static final String DEVICE_ID = "dvid"; // (String) id của thiết
														// bị
		public static final String LOCALE = "loc"; // (Object) sử dụng ngôn ngữ
													// (Dành cho Server)
		public static final String CLIENT_INFO = "cli"; // (Object) thông tin
														// của thiết bị (Dành
														// cho Server)

		// Client Info
		public static final String PROTOCOL_VER = "pcv"; // (String) phiên bản
															// protocol
		public static final String CARRIER = "cr"; // (String) nhà mạng
		public static final String FLATFORM = "ff"; // (String) nền tảng
		public static final String FLATFORM_VER = "ffv"; // (String) phiên bản
															// nền tảng
		public static final String CLIENT_VER = "cv"; // (String) phiên bản app
		public static final String OS = "os"; // (int) hệ điều hành (1: android,
												// 2: ios, 3: desktop 4 :
												// windowphone, 5: Macos)
		public static final String PHONE_NUMBER = "pn"; // (String) số phone
		public static final String NETWORK = "nw"; // (String) mạng đang sử dụng
													// (3G, Wifi)
		public static final String IMEI = "imei"; // (String) số Imei của phone
		public static final String PROVIDER = "pro"; // (String) nhà đối tác,
														// phát hành
		public static final String MODEL = "md"; // (String) Đời máy của phone
		// ClientInfoObject
		public static final String REFCODE = "rce"; // (String)
		public static final String WIDTH = "wih"; // (int) chiều ngang của thiết
													// bị
		public static final String HEIGHT = "het"; // (int) chiều cao của thiết
													// bị
		public static final String EXIST_HACK_IN_APP = "ehia"; // (int) có hack
																// trong app?
																// (1: có, 0:
																// không)
		public static final String NOTIFY_KEY = "nky"; // (String)
		public static final String SUB_PROVIDER = "spr"; // (int)
		public static final String DEVICE_UDID = "dud"; // (String)
		public static final String OS_VERSION = "osv"; // (String) version hệ
														// điều hành
		public static final String DEVICE_NAME = "dvn"; // (String) tên của
														// thiết bị
		public static final String IS_JAIBREAK = "ijb"; // (int) có jaibreak
														// hoặc root? (1: có, 0:
														// không)
		public static final String BUNDLE_ID = "bid"; // (String)

		public static final String GAME_DATA = "gdt"; // (SFSArray) chứa toàn bộ
														// thông tin game data
														// trong bàn chơi
		public static final String PLAYMOVE_ID = "pmid"; // (int) id PLAYMOVE
															// của game
		public static final String STEP = "st";

		public static final String PAGE_SIZE = "ps"; // (int) số trang
		public static final String PAGE_ORDER = "pod"; // (int) thứ tự trang (default: 1)
		
		
	}

}
