/**
 * 
 */
package com.rockitgaming.server.gamelib;

import java.util.List;

import com.rockitgaming.server.gamelib.api.GameAPI;
import com.rockitgaming.server.gamelib.result.IPlayMoveResult;
import com.rockitgaming.server.gamelib.result.StartGameResult;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;


/**
 *  Class controller của GameLogic
 * 
 * @author thuctvd
 *
 */
public interface GameInterface {
	/**
	 * Xử lý khi user vào bàn chơi
	 */
	public void onUserJoinBoard(User userJoin);
	/**
	 * Xử lý khi user thoát khỏi bàn chơi
	 */
	public void onUserLeaveBoard(User userLeave);
	/**
	 * Xử lý start game
	 * @param owner	- chủ bàn
	 * @param listPlayer - danh sách người chơi
	 * @param extraData - các Extra Data từ Client gửi lên tùy theo từng game muốn sử dụng
	 */
	public StartGameResult onStartGame(User owner, List<User> listPlayer, Message extraData);
	/**
	 * Xử lý các nước đi trong game
	 * @param sender 
	 */
	public IPlayMoveResult onPlayMoveHandle(User sender, Message data);
	/**
	 * Lấy dữ liệu GameData cho người xem 
	 */
	public Object getGameDataForViewer();	
	/**
	 * Set người được quyền chia bài
	 * @param paramLong
	 */
	public void setDealer(long paramLong);
	/**
	 * Set GameAPI cho game
	 * @param api
	 */
	public void setApi(GameAPI api);
	/**
	 * Lấy thông tin gchoi7trong bàn đang chơi
	 * PlayerList - DataInGame....
	 * @return
	 */
	public void getGameData(Message data);
	
	/**
	 * Xử lý khi hết thời gian của 1 lượt đi
	 */
	public IPlayMoveResult timeout();
	public void startCountdownStartGame(long timeOut);
	
}
