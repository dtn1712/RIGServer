/**
 * 
 */
package com.rockitgaming.server.gamelib.eventhandler;

import java.util.List;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.event.CoreEventParam;
import com.rockitgaming.server.event.ICoreEvent;
import com.rockitgaming.server.extensions.IServerEventHandler;
import com.rockitgaming.server.extensions.MEExtension;
import com.rockitgaming.server.gamelib.GameExtension;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.gamelib.model.RoomInfo;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author thuctvd
 *
 */
public class UserJoinRoomExtensionHandler implements IServerEventHandler {

	@Override
	public void handleServerEvent(ICoreEvent event) {
		User user = (User) event.getParameter(CoreEventParam.USER);
		Room room = (Room) event.getParameter(CoreEventParam.ROOM);

		GameExtension gameExtension = (GameExtension) room.getExtension();
		// TODO Kiem tra còn đủ tiền chơi ko

		// Trả về thông tin trong bàn
		List<User> players = room.getPlayersList();
		Message message = DefaultMessageFactory.createMessage(IwinCommand.COMMAND_JOIN_BOARD);
		
		// không cần trả về chỗ này vì user đang là người xem
		// SystemEventType.KEYI_USER_ID dánh sách userId của player trong bàn
		// message.putInt(SystemEventType.KEYI_USER_ID, user.getUserId());
		toMessageArrayPlayers(message, players, room.getOwner());
		gameExtension.gameController.getGameData(message);

		Object roomInfoObj = room.getProperty(IwinKey.ROOM_INFO);
		if (roomInfoObj != null) {
			RoomInfo roomInfo = (RoomInfo) roomInfoObj;
			message.putInt(SystemEventType.KEYI_ROOM_ID, room.getId());
			message.putString(SystemEventType.KEYS_ROOM_NAME, room.getName());
			message.putInt(IwinKey.KEYI_BETCOIN, roomInfo.getBetCoin());
			message.putInt(IwinKey.KEYI_TIMEOUT, roomInfo.getTimeOut());
		}

		gameExtension.sendResponse(message, user);
		gameExtension.gameController.onUserJoinBoard(user);
	}


	@Override
	public void setParentExtension(MEExtension meExtesion) {
		// TODO Auto-generated method stub

	}


	@Override
	public MEExtension getParentExtension() {
		// TODO Auto-generated method stub
		return null;
	}


	/**
	 * Parse data gửi về cho client
	 * 
	 * @param players
	 * @param owner
	 * @return
	 */
	private void toMessageArrayPlayers(Message data, List<User> players, User owner) {
		for (User player : players) {
			data.putInt(SystemEventType.KEYI_USER_ID, player.getUserId());
			data.putString(SystemEventType.KEYS_USERNAME, player.getUserName());
			data.putString(SystemEventType.KEYS_AVATAR, player.getAvatar());
			data.putLong(SystemEventType.KEYL_MONEY, player.getMoney());
			data.putByte(IwinKey.KEYB_IS_OWNER, (player == owner) ? (byte) 1 : (byte) 0);
			data.putInt(IwinKey.KEYI_PLAYER_ID, player.getPlayerId());
		}
	}

}
