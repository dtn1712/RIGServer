/**
 * 
 */
package com.rockitgaming.server.gamelib.eventhandler;

import com.rockitgaming.server.extensions.BaseClientRequestHandler;
import com.rockitgaming.server.gamelib.GameController;
import com.rockitgaming.server.gamelib.GameExtension;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.util.Reporter;

/**
 * Class xử lý tất cả các request khi user đang trong bàn chơi Game
 * 
 * @author vutp
 *
 */
public class InGameRequestHandler extends BaseClientRequestHandler {

	@Override
	protected void init() {

	}


	@Override
	public void handleClientRequest(User user, IMessage message) {
		Reporter.infor("[GameExtension - ClientRequestHandler] - ProcessMessage: Id=" + message.getCommandId() + " ----Data=" + message.toString());

		GameExtension gameExt = (GameExtension) getParentExtension();
		GameController gameController = gameExt.gameController;
		// Reporter.infor("[GameExtension----- ClientRequestHandler] - ProcessMessage: ROOMID --- :"
		// + gameController.getRoom().getId());

		short commandInGame = message.getShort(IwinKey.KEYR_SUB_COMMAND_IN_GAME);
		switch (commandInGame) {
		case IwinCommand.COMMAND_PLAY_MOVE:
			gameController.onPlayMove(user, (Message) message);
			break;
		}

	}
}
