package com.rockitgaming.server.gamelib.eventhandler;

import java.util.List;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.entities.manager.GameManager;
import com.rockitgaming.server.exceptions.MEJoinRoomException;
import com.rockitgaming.server.extensions.BaseClientRequestHandler;
import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.model.cluster.ClusterRoom;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.key.ErrorCode;
import com.rockitgaming.server.netty.core.socket.SocketServer;
import com.rockitgaming.server.netty.core.util.Reporter;
import com.rockitgaming.server.services.IClusterService;
import com.rockitgaming.server.services.TokenFactory;

/**
 * 
 * Tính năng chơi Ngay - Chọn phòng bất kì phù hợp cho user join vào
 * 
 * @param sender
 * @param data
 * 
 * @author thuctvd
 *
 */
public class AutoJoinRoomRequestHandler extends BaseClientRequestHandler {

	private IClusterService clusterService;


	@Override
	protected void init() {
		clusterService = SocketServer.getInstance().getClusterService();
	}


	@Override
	public void handleClientRequest(User user, IMessage message) {
		byte gameId = user.getCurrentGameId();
		if (gameId == -1) {
			send(DefaultMessageFactory.createErrorMessage(ErrorCode.USER_NOT_JOIN_GAME, "User has not joined game yet"), user);
			return;
		}

		List<Room> rooms = GameManager.getInstance().getAvailableRooms(gameId, user.getMoney());
		if (rooms.isEmpty()) {
			send(DefaultMessageFactory.createErrorMessage(ErrorCode.ROOM_LIST_EMPTY, "No available rooms for user"), user);
			return;
		}

		Room room = rooms.get(0);
		// trường hợp local còn phòng trống
		if (room != null) {
			// int randRoom = new Random().nextInt(rooms.size());
			// Room room = rooms.get(randRoom);
			try {
				getParentExtension().getApi().joinRoom(user, room, true, "");
			} catch (MEJoinRoomException e) {
				Reporter.debug("ClientRequestController - joinRoom ERROR", e);
			}
		} else {
			// trường hợp local hết phòng, tìm phòng trống trên cluster
			ClusterRoom clusterRoom = clusterService.findRoom(ServerConfig.getServerHost());
			if (clusterRoom != null) {
				clusterRoom.setClusterId(user.getCurrentGameId(), clusterRoom.getId());
				send(TokenFactory.createChangeServerMessage(user, clusterRoom.getIpAddress(), clusterRoom.getId()), user);
			}
		}
	}

}
