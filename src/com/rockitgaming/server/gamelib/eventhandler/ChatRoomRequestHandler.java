package com.rockitgaming.server.gamelib.eventhandler;

import java.util.List;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.extensions.BaseClientRequestHandler;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public class ChatRoomRequestHandler extends BaseClientRequestHandler {

	@Override
	protected void init() {

	}


	@Override
	public void handleClientRequest(User user, IMessage message) {
		String chatText = message.getString(SystemEventType.KEYS_MSG);
		Room room = user.getLastJoinedRoom();
		if (room.isGame()) {
			List<User> userList = room.getUserList();
			userList.remove(user);
			Message response = DefaultMessageFactory.responseMessage(IwinCommand.COMMAND_CHAT_ROOM);
			response.putString(SystemEventType.KEYS_USERNAME, user.getUserName());
			response.putString(SystemEventType.KEYS_MSG, chatText);
			send(message, userList);
		}
	}

}
