/**
 * 
 */
package com.rockitgaming.server.gamelib.eventhandler;

import java.util.List;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.event.CoreEventParam;
import com.rockitgaming.server.event.ICoreEvent;
import com.rockitgaming.server.extensions.IServerEventHandler;
import com.rockitgaming.server.extensions.MEExtension;
import com.rockitgaming.server.gamelib.GameExtension;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author thuctvd
 *
 */
public class UserLeaveRoomExtensionHandler implements IServerEventHandler {	

	@Override
	public void handleServerEvent(ICoreEvent event) {
		User user = (User) event.getParameter(CoreEventParam.USER);
		Room room = (Room) event.getParameter(CoreEventParam.ROOM);
		if (!room.isGame()) {
			// không xử lý với trường hợp lobby
			return;
		}
		
		GameExtension gameExtension = (GameExtension) room.getExtension();
		gameExtension.gameController.onUserLeaveBoard(user);
		
		if (room.isEmpty()) {
			room.setOwner(null);
			// TODO nếu không phải room mặc định thì remove
			return;
		}
		
		// tìm và set owner mới là user có tiền nhiều nhất
		User owner = room.getOwner();
		if (owner != null && owner.getUserId() == user.getUserId()) {
			List<User> users = room.getUserList();
			owner = users.get(0);
			for (int i = 1; i < users.size(); i++) {
				user = users.get(i);
				long userMoney = user.getMoney();
				long ownerMoney = owner.getMoney();
				if (userMoney  > ownerMoney) {
					owner = user;
				}
			}
			room.setOwner(owner);
		}
		
		// Trả về thông tin trong bàn
		List<User> players = room.getPlayersList();
		if (players.isEmpty()) {
			return;
		}
		
		Message message = DefaultMessageFactory.createMessage(IwinCommand.COMMAND_JOIN_BOARD);
		toMessageArrayPlayers(message, players, owner);
		gameExtension.gameController.getGameData(message);
		gameExtension.sendResponseForListUser(message, players);
	}

	@Override
	public void setParentExtension(MEExtension meExtesion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MEExtension getParentExtension() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Parse data gửi về cho client
	 * @param players
	 * @param owner
	 * @return
	 */
	private void toMessageArrayPlayers(Message message, List<User> players, User owner) {
		for (User player : players) {
			message.putInt(SystemEventType.KEYI_USER_ID, player.getUserId());
			message.putString(SystemEventType.KEYS_USERNAME, player.getUserName());
			message.putString(SystemEventType.KEYS_AVATAR, player.getAvatar());
			message.putLong(SystemEventType.KEYL_MONEY, player.getMoney());
			message.putByte(IwinKey.KEYB_IS_OWNER, (player == owner) ? (byte) 1 : (byte) 0);
			message.putInt(IwinKey.KEYI_PLAYER_ID, player.getPlayerId());
		}
	}
	
}
