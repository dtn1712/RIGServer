package com.rockitgaming.server.gamelib.eventhandler;

import java.util.List;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.entities.manager.IRoomManager;
import com.rockitgaming.server.extensions.BaseClientRequestHandler;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.model.RoomPage;
import com.rockitgaming.server.model.cluster.ClusterRoom;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.socket.SocketServer;
import com.rockitgaming.server.services.IClusterService;

/**
 * @author lamhm
 *
 */
public class GetRoomListHandler extends BaseClientRequestHandler {
	private IClusterService clusterService;
	private IRoomManager roomManager;


	@Override
	protected void init() {
		clusterService = SocketServer.getInstance().getClusterService();
		roomManager = SocketServer.getInstance().getRoomManager();
	}


	@Override
	public void handleClientRequest(User user, IMessage message) {
		Message response = DefaultMessageFactory.responseMessage(IwinCommand.COMMAND_GET_ROOM_LIST);
		//TODO hỗ trợ lấy theo page
		List<Room> roomList = roomManager.getRoomList();
		for (Room room : roomList) {
			int size = room.getPlayersList().size();
			if (!room.isGame()) {
				size--;
				continue;
			}

			response.putInt(SystemEventType.KEYI_ROOM_ID, room.getId());
			response.putString(SystemEventType.KEYS_ROOM_NAME, room.getName());
			response.putInt(IwinKey.KEYI_MAX_USER, room.getMaxUsers());
			response.putInt(IwinKey.KEYI_COUNT, size);
			response.putLong(IwinKey.KEYL_CASH, 10);
			response.putString(SystemEventType.KEYS_IP_ADDRESS, room.getHost());
		}

		RoomPage<ClusterRoom> roomPage = clusterService.getRoomPage(0, true);
		if (roomPage != null) {
			List<ClusterRoom> rooms = roomPage.getRooms();
			for (ClusterRoom clusterRoom : rooms) {
				response.putInt(SystemEventType.KEYI_ROOM_ID, clusterRoom.getId());
				response.putString(SystemEventType.KEYS_ROOM_NAME, clusterRoom.getName());
				response.putInt(IwinKey.KEYI_MAX_USER, clusterRoom.getMaxUsers());
				response.putInt(IwinKey.KEYI_COUNT, clusterRoom.getCurrentUsers());
				response.putLong(IwinKey.KEYL_CASH, clusterRoom.getMinCash());
				response.putString(SystemEventType.KEYS_IP_ADDRESS, clusterRoom.getIpAddress());
			}
			response.putInt(IwinKey.KEYI_NEXT_PAGE, roomPage.getNextPage());
			response.putByte(IwinKey.KEYB_ON_CLUSTER, (byte) 1);
		}

		send(response, user);
	}

}
