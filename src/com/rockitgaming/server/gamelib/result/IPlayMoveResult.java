package com.rockitgaming.server.gamelib.result;

import java.util.List;

import com.rockitgaming.server.gamelib.GameController;

public interface IPlayMoveResult {
	
	public void handleResult(GameController gameController);
	
	public void addChildResult(IPlayMoveResult result);
	
	public List<IPlayMoveResult> getChildrenResult();
	
}