/**
 * 
 */
package com.rockitgaming.server.gamelib.result;

import java.util.ArrayList;
import java.util.List;

import com.rockitgaming.server.gamelib.GameController;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.util.Reporter;

/**
 * @author thuctvd
 *
 */
public class EndGameResult implements IPlayMoveResult {

	// List kết quả của từng user
	private ArrayList<UserResult> userResults = new ArrayList<UserResult>();
	// Thông tin riêng của từng game
	private Message extraDataResult = new Message();
	
	@Override
	public void handleResult(GameController gameController) {
		for (UserResult userResult : userResults) {
			User user = gameController.getRoom().getUserById(userResult.getUserId());
			String desc = "Ket thuc game";
//			gameController.getApi().updateUserLevel(iwinUser, userResult.getResultType(), userResult.getExp(), userResult.getExtraDataLevel());
			// FIXME chỗ này đang bị lỗi
			// gameController.getApi().updateUserMoney(user, (int)userResult.getMoney(), ResultType.getMoneyReason(userResult.getResultType()), desc);
			Reporter.debug("update iwin user: " + userResult.getUserId());
		}
		
		Reporter.serverInfo("-----> END_GAME: " + extraDataResult.toString());
		gameController.getApi().sendAllInRoom(extraDataResult);
		gameController.startCountdownStartGame(5000);
	}

	@Override
	public void addChildResult(IPlayMoveResult result) {
		throw new Error("Could not add child, because this is not ListPlayMoveResult");
	}

	@Override
	public List<IPlayMoveResult> getChildrenResult() {
		return null;
	}
	
	public void addUserResult(UserResult userResult) {
		userResults.add(userResult);
	}

	public Message getExtraDataResult() {
		return extraDataResult;
	}

	public void setExtraDataResult(Message extraDataResult) {
		this.extraDataResult = extraDataResult;
	}
	
}