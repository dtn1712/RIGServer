package com.rockitgaming.server.extensions;

import java.io.Serializable;

public final class ExtensionSettings implements Serializable {
	private static final long serialVersionUID = 2896660214476996410L;
	
	public String name = "";
	public String type = "JAVA";
	public String file = "";
	public String propertiesFile = "";
	public String reloadMode = "AUTO";
}
