package com.rockitgaming.server.extensions;

import com.rockitgaming.server.event.ICoreEvent;

/**
 * @author lamhm
 *
 */
public abstract interface IServerEventHandler {
	public abstract void handleServerEvent(ICoreEvent event);


	public abstract void setParentExtension(MEExtension meExtesion);


	public abstract MEExtension getParentExtension();
}
