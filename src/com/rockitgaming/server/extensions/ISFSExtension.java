package com.rockitgaming.server.extensions;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.event.ICoreEvent;
import com.rockitgaming.server.exceptions.MEException;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

public abstract interface ISFSExtension {
	public abstract void init();


	public abstract void destroy();


	public abstract String getName();


	public abstract void setName(String paramString);


	public abstract String getExtensionFileName();


	public abstract void setExtensionFileName(String paramString);


	public abstract String getPropertiesFileName();


	public abstract void setPropertiesFileName(String paramString) throws IOException;


	public abstract Properties getConfigProperties();


	public abstract boolean isActive();


	public abstract void setActive(boolean paramBoolean);


	public abstract Room getCurrentRoom();


	public abstract void setCurrentRoom(Room paramRoom);


	public abstract ExtensionReloadMode getReloadMode();


	public abstract void setReloadMode(ExtensionReloadMode paramExtensionReloadMode);


	/**
	 * Phương thức xử lý của mỗi controller
	 * 
	 * @param user
	 * @param message
	 * @throws MEException
	 */
	public abstract void handleClientRequest(User user, IMessage message) throws MEException;


	/**
	 * Nhận và xử lý event từ system
	 * 
	 * @param event
	 */
	public abstract void handleServerEvent(ICoreEvent event);


	public abstract Object handleInternalMessage(String paramString, Object paramObject);


	/**
	 * Gửi một message đến một player
	 * 
	 * @param message
	 * @param receiver
	 */
	public abstract void send(IMessage message, User receiver);


	/**
	 * Gửi một message đến nhiều player
	 * 
	 * @param message
	 * @param receivers
	 */
	public abstract void send(IMessage message, List<User> receivers);
}
