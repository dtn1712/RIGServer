package com.rockitgaming.server.extensions;

import java.util.List;

import org.slf4j.Logger;

import com.rockitgaming.server.api.IMEApi;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public abstract class BaseClientRequestHandler implements IClientRequestHandler {
	private MEExtension parentExtension;
	
	public BaseClientRequestHandler(){
		init();
	}
	
	protected abstract void init();

	public MEExtension getParentExtension() {
		return parentExtension;
	}


	public void setParentExtension(MEExtension ext) {
		parentExtension = ext;
	}


	protected IMEApi getApi() {
		return parentExtension.sfsApi;
	}


	protected void send(IMessage message, User recipient) {
		parentExtension.send(message, recipient);
	}


	protected void send(IMessage message, List<User> recipients) {
		parentExtension.send(message, recipients);
	}


	protected Logger getLogger() {
		return parentExtension.getLogger();
	}
}
