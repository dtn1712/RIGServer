package com.rockitgaming.server.extensions;

public enum ExtensionReloadMode
{
  AUTO,  MANUAL,  NONE;
}

