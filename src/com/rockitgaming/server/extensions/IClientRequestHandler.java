package com.rockitgaming.server.extensions;

import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public abstract interface IClientRequestHandler {
	public abstract void handleClientRequest(User user, IMessage message);


	public abstract void setParentExtension(MEExtension meExtension);


	public abstract MEExtension getParentExtension();
}
