package com.rockitgaming.server.extensions;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.api.IMEApi;
import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.exceptions.MERuntimeException;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.socket.SocketServer;

public abstract class BaseMEExtension implements ISFSExtension {
	private String name;
	private String fileName;
	private String configFileName;
	private ExtensionLevel level;
	private ExtensionType type;
	private Room currentRoom = null;
	private volatile boolean active;
	private SocketServer sfs = null;
	private Properties configProperties;
	private ExtensionReloadMode reloadMode;
	private String currentPath;
	protected volatile int lagSimulationMillis = 0;
	protected volatile int lagOscillation = 0;
	private final Logger logger;
	private Random rnd;
	protected final IMEApi sfsApi;


	public BaseMEExtension() {
		this.logger = LoggerFactory.getLogger("Extensions");
		this.active = true;

		this.sfs = SocketServer.getInstance();
		this.sfsApi = this.sfs.getAPIManager().getMEApi();
	}


	public String getCurrentFolder() {
		return this.currentPath;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		if (this.name != null) {
			throw new MERuntimeException("Cannot redefine name of extension: " + toString());
		}
		this.name = name;

		this.currentPath = ("extensions/" + name + "/");
	}


	public String getExtensionFileName() {
		return this.fileName;
	}


	public Properties getConfigProperties() {
		return this.configProperties;
	}


	public String getPropertiesFileName() {
		return this.configFileName;
	}


	public void setPropertiesFileName(String fileName) throws IOException {
		if (this.configFileName != null) {
			throw new MERuntimeException("Cannot redefine properties file name of an extension: " + toString());
		}
		boolean isDefault = false;
		if ((fileName == null) || (fileName.length() == 0) || (fileName.equals("config.properties"))) {
			isDefault = true;
			this.configFileName = "config.properties";
		} else {
			this.configFileName = fileName;
		}
		String fileToLoad = "extensions/" + this.name + "/" + this.configFileName;
		if (isDefault) {
			loadDefaultConfigFile(fileToLoad);
		} else {
			loadCustomConfigFile(fileToLoad);
		}
	}


	public IMEApi getApi() {
		return this.sfsApi;
	}


	public Object handleInternalMessage(String cmdName, Object params) {
		return null;
	}


	private void loadDefaultConfigFile(String fileName) {
		this.configProperties = new Properties();
		try {
			this.configProperties.load(new FileInputStream(fileName));
		} catch (IOException localIOException) {
		}
	}


	private void loadCustomConfigFile(String fileName) throws IOException {
		this.configProperties = new Properties();
		this.configProperties.load(new FileInputStream(fileName));
	}


	public void setExtensionFileName(String fileName) {
		if (this.fileName != null) {
			throw new MERuntimeException("Cannot redefine file name of an extension: " + toString());
		}
		this.fileName = fileName;
	}


	public Room getCurrentRoom() {
		return this.currentRoom;
	}


	public void setCurrentRoom(Room room) {
		if (this.currentRoom != null) {
			throw new MERuntimeException("Cannot redefine parent room of extension: " + toString());
		}
		this.currentRoom = room;
	}


	public void addEventListener() {
		// this.sfs.getExtensionManager().addRoomEventListener(eventType,
		// listener, this.parentRoom);

	}


	public void removeEventListener() {
		// this.sfs.getExtensionManager().removeRoomEventListener(eventType,
		// listener, this.parentRoom);
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean flag) {
		this.active = flag;
	}


	public ExtensionLevel getLevel() {
		return this.level;
	}


	public void setLevel(ExtensionLevel level) {
		if (this.level != null) {
			throw new MERuntimeException("Cannot change level for extension: " + toString());
		}
		this.level = level;
	}


	public ExtensionType getType() {
		return this.type;
	}


	public void setType(ExtensionType type) {
		if (this.type != null) {
			throw new MERuntimeException("Cannot change type for extension: " + toString());
		}
		this.type = type;
	}


	public ExtensionReloadMode getReloadMode() {
		return this.reloadMode;
	}


	public void setReloadMode(ExtensionReloadMode mode) {
		if (this.reloadMode != null) {
			throw new MERuntimeException("Cannot change reloadMode for extension: " + toString());
		}
		this.reloadMode = mode;
	}


	@Override
	public void send(IMessage message, List<User> recipients) {
		checkLagSimulation();
		sfsApi.sendExtensionResponse(message, recipients);
	}


	@Override
	public void send(IMessage message, User recipient) {
		checkLagSimulation();
		sfsApi.sendExtensionResponse(message, recipient);
	}


	public String toString() {
		return String.format("{ Ext: %s, Type: %s, Lev: %s, %s }", new Object[] { this.name, this.type, this.level,
				this.currentRoom == null ? "{}" : this.currentRoom });
	}


	public Logger getLogger() {
		return this.logger;
	}


	protected void removeEventsForListener() {
		// this.sfs.getExtensionManager().removeListenerFromRoom(listener,
		// this.parentRoom);
	}


	private void checkLagSimulation() {
		if (this.lagSimulationMillis > 0) {
			try {
				long lagValue = this.lagSimulationMillis;
				if (this.lagOscillation > 0) {
					if (this.rnd == null) {
						this.rnd = new Random();
					}
					int sign = this.rnd.nextInt(100) > 49 ? 1 : -1;

					lagValue += sign * this.rnd.nextInt(this.lagOscillation);
				}
				if (this.logger.isDebugEnabled()) {
					this.logger.debug("Lag simulation, sleeping for: " + lagValue + "ms.");
				}
				System.out.println("Lag: " + lagValue);

				Thread.sleep(lagValue);
			} catch (InterruptedException e) {
				this.logger.warn("Interruption during lag simulation: " + e);
			}
		}
	}
}
