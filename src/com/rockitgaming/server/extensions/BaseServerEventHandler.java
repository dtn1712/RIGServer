package com.rockitgaming.server.extensions;

import java.util.List;

import org.slf4j.Logger;

import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public abstract class BaseServerEventHandler implements IServerEventHandler {
	private MEExtension parentExtension;


	public MEExtension getParentExtension() {
		return parentExtension;
	}


	public void setParentExtension(MEExtension ext) {
		parentExtension = ext;
	}


	// protected MEAPI getApi() {
	// return parentExtension.sfsApi;
	// }

	protected void send(IMessage message, User recipient) {
		// parentExtension.send(message, recipient);
	}


	protected void send(IMessage message, List<User> recipients) {
		// parentExtension.send(message, recipients);
	}


	protected void trace(Object... args) {
		// parentExtension.trace(args);
	}


	protected Logger getLogger() {
		return parentExtension.getLogger();
	}
}
