package com.rockitgaming.server.netty.core.event.system;

import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public class LogoutRequestHandler extends AbstractRequestHandler {

	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}


	@Override
	public void perform(User user, IMessage message) {
		writeMessage(user, DefaultMessageFactory.createMessage(SystemEventType.COMMAND_USER_LOGOUT));
		getMEApi().logout(user);
	}

}
