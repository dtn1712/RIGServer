package com.rockitgaming.server.netty.core.event.system;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.entities.manager.IRoomManager;
import com.rockitgaming.server.exceptions.MEJoinRoomException;
import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.socket.SocketServer;
import com.rockitgaming.server.netty.core.util.Reporter;
import com.rockitgaming.server.services.TokenFactory;

/**
 * @author lamhm
 *
 */
public class JoinRoomRequestHandler extends AbstractRequestHandler {

	private IRoomManager roomManger;


	@Override
	public void initialize() {
		roomManger = SocketServer.getInstance().getRoomManager();
	}


	@Override
	public void perform(User user, IMessage message) {
		Integer roomId = message.getInt(SystemEventType.KEYI_ROOM_ID);
		String host = message.getString(SystemEventType.KEYS_IP_ADDRESS);
		if (roomId == null || host == null) {
			// TODO send error
			return;
		}

		try {
			boolean isLocal = host.trim().equals(ServerConfig.getServerHost());
			if (isLocal) {
				Room room = roomManger.getRoomById(roomId);
				if (room != null && room.isGame() && isLocal) {
					getMEApi().joinRoom(user, room);
				} else {
					// TODO send error
				}
				return;
			}

			// TODO log, remove thông tin user trên server này
			writeMessage(user, TokenFactory.createChangeServerMessage(user, host, roomId));
		} catch (MEJoinRoomException e) {
			Reporter.getErrorLogger().error("Join room error", e);
		}
	}

}
