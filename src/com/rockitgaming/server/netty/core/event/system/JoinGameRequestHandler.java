package com.rockitgaming.server.netty.core.event.system;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.entities.manager.GameManager;
import com.rockitgaming.server.exceptions.MEJoinRoomException;
import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.key.ErrorCode;
import com.rockitgaming.server.netty.core.util.Reporter;

/**
 * @author lamhm
 *
 */
public class JoinGameRequestHandler extends AbstractRequestHandler {

	@Override
	public void initialize() {

	}


	@Override
	public void perform(User user, IMessage message) {
		Byte gameId = message.getByte(SystemEventType.KEYB_GAME_ID);
		if (!GameManager.getInstance().isGameExtRunning(gameId)) {
			writeMessage(user, DefaultMessageFactory.createErrorMessage(ErrorCode.GAME_EXT_STOPPED, "Game Extension stopped"));
		}
		Room room = GameManager.getInstance().getLobbyGame(gameId);
		if (room == null || room.isGame()) {
			writeMessage(user, DefaultMessageFactory.createErrorMessage(ErrorCode.GAME_EXT_LOBBY_NOT_FOUND, "Lobby not found"));
		}
		try {
			user.setCurrentGameId(gameId);
			getMEApi().joinRoom(user, room);
		} catch (MEJoinRoomException e) {
			Reporter.getErrorLogger().error("Join game fail", e);
		}

	}

}
