package com.rockitgaming.server.netty.core.event.system;

import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public class LeaveRoomRequestHandler extends AbstractRequestHandler {

	@Override
	public void initialize() {

	}


	@Override
	public void perform(User user, IMessage message) {
		getMEApi().leaveRoom(user, user.getLastJoinedRoom());
	}

}
