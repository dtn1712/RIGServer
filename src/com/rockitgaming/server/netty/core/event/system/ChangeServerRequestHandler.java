package com.rockitgaming.server.netty.core.event.system;

import java.util.Map;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.entities.manager.IRoomManager;
import com.rockitgaming.server.exceptions.MEJoinRoomException;
import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.socket.SocketServer;
import com.rockitgaming.server.services.TokenFactory.CustomToken;
import com.rockitgaming.server.util.GsonUtils;

/**
 * @author lamhm
 *
 */
public class ChangeServerRequestHandler extends AbstractRequestHandler {
	private IRoomManager roomManager;


	@Override
	public void initialize() {
		roomManager = SocketServer.getInstance().getRoomManager();
	}


	@Override
	public void perform(User user, IMessage message) {
		String token = message.getString(SystemEventType.KEYS_ACCESS_TOKEN);
		CustomToken customToken = GsonUtils.fromGsonString(token, CustomToken.class);
		int userId = customToken.getUserId();
		// User userDb = dataManager.getUserInfoByMoboId(moboId);
		User userDb = dataManager.getUserInfoByUserId(userId);
		try {
			if (userDb != null) {
				user.setCurrentGameId(customToken.getGameId());
				user.setLoginInfo(userDb);
				userManager.addUser(user);
				Map<Object, Object> metaData = customToken.getMetaData();
				int roomId = ((Double) metaData.get("roomId")).intValue();
				Room room = roomManager.getRoomById(roomId);
				if (room != null) {
					getMEApi().joinRoom(user, room);
				} else {
					// TODO send error
				}
			} else {
				// TODO send, log error
			}
		} catch (MEJoinRoomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
