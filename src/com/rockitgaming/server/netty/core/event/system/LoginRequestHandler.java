package com.rockitgaming.server.netty.core.event.system;

import static com.rockitgaming.server.netty.core.event.SystemEventType.COMMAND_USER_LOGIN;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYB_GAME_ID;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYI_USER_ID;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYL_MONEY;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYS_ACCESS_TOKEN;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYS_AVATAR;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYS_GAME_NAME;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYS_LANGUAGE;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYS_PASSWORD;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYS_USERNAME;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.api.ws.MoboAuthAPI;
import com.rockitgaming.server.entities.GameSettings;
import com.rockitgaming.server.entities.manager.GameManager;
import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.key.ErrorCode;
import com.rockitgaming.server.netty.core.socket.SocketServer;
import com.rockitgaming.server.netty.core.util.Reporter;
import com.rockitgaming.server.netty.core.util.StringUtils;
import com.rockitgaming.server.services.CouchbaseService;

public class LoginRequestHandler extends AbstractRequestHandler {
	private static final Logger LOG = LoggerFactory.getLogger(LoginRequestHandler.class);
	private static final int NUMBER_COUNT_UPDATE = 10;
	private Map<String, AtomicInteger> failMap;
	private CouchbaseService couchbaseService;


	@Override
	public void initialize() {
		failMap = Collections.synchronizedMap(new HashMap<String, AtomicInteger>());
		couchbaseService = SocketServer.getInstance().getCouchbaseService();
	}


	@Override
	public void perform(User user, IMessage message) {
		loginWithUserNameAndPassWord(user, message);
		// loginWithMoboAccessToken(user, message);
	}


	private void loginWithMoboAccessToken(User user, IMessage message) {
		String accessToken = message.getString(KEYS_ACCESS_TOKEN);
		MoboAuthAPI moboAuthAPI = new MoboAuthAPI();
		User userDb = null;

		accessToken = accessToken.trim();
		if (!accessToken.equals("")) {
			Integer moboId = couchbaseService.getMoboIdByToken(accessToken.trim());
			if (moboId == null) {
				if (!moboAuthAPI.authen(0, "", accessToken)) {
					responseErrorMessage(user, ErrorCode.LOGIN_MOBO_VERIRY_ACCESSTOKEN_FAILED, "Verify accessToken Failed");
					return;
				}
				moboId = Integer.valueOf(moboAuthAPI.getMoboAuthInfo().getMobo_id());
				// lần đầu put vào cache
				couchbaseService.saveToken(accessToken, moboId);
			}

			String userName = message.getString(KEYS_USERNAME);
			if (userName != null && !userName.equals("")) {
				// create new account
				userDb = dataManager.insertUser("", 0, userName, ServerConfig.serverId, "");
			} else {
				// already has account
				userDb = dataManager.getUserInfoByMoboId(moboId);
			}
		}

		// User mới - phản hồi cho client để show popup nhập nickname
		if (userDb == null) {
			responseErrorMessage(user, ErrorCode.LOGIN_MOBO_INPUT_NICKNAME, "Input NickName to create Account");
			return;
		} else {
			// Login success
			// user info
			Message response = DefaultMessageFactory.responseMessage(COMMAND_USER_LOGIN);
			response.putString(KEYS_USERNAME, userDb.getUserName());
			response.putLong(KEYL_MONEY, userDb.getMoney());
			response.putString(KEYS_AVATAR, userDb.getAvatar());
			response.putString(KEYS_LANGUAGE, "vi");
			// game list
			List<GameSettings> gameSettings = GameManager.getInstance().getAllGameSettings();
			for (GameSettings settings : gameSettings) {
				response.putString(KEYS_GAME_NAME, settings.gameNameEn);
				response.putByte(KEYB_GAME_ID, settings.gameId);
			}

			user.setLoginInfo(userDb);
			userManager.addUser(user);
			writeMessage(user, response);
		}
	}


	private void loginWithUserNameAndPassWord(User user, IMessage message) {
		String userName = message.getString(KEYS_USERNAME);
		String password = message.getString(KEYS_PASSWORD);
		String accessToken = message.getString(KEYS_ACCESS_TOKEN);
		if (accessToken == null) {
			accessToken = "";
		}
		if (!isValidateData(user, userName, password)) {
			return;
		}

		userName = userName.trim().toLowerCase();
		password = password.trim();
		accessToken = accessToken.trim();

		Reporter.infor(String.format("[FATAL] [user:%s, pass:%s]", userName, password));

		int rs = dataManager.checkLogin(userName, password);
		Reporter.infor("LoginRequestHandler, login result: " + rs);

		User userDb = null;
		if (rs == -1) {
			// sai pass
			responseErrorMessage(user, ErrorCode.LOGIN_USER_WRONG_PASS, "Wrong password");
			return;
		}

		if (rs == 1) {
			// tồn tại account này
			userDb = dataManager.getUserInfoByUserName(userName);
		} else if (rs == 0) {
			// kiểm tra tồn tại user này không
			if (dataManager.checkUserNameExist(userName)) {
				responseErrorMessage(user, ErrorCode.LOGIN_USER_NAME_EXIST, "Create new account fail! Username exist");
				return;
			}

			// tài khoản mới
			userDb = dataManager.insertUser("", 0, userName, ServerConfig.serverId, password);
		}

		if (userDb != null) {
			Message response = DefaultMessageFactory.responseMessage(COMMAND_USER_LOGIN);
			// user info
			response.putInt(KEYI_USER_ID, userDb.getUserId());
			response.putString(KEYS_USERNAME, userDb.getUserName());
			response.putLong(KEYL_MONEY, userDb.getMoney());
			response.putString(KEYS_AVATAR, userDb.getAvatar());
			response.putString(KEYS_LANGUAGE, "vi");
			// game list
			List<GameSettings> gameSettings = GameManager.getInstance().getAllGameSettings();
			for (GameSettings settings : gameSettings) {
				response.putString(KEYS_GAME_NAME, settings.gameNameEn);
				response.putByte(KEYB_GAME_ID, settings.gameId);
			}

			user.setLoginInfo(userDb);
			userManager.addUser(user);
			writeMessage(user, response);
		} else {
			LOG.error(String.format("User[%s, %s] not found!", userName, password));
			// Xử lý log khi user login fail.
			responseErrorMessage(user, ErrorCode.LOGIN_NEW_USER, "User not found!");
		}
	}


	private boolean isValidateData(User user, String userName, String password) {
		if (userName == null || userName.trim().length() <= 0 || password == null || password.trim().length() <= 0) {
			responseErrorMessage(user, ErrorCode.LOGIN_USER_EMPTY_USERNAME_OR_PASS, "Username or password is empty");
			return false;
		}

		if (userName.length() < 5 || userName.length() > 20) {
			responseErrorMessage(user, ErrorCode.LOGIN_USER_NAME_LENGTH, "Create new account fail! Username must to contain between 5 and 20");
			return false;
		}

		if (!StringUtils.checkNameValid(userName)) {
			responseErrorMessage(user, ErrorCode.LOGIN_USER_NAME_CONTAIN, "Create new account fail! Username must contain a-z, A-Z or 0-9");
			return false;
		}

		return true;
	}


	private void responseErrorMessage(User user, short errorId, String message) {
		writeMessage(user, DefaultMessageFactory.createErrorMessage(errorId, message));

		AtomicInteger integer = failMap.get(user.getClientIp());
		if (integer != null) {
			integer.incrementAndGet();
			failMap.put(user.getClientIp(), integer);
			if (integer.get() % NUMBER_COUNT_UPDATE == 0) {
				dataManager.countLoginFail(user.getClientIp(), NUMBER_COUNT_UPDATE);
			}
		} else {
			failMap.put(user.getClientIp(), new AtomicInteger(1));
		}
	}
}
