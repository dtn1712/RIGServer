package com.rockitgaming.server.netty.core.event.system;

import static com.rockitgaming.server.netty.core.event.SystemEventType.*;

import com.rockitgaming.server.netty.core.device.ClientDevice;
import com.rockitgaming.server.netty.core.device.Device;
import com.rockitgaming.server.netty.core.device.VersionManager;
import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

public class SendClientInfoRequestHandler extends AbstractRequestHandler {

	@Override
	public void initialize() {

	}


	@Override
	public void perform(User user, IMessage message) {
		ClientDevice clientDevice = new ClientDevice();
		clientDevice.setCarrier(message.getString(KEYS_DEVICE_CARRIER));
		clientDevice.setDevice(message.getString(KEYS_DEVICE_NAME));
		clientDevice.setNet(message.getString(KEYS_DEVICE_NET));
		clientDevice.setOs(message.getString(KEYS_DEVICE_OS));
		clientDevice.setOsVersion(message.getString(KEYS_OS_VERSION));
		clientDevice.setIsJaibreak(message.getByte(KEYBL_IS_JAIBREAK) == 1);
		clientDevice.setProviderID(message.getString(KEYS_PROVIDER_ID));
		clientDevice.setSubProvider(message.getString(KEYS_SUB_PROVIDER));
		clientDevice.setVersion(message.getString(KEYS_GAME_VERSION));
		clientDevice.setWidth(message.getShort(KEYR_SCREEN_WIDTH));
		clientDevice.setHeight(message.getShort(KEYR_SCREEN_HEIGHT));
		clientDevice.setIsExistHackInApp(message.getByte(KEYBL_IS_HACK_INAPP) == 1);
		clientDevice.setFirstLogin(message.getByte(KEYBL_IS_FIRST_LOGIN) == 1);
		clientDevice.setNotifyKey(message.getString(KEYS_NOTIFICATION_ID));
		clientDevice.setRefcode(message.getString(KEYS_REFCODE));
		clientDevice.setChannel(message.getString(KEYS_CHANNEL));
		Device device = new Device();
		device.setClientDevice(clientDevice);
		device.setVersion(VersionManager.getInstance().getVersion(clientDevice.getVersion()));
		user.setDevice(device);
	}

}
