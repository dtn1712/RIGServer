package com.rockitgaming.server.netty.core.event.system;

import static com.rockitgaming.server.netty.core.event.SystemEventType.COMMAND_USER_CONNECT;

import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public class ConnectRequestHandler extends AbstractRequestHandler {

	@Override
	public void initialize() {

	}


	@Override
	public void perform(User user, IMessage message) {
		// khi kết nối chưa có user, tạo user giả để response về
		User client = new User();
		client.setSessionId(message.getSessionId());

		writeMessage(client, DefaultMessageFactory.responseMessage(COMMAND_USER_CONNECT));
	}

}
