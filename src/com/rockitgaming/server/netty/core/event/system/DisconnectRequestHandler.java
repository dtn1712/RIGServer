package com.rockitgaming.server.netty.core.event.system;

import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.netty.core.event.AbstractRequestHandler;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public class DisconnectRequestHandler extends AbstractRequestHandler {

	@Override
	public void initialize() {

	}


	@Override
	public void perform(User user, IMessage message) {
		// xử lý log user session khi user disconnect
		dataManager.checkLogout(user.getUserId(), ServerConfig.serverId, user.getTimeOnline());
		getMEApi().leaveRoom(user, user.getLastJoinedRoom(), false, false);
		// TODO còn bug
		// SessionLog sessionLog = new SessionLog(user, user.getDevice());
		// ThreadPoolManager.getInstance().executeLogGeneral(new
		// SessionLogTask(sessionLog));
		// Reporter.infor("User " + user.getUserName() + " disconnected");

		userManager.removeUser(user.getUserId());
	}

}
