package com.rockitgaming.server.netty.core.event;

import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public interface IRequestHandler {
	/**
	 * Init các service, attribute cho một handler cụ thể
	 */
	public void initialize();


	/**
	 * Thực thi message request
	 * 
	 * @param user đối tượng lưu trữ thông tin của user request
	 * @param message message user gửi lên server
	 */
	public void perform(User user, IMessage message);
}
