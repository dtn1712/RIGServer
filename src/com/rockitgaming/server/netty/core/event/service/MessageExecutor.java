package com.rockitgaming.server.netty.core.event.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.exceptions.MEException;
import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.RequestComparator;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * Class xử lý các message từ queue.<br>
 * Mọi message được nhận từ MessageHandler được push vào queue và chờ
 * MesssageExecutor xử lý
 * 
 * @author lamhm
 *
 */
public class MessageExecutor {
	private static final Logger LOG = LoggerFactory.getLogger(MessageExecutor.class);
	private int threadPoolSize;
	private ExecutorService threadPool;
	private BlockingQueue<IMessage> requestQueue;


	public MessageExecutor() {
		threadPoolSize = ServerConfig.msgThreadPoolSize;
		requestQueue = new PriorityBlockingQueue<IMessage>(50, new RequestComparator());
		threadPool = Executors.newFixedThreadPool(threadPoolSize, new MessageExecutorThreadFactory("extension-message-executor"));

		for (int i = 0; i < threadPoolSize; i++) {
			threadPool.execute(new MessageRunnable());
		}
	}

	private class MessageRunnable implements Runnable {
		private IMessage message = null;


		@Override
		public void run() {
			while (true) {
				try {
					message = requestQueue.take();
					User user = message.getUser();
					Room lastJoinedRoom = user.getLastJoinedRoom();
					if (lastJoinedRoom != null) {
						lastJoinedRoom.getExtension().handleClientRequest(user, message);
					} else {
						LOG.error("User đã rời tất cả room: " + user);
					}
				} catch (RuntimeException ex) {
					LOG.error(ex.toString());
				} catch (InterruptedException | MEException e) {
					LOG.error(e.toString());
				}catch (Exception e) {
					LOG.error(e.toString());
				}
			}
		}
	}


	public void addExtensionMessage(Message message) {
		try {
			// TODO check max pool size
			requestQueue.put(message);
		} catch (InterruptedException e) {
			LOG.error(e.toString());
		}
	}


	public void shutdown() {
		threadPool.shutdown();
	}

}
