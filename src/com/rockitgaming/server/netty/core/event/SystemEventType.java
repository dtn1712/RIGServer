package com.rockitgaming.server.netty.core.event;

/**
 * Class định nghĩa các Event của hệ thống. Các Event của hệ thống sẽ được xử lý
 * bởi
 * 
 * @author lamhm
 *
 */
public class SystemEventType {
	public static final byte PROTOCOL_VERSION = 1;

	/************************ COMMAND ID *********************/
	public static final short COMMAND_ERROR = -1;
	public static final short COMMAND_USER_CONNECT = 0x00;
	public static final short COMMAND_USER_DISCONNECT = 0x01;
	public static final short COMMAND_USER_LOGIN = 0x02;
	public static final short COMMAND_USER_LOGOUT = 0x03;
	public static final short COMMAND_USER_JOIN_ROOM = 0x04;
	public static final short COMMAND_USER_LEAVE_ROOM = 0x05;

	public static final short COMMAND_USER_JOIN_GAME = 0x06;
	public static final short COMMAND_USER_SEND_INFO = 0x08;

	public static final short COMMAND_SPECTATOR_TO_PLAYER = 0x09;
	public static final short COMMAND_PLAYER_TO_SPECTATOR = 0x0A;
	public static final short COMMAND_USER_CREATE_ROOM = 0x0B;
	public static final short COMMAND_USER_CHANGE_SERVER = 0x0C;

	/************************ KEY ****************************/
	public static final short KEYI_USER_ID = 0x01;
	public static final short KEYL_SESSION_ID = 0x02;

	// Login
	public static final short KEYS_USERNAME = 0x03;
	public static final short KEYS_PASSWORD = 0x04;
	public static final short KEYS_GAME_NAME = 0x05;
	public static final short KEYS_ACCESS_TOKEN = 0x20;

	// send info
	public static final short KEYS_DEVICE_CARRIER = 0x06;
	public static final short KEYS_DEVICE_NAME = 0x07;
	public static final short KEYS_DEVICE_NET = 0x08;
	public static final short KEYS_DEVICE_OS = 0x09;
	public static final short KEYS_OS_VERSION = 0x0A;
	public static final short KEYBL_IS_JAIBREAK = 0x0B;
	public static final short KEYS_PROVIDER_ID = 0x0C;
	public static final short KEYS_SUB_PROVIDER = 0x0D;
	public static final short KEYS_GAME_VERSION = 0x0E;
	public static final short KEYS_LANGUAGE = 0x0F;
	public static final short KEYS_DEVICE_UNIQUE_ID = 0x10;
	public static final short KEYS_PHONE_NUMBER = 0x11;
	public static final short KEYR_SCREEN_WIDTH = 0x12;
	public static final short KEYR_SCREEN_HEIGHT = 0x13;
	public static final short KEYBL_IS_HACK_INAPP = 0x14;
	public static final short KEYBL_IS_FIRST_LOGIN = 0x15;
	public static final short KEYS_NOTIFICATION_ID = 0x16;
	public static final short KEYS_REFCODE = 0x17;
	public static final short KEYS_CHANNEL = 0x18;

	// Common key
	public static final short KEYR_CODE = 0x19;
	public static final short KEYS_MSG = 0x1A;
	public static final short KEYL_MONEY = 0x1B;
	public static final short KEYS_AVATAR = 0x1C;
	public static final short KEYB_GAME_ID = 0x1D;
	public static final short KEYB_STATUS = 0x1E;
	public static final short KEYS_IP_ADDRESS = 0x1F;

	public static final short KEYI_ROOM_ID = 0x21; 
	public static final short KEYS_ROOM_NAME = 0x22; 

}
