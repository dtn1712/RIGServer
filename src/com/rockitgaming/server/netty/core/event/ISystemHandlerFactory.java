package com.rockitgaming.server.netty.core.event;

/**
 * @author lamhm
 *
 */
public abstract interface ISystemHandlerFactory {
	/**
	 * Tìm handler của game cụ thể định nghĩa
	 * 
	 * @param commandId
	 * @return
	 */
	public abstract AbstractRequestHandler findHandler(short commandId);


	/**
	 * Tìm handler của system
	 * 
	 * @param commandId
	 * @return
	 */
	public abstract AbstractRequestHandler findSystemHandler(short commandId);
}
