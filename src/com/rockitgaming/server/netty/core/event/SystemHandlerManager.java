package com.rockitgaming.server.netty.core.event;

import com.rockitgaming.server.entities.manager.IUserManager;
import com.rockitgaming.server.netty.core.event.system.ChangeServerRequestHandler;
import com.rockitgaming.server.netty.core.event.system.ConnectRequestHandler;
import com.rockitgaming.server.netty.core.event.system.DisconnectRequestHandler;
import com.rockitgaming.server.netty.core.event.system.JoinGameRequestHandler;
import com.rockitgaming.server.netty.core.event.system.JoinRoomRequestHandler;
import com.rockitgaming.server.netty.core.event.system.LeaveRoomRequestHandler;
import com.rockitgaming.server.netty.core.event.system.LoginRequestHandler;
import com.rockitgaming.server.netty.core.event.system.LogoutRequestHandler;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.IMessageWriter;
import com.rockitgaming.server.netty.core.socket.SocketServer;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author lamhm
 *
 */
public class SystemHandlerManager {
	private Map<Short, AbstractRequestHandler> systemHandler;


	// TODO review EventManager SFS
	public SystemHandlerManager() {
		systemHandler = new ConcurrentHashMap<Short, AbstractRequestHandler>();
		systemHandler.put(SystemEventType.COMMAND_USER_CONNECT, new ConnectRequestHandler());
		systemHandler.put(SystemEventType.COMMAND_USER_DISCONNECT, new DisconnectRequestHandler());
		systemHandler.put(SystemEventType.COMMAND_USER_LOGIN, new LoginRequestHandler());
		systemHandler.put(SystemEventType.COMMAND_USER_JOIN_GAME, new JoinGameRequestHandler());
		systemHandler.put(SystemEventType.COMMAND_USER_LOGOUT, new LogoutRequestHandler());
		systemHandler.put(SystemEventType.COMMAND_USER_JOIN_ROOM, new JoinRoomRequestHandler());
		systemHandler.put(SystemEventType.COMMAND_USER_LEAVE_ROOM, new LeaveRoomRequestHandler());
		systemHandler.put(SystemEventType.COMMAND_USER_CHANGE_SERVER, new ChangeServerRequestHandler());
	}


	public void init(IUserManager userManager, IMessageWriter messageWriter) {
		for (Entry<Short, AbstractRequestHandler> handlerEntry : systemHandler.entrySet()) {
			AbstractRequestHandler handler = handlerEntry.getValue();
			handler.setUserManager(userManager);
			handler.setMessageWriter(messageWriter);
			handler.setMEApi(SocketServer.getInstance().getAPIManager().getMEApi());
		}
	}


	public AbstractRequestHandler getHandler(Short commandId) {
		return systemHandler.get(commandId);
	}


	public void dispatchEvent(IMessage message) {
		// TODO validate
		AbstractRequestHandler requestHandler = systemHandler.get(message.getCommandId());
		requestHandler.perform(message.getUser(), message);
	}
}
