package com.rockitgaming.server.netty.core.event;

import com.rockitgaming.server.api.IMEApi;
import com.rockitgaming.server.dao.DataManager;
import com.rockitgaming.server.entities.manager.IUserManager;
import com.rockitgaming.server.netty.core.gate.IChannelService;
import com.rockitgaming.server.netty.core.gate.IMessageWriter;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;

import java.util.List;

/**
 * Lớp trừu tượng của một RequestHandler
 * 
 * @author lamhm
 *
 */
public abstract class AbstractRequestHandler implements IRequestHandler {
	protected IChannelService channelService;
	protected IUserManager userManager;
	private IMessageWriter messageWriter;
	protected final DataManager dataManager = DataManager.getInstance();

	private IMEApi meAPI;


	public AbstractRequestHandler() {
		initialize();
	}


	protected void writeMessage(User user, Message message) {
		messageWriter.writeMessage(user, message);
	}


	protected void writeMessage(List<User> users, Message message) {
		messageWriter.writeMessage(users, message);
	}


	protected void writeErrorMessage(User user, short errorCode, String errorMessage) {
		messageWriter.writeMessage(user, DefaultMessageFactory.createErrorMessage(errorCode, errorMessage));
	}


	public IUserManager getUserManager() {
		return userManager;
	}


	public void setUserManager(IUserManager userManager) {
		this.userManager = userManager;
	}


	public void setMessageWriter(IMessageWriter messageWriter) {
		this.messageWriter = messageWriter;
	}


	public IChannelService getChannelService() {
		return channelService;
	}


	public void setChannelService(IChannelService channelService) {
		this.channelService = channelService;
	}


	public IMEApi getMEApi() {
		return meAPI;
	}


	public void setMEApi(IMEApi meAPI) {
		this.meAPI = meAPI;
	}

}
