package com.rockitgaming.server.netty.core.gate;

import java.util.Iterator;

/**
 * @author lamhm
 *
 */
public interface IUser {

	<V> V getAttribute(Object key, Class<V> clazz);


	// TODO lưu avatar thông tin cơ bản của user ở đây
	Iterator<Object> getAttributeKeys();


	long getCreatedTime();


	byte getDeviceType();


	long getSessionId();


	String getLocale();


	String getPlatformInformation();


	byte getProtocolVersion();


	String getScreenSize();


	int getUserId();


	String getUserName();


	String getVersion();


	void initialize(String version, long sessionId, long clientId, byte deviceType, long createTime);


	void removeAttribute(Object key);


	void setAttribute(Object key, Object value);


	void setVersion(String version);


	byte getCurrentGameId();


	void setCurrentGameId(byte currentGameId);

}
