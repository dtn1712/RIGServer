package com.rockitgaming.server.netty.core.gate.wood;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.rockitgaming.server.netty.core.gate.IChannelService;

/**
 * @author lamhm
 *
 */
public class ChannelService implements IChannelService {
	private final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
	private static final AttributeKey<Long> SESSION_ID = AttributeKey.valueOf("dispatcher.sessionId");
	private static final AttributeKey<User> USER = AttributeKey.valueOf("dispatcher.user");
	private Map<Long, Channel> clientChannels;


	public ChannelService() {
		clientChannels = new ConcurrentHashMap<Long, Channel>();
	}


	public void connect(long sessionId, Channel channel) {
		channel.attr(SESSION_ID).set(sessionId);

		User user = new User();
		user.setSessionId(sessionId);
		user.setClientIp(channel.localAddress().toString().substring(1));
		channel.attr(USER).set(user);

		channels.add(channel);
		clientChannels.put(sessionId, channel);
	}


	/**
	 * Set đối tượng device info cho Channel
	 */
	public void setDeviceInfo() {

	}


	public Channel getChannel(long session) {
		return clientChannels.get(session);
	}


	/**
	 * Remove by key
	 * 
	 * @param sessionId
	 */
	public void remove(long sessionId) {
		Channel channel = clientChannels.remove(sessionId);
		channels.remove(channel);
	}


	public void remove(User user) {
		remove(user.getUserId());
	}


	/**
	 * Remove by Object
	 * 
	 * @param channel
	 */
	public long remove(Channel channel) {
		Long sessionId = channel.attr(SESSION_ID).get();
		clientChannels.remove(sessionId);
		return sessionId;
	}


	public long getSessionId(Channel channel) {
		return channel.attr(SESSION_ID).get();
	}


	public User getUser(Channel channel) {
		return channel.attr(USER).get();
	}

}
