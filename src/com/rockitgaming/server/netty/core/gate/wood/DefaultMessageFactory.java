package com.rockitgaming.server.netty.core.gate.wood;

import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYR_CODE;
import static com.rockitgaming.server.netty.core.event.SystemEventType.KEYS_MSG;

import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.netty.core.event.SystemEventType;

/**
 * @author lamhm
 *
 */
public class DefaultMessageFactory {
	private static final byte PROTOCOL_VERSION = 1;


	public static Message createMessage(short commandId) {
		Message message = new Message();
		message.setCommandId(commandId);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}


	/**
	 * Tạo message lỗi.<br>
	 * Khi nào sử dụng createErrorMessage?<br>
	 * Khi client cần bắt những lỗi chung chung để hiện dialog.<br>
	 * Đối với các lỗi logic trong game thì nên trả về mã code lỗi theo command
	 * mà client request để client xử lý theo logic.
	 * 
	 * @param code mã code lỗi
	 * @param errorMessage thông tin lỗi
	 */
	public static Message createErrorMessage(short code, String errorMessage) {
		Message message = new Message();
		message.setCommandId(SystemEventType.COMMAND_ERROR);
		message.setProtocolVersion(PROTOCOL_VERSION);

		message.putShort(KEYR_CODE, code);
		message.putString(KEYS_MSG, errorMessage);
		return message;
	}


	public static Message responseMessage(short commandId) {
		return createMessage(commandId);
	}


	/**
	 * Tạo message connect
	 * 
	 * @param sessionId
	 * @return
	 */
	public static Message createConnectMessage(long sessionId) {
		Message message = new Message();
		message.setSessionId(sessionId);
		message.setCommandId(SystemEventType.COMMAND_USER_CONNECT);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}


	/**
	 * Tạo message disconnect
	 * 
	 * @param userId
	 * @return
	 */
	public static Message createDisconnectMessage(User user) {
		Message message = new Message();
		message.setUser(user);
		message.setCommandId(SystemEventType.COMMAND_USER_DISCONNECT);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}


	/**
	 * Tạo message in game
	 * 
	 * @return
	 */
	public static Message createMessageInGame() {
		Message message = new Message();
		message.setCommandId(IwinCommand.COMMAND_REQUEST_IN_GAME);
		message.setProtocolVersion(PROTOCOL_VERSION);
		message.putShort(IwinKey.KEYR_SUB_COMMAND_IN_GAME, IwinCommand.COMMAND_PLAY_MOVE);
		return message;
	}

}
