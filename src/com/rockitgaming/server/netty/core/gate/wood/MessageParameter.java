package com.rockitgaming.server.netty.core.gate.wood;

/**
 * Parameter lưu trữ key và giá trị của một tham số trong một message.
 * 
 * @author lamhm
 *
 */
public class MessageParameter {
	Short key;
	byte[] value;
}
