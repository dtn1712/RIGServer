package com.rockitgaming.server.netty.core.gate;

import java.util.List;

import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author lamhm
 *
 */
public interface IMessageWriter {
	/**
	 * P2P Send message đến user
	 * 
	 * @param user
	 * @param message
	 */
	public void writeMessage(User user, Message message);


	/**
	 * P2G Send message đến danh sách user
	 * 
	 * @param user
	 * @param message
	 */
	public void writeMessage(List<User> user, Message message);

}
