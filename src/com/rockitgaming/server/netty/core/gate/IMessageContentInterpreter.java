package com.rockitgaming.server.netty.core.gate;

/**
 * Hỗ trợ trace thông tin message
 * 
 * @author lamhm
 *
 */
public abstract interface IMessageContentInterpreter {
	public abstract String interpretCommand(Short serviceId);


	public abstract String interpretKey(Short key);


	public abstract String interpetValue(Short key, byte[] value);


	public abstract void addInToInterpreter(Class<?>... clazz);
}
