package com.rockitgaming.server.netty.core.device;

/**
 * @author thuctvd
 *
 */
public class CompareStatus {
    /**
     * dung de so sanh trong comparable.
     */
    public static final int BEFORE = -1;   
    public static final int EQUAL = 0;    
    public static final int AFTER = 1;
}
