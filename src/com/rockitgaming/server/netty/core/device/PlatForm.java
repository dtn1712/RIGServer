/**
 * 
 */
package com.rockitgaming.server.netty.core.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.netty.core.util.StringUtils;

/**
 * @author thuctvd
 *
 */
public class PlatForm {
	public static final int ANDROID_FLATFORM = 1;
	public static final int IOS_FLATFORM = 2;

	public static final PlatForm ANDROID = new PlatForm(ANDROID_FLATFORM);
	public static final PlatForm IOS = new PlatForm(IOS_FLATFORM);
	public static final PlatForm UNKNOW = new PlatForm(0);

	public static final String IOS_NAME = "ios";
	public static final String IPHONE_NAME = "iphone";
	public static final String IPOD_NAME = "ipod";
	public static final String IPAD_NAME = "ipad";
	public static final String ANDROID_NAME = "android";
	public static final String UNKNOW_NAME = "unknow";
	static Logger logger = LoggerFactory.getLogger(PlatForm.class);

	private int id;
	private String name;


	public PlatForm(int id) {
		this.id = id;

		// set name
		switch (id) {
		case ANDROID_FLATFORM:
			name = ANDROID_NAME;
			break;
		case IOS_FLATFORM:
			name = IOS_NAME;
			break;
		default:
			name = UNKNOW_NAME;
			break;
		}
	}


	public boolean isReview() {
		return false;
	}


	public int getId() {
		return id;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getName() {
		return name;
	}


	public static PlatForm getPlatForm(String clientPhone) {
		PlatForm returnPlatForm = null;
		if (StringUtils.containsIgnoreCase(clientPhone, ANDROID_NAME)) {
			returnPlatForm = ANDROID;
		} else if (StringUtils.containsIgnoreCase(clientPhone, IPHONE_NAME) || StringUtils.containsIgnoreCase(clientPhone, IPAD_NAME)
				|| StringUtils.containsIgnoreCase(clientPhone, IPOD_NAME)) {
			returnPlatForm = IOS;
		}
		logger.debug(clientPhone + " " + returnPlatForm);
		return returnPlatForm;
	}


	public static PlatForm getPlatForm(ClientDevice clientDevice) {
		PlatForm returnPlatForm = UNKNOW;
		if (IOS_NAME.equalsIgnoreCase(clientDevice.getOs())) {
			returnPlatForm = IOS;
		} else if (ANDROID_NAME.equalsIgnoreCase(clientDevice.getOs())) {
			returnPlatForm = ANDROID;
		}
		logger.debug(clientDevice + " " + returnPlatForm);
		return returnPlatForm;
	}


	public Version getCurrentVersion() {
		Version currentVersion = ServerConfig.clientAndroidVersion;
		switch (this.getId()) {
		case ANDROID_FLATFORM:
			currentVersion = ServerConfig.clientAndroidVersion;
			break;
		case IOS_FLATFORM:
			currentVersion = ServerConfig.clientIOSVersion;
			break;
		}
		return currentVersion;
	}


	/**
	 * Lấy tên của hệ điều hành
	 * 
	 * @param osId 1: android, 2: ios, 3: desktop 4 : windowphone, 5: Macos
	 * @return
	 */
	public static String getOsName(int osId) {
		switch (osId) {
		case ANDROID_FLATFORM:
			return ANDROID_NAME;
		case IOS_FLATFORM:
			return IOS_NAME;
		default:
			return UNKNOW_NAME;
		}
	}
}
