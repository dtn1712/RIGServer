/**
 * 
 */
package com.rockitgaming.server.netty.core.device;

import com.rockitgaming.server.netty.core.util.Reporter;

/**
 * @author thuctvd
 *
 */
public class Version {
    public static final Version CURRENT_IOS_VERSION = new Version("1.0.0");
    public static final Version CURRENT_ANDROID_VERSION = new Version("1.0.0");
    public static final Version PROVIDER_UPDATE_VERSION = new Version("1.0.5");
    public static final Version CHARGE_MONEY_WITH_DESC = new Version("1.0.0");
    public static final Version UNKNOWN_VERSION = new Version("0.0.0");
    public static final Version CHECK_INVITED_AND_TEXT_COLOR_ANDROID_VERSION = new Version("4.5.0");
    /**
     * String of version from client.
     */
    private final String versionName;
    /**
     * dung de parse version ra 3 phan.
     */
    private Integer version, versionSub1, versionSub2;

    /**
     * Constructor new Verion. it must have a verions String.
     *
     * @param version Name of this version
     */
    public Version(String version) {
        this.versionName = version;
        parseVersion(versionName);
    }

    public String getVersionName() {
        return versionName;
    }

    private void parseVersion(String versionName) {
        version = versionSub1 = versionSub2 = 0;
        try {
            // version la so dau tien
            String[] versions = versionName.split("\\.");
            if (versions.length > 0) {
                try {
                    version = Integer.parseInt(versions[0]);
                } catch (Exception e) {
                    Reporter.getErrorLogger().error(versionName, e);
                }
                // bo dau cham ke tiep di la subversion 1
                if (versions.length > 1) {
                    try {
                        versionSub1 = Integer.parseInt(versions[1]);
                    } catch (Exception e) {
                        Reporter.getErrorLogger().error(versionName, e);
                    }
                    if (versions.length > 2) {
                        // bo tiep dau cham nua la subversion 2
                        try {
                            versionSub2 = Integer.parseInt(versions[2]);
                        } catch (Exception e) {
                            Reporter.getErrorLogger().error(versionName, e);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Reporter.getErrorLogger().error("parseVersion got exception :", e);
        }
    }
    
    public int compareTo(Version o) {
        int compareValue = this.version.compareTo(o.version);
        if (compareValue == CompareStatus.EQUAL) {
            compareValue = this.versionSub1.compareTo(o.versionSub1);
            if (compareValue == CompareStatus.EQUAL) {
                compareValue = this.versionSub2.compareTo(o.versionSub2);
            }
        }
        return compareValue;
    }
    
    @Override
    public String toString() {
        return versionName;
    }
}
