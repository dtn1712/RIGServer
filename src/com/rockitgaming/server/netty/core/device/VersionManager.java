package com.rockitgaming.server.netty.core.device;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class quan ly version dang chay duoi client.
 * @author vutp
 *
 */

public class VersionManager {
    private static VersionManager instance;
    public static VersionManager getInstance() {
        if (instance == null) {
            instance = new VersionManager();
        }
        return instance;
    }

    // Hash contain all verion of client. hash use key is verion name of it.
    private Map<String, Version> versionMap;
    public static final String DEFAULT_VERSION = "1.4.1";
    
    private VersionManager() {
        this.versionMap = new ConcurrentHashMap<>();
    }

    // Get a version from version manager. It will auto declare a new if doesn't exist.
    public Version getVersion(String key) {
        if (!versionMap.containsKey(key)) {
            versionMap.put(key, new Version(key));
        }
        return versionMap.get(key);
    }

}
