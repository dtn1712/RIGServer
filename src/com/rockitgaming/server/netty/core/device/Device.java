/**
 * 
 */
package com.rockitgaming.server.netty.core.device;

import com.rockitgaming.server.provider.Provider;


/**
 * @author thuctvd
 *
 */
public class Device {
	private static final PlatForm DEFAULT_PLATFORM = PlatForm.UNKNOW;
    private PlatForm platForm = DEFAULT_PLATFORM;
//    private ClientInfo clientInfo;
//    private String clientPhone = "";
    private ClientDevice clientDevice;
    private String refcode;
    private String phoneCode;
    private int deviceProviderId = Provider.IWIN_ID;
    private Version version;

    public Device() {
        this(DEFAULT_PLATFORM);
    }

    public Device(PlatForm platForm) {
        this.platForm = platForm;
        version = Version.UNKNOWN_VERSION;
    }

    public PlatForm getPlatForm() {
        return platForm;
    }

    public void setPlatForm(PlatForm platForm) {
        this.platForm = platForm;
    }
    
//    public ClientInfo getClientInfo() {
//        return clientInfo;
//    }
//
//    public void setClientInfo(ClientInfo clientInfo) {
//        this.clientInfo = clientInfo;
//        setPlatForm(PlatForm.getPlatForm(clientInfo));
//        setVersion(VersionManager.getInstance().getVersion(clientInfo.getClientVer()));
//    }

//    public String getClientPhone() {
//        return clientPhone;
//    }
//
//    public void setClientPhone(String clientPhone) {
//        this.clientPhone = clientPhone;
//        setPlatForm(PlatForm.getPlatForm(clientPhone));
//    }

    public String getRefcode() {
        return refcode;
    }

    public void setRefcode(String refcode) {
        this.refcode = refcode;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public int getDeviceProviderId() {
        return deviceProviderId;
    }

    public void setDeviceProviderId(int deviceProviderId) {
        this.deviceProviderId = deviceProviderId;
    }

    public String getDeviceInfo() {
        String deviceInfor = "";
        if (getClientDevice() != null) {
            StringBuilder sb = new StringBuilder(getClientDevice().getDevice());
            sb.append(";").append(getClientDevice().getOs()).append(";").append(getClientDevice().getOsVersion());
            deviceInfor = sb.toString();
        }
        return deviceInfor;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public boolean isHigherCurrentVersion() {
        return isHigherVersion(getPlatForm().getCurrentVersion());
    }

    public boolean isHigherVersion(Version version) {
        return this.getVersion().compareTo(version) >= 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Device{platForm=");
        sb.append(platForm)
                .append(", clientInfoObject=").append(getClientDevice())
//                .append(", clientPhone=").append(clientPhone)
                .append(", refcode=").append(refcode)
                .append(", phoneCode=").append(phoneCode)
                .append(", deviceProviderId=").append(deviceProviderId)
                .append(", version=").append(version).append('}');
        return sb.toString();
    }

	public ClientDevice getClientDevice() {
		return clientDevice;
	}

	public void setClientDevice(ClientDevice clientDevice) {
		this.clientDevice = clientDevice;
	}
}
