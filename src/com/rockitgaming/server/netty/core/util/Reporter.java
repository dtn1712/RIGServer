package com.rockitgaming.server.netty.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class xử lý các loại Log
 *
 * @author thuctvd
 */
public final class Reporter {

	private final static String SLOW_LOG_NAME = "SlowLog";
	
    private final static Logger LOGGER;
    private final static Logger ERROR;
    private final static Logger POKER;
    private final static Logger MONEYDB_SLOWLOG;    
    private final static Logger EMULATOR_LOGGER;
    private final static Logger SERVER_INFO;
    private final static Logger DEBUG_USER;    

    static {
        try {
            LOGGER = LoggerFactory.getLogger("iwin.server");
            ERROR = LoggerFactory.getLogger("iwin.error");
            POKER = LoggerFactory.getLogger("poker");
            MONEYDB_SLOWLOG = LoggerFactory.getLogger("MoneyDBSlowLog");            
            EMULATOR_LOGGER = LoggerFactory.getLogger("emulator");
            SERVER_INFO = LoggerFactory.getLogger("serverinfo");
            DEBUG_USER = LoggerFactory.getLogger("debuguser");
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static Logger getErrorLogger() {
        return ERROR;
    }

    /**
     * Avoid default constructor is called directly.
     */
    private Reporter() {
        // Nothing todo.
    }

    public static Logger getEMULATOR_LOGGER() {
        return EMULATOR_LOGGER;
    }

    /**
     * Get an application logger by application Id.
     *
     * @param appId Application Id.
     * @return Logger instance.
     */
    public static Logger getAppLogger(int appId) {
        return LoggerFactory.getLogger(String.valueOf(appId));
    }

    /**
     * Get an administration application logger.
     *
     * @return Logger instance.
     */
    public static Logger getAdminLogger() {
        return LoggerFactory.getLogger("admin");
    }

    /**
     * Get a bonus money logger.
     *
     * @return Logger instance.
     */
    public static Logger getBonusMoneyLogger() {
        return LoggerFactory.getLogger("bonusmoney");
    }

    /**
     * Get a bonus money by elo logger.
     *
     * @return Logger instance.
     */
    public static Logger getBonusMoneyByEloLogger() {
        return LoggerFactory.getLogger("bonusmoneyelo");
    }

    /**
     * Get a trace bonus money logger.
     *
     * @return Logger instance.
     */
    public static Logger getTraceBonusLogger() {
        return LoggerFactory.getLogger("tracebonus");
    }

    /**
     * Get an pay card logger.
     *
     * @return Logger instance.
     */
    public static Logger getPayCardLogger() {
        return LoggerFactory.getLogger("paycard");
    }

    /**
     * Get accessing logger.
     *
     * @return Logger instance.
     */
    public static Logger getAccessLogger() {
        return LoggerFactory.getLogger("access");
    }

    /**
     * Get pushing logger.
     *
     * @return Logger instance.
     */
    public static Logger getSpamLogger() {
        return LoggerFactory.getLogger("pushing");
    }

    /**
     * Get trace logger.
     *
     * @return Logger instance.
     */
    public static Logger getTraceLogger() {
        return LoggerFactory.getLogger("tracing");
    }

    /**
     * Get hacking logger.
     *
     * @return Logger instance.
     */
    public static Logger getHackingLogger() {
        return LoggerFactory.getLogger("hacking");
    }

    /**
     * Get chat logger.
     *
     * @return Logger instance.
     */
    public static Logger getChatLogger() {
        return LoggerFactory.getLogger("CHAT");
    }

    /**
     * Get charge money logger.
     *
     * @return Logger instance.
     */
    public static Logger getChargeMoneyLogger() {
        return LoggerFactory.getLogger("chargemoney");
    }

    /**
     * Get transfer money logger.
     *
     * @return Logger instance.
     */
    public static Logger getTransferMoneyLogger() {
        return LoggerFactory.getLogger("transfermoney");
    }

    /**
     * Get Database logger.
     *
     * @return Logger instance.
     */
    public static Logger getDatabaseLogger() {
        return LoggerFactory.getLogger("iwin.db");
    }

    /**
     * Get Database logger.
     *
     * @return Logger instance.
     */
    public static Logger getDbMonitorLogger() {
        return LoggerFactory.getLogger("iwin.dbcounter");
    }

    public static Logger getMongoDbMonitorLogger() {
        return LoggerFactory.getLogger("iwin.mongodbcounter");
    }

    /**
     * Get tracking version logger.
     *
     * @return Logger instance.
     */
    public static Logger getVersionLogger() {
        return LoggerFactory.getLogger("versiontracking");
    }

    public static Logger getLocationLogger() {
        return LoggerFactory.getLogger("location");
    }

    /**
     * Write poker action to log.
     *
     * @param message Debug message.
     */
    public static Logger getXiToLogger() {
        return LoggerFactory.getLogger("game.xito");
    }

    /**
     * Write an information to log.
     *
     * @param message Message information.
     */
    public static void infor(final String message) {
        LOGGER.info(message);
    }

    /**
     * Write debug message to log.
     *
     * @param message Debug message.
     */
    public static void debug(final String message) {
        LOGGER.debug(message);
    }

    public static void serverInfo(final String info) {
        SERVER_INFO.info(info);
    }

    public static void debugUser(final String info) {
        DEBUG_USER.info(info);
    }

    /**
     * Write poker action to log.
     *
     * @param message Debug message.
     */
    public static Logger getPokerLogger() {
        return POKER;
    }

    /**
     * Write debug message to log.
     *
     * @param message Debug message.
     * @param exception Exception.
     */
    public static void debug(final String message, final Throwable exception) {
        LOGGER.debug(message, exception);
    }

    /**
     * Write a warning message to log.
     *
     * @param message Warning message.
     */
    public static void warn(final String message) {
        LOGGER.warn(message);
    }

    /**
     * Write a warning message to log.
     *
     * @param message Warning message.
     * @param exception Exception.
     */
    public static void warn(final String message, final Throwable exception) {
        LOGGER.warn(message, exception);
    }

    /**
     * Write a string format to console.
     *
     * @param format String format
     * @param params Parameters.
     */
    public static void writeConsole(final String format, final Object... params) {
//        String buffer = String.format(format, params);
//        System.out.println(buffer);
    }

    /**
     * Write a message to console.
     *
     * @param message Message.
     */
    public static void writeConsole(final String message) {
        System.out.println(message);
    }

    /**
     * get logger by name.
     *
     * @param logName Log name.
     */
    public static Logger getLogger(String logName) {
        return LoggerFactory.getLogger(logName);
    }

    /**
     * Log lại thông tin những cmd xử lý chậm.
     *
     * @param message
     */
    public static void logSlowedResponseCommand(int commandId, long processingTime, String username, String clientPhone) {
        StringBuilder format = new StringBuilder("Reponse \t");
        format.append(commandId);
        format.append("\t");
        format.append(processingTime);
        format.append("\t");
        format.append(username);
        format.append("\t");
        format.append(clientPhone);
        LoggerFactory.getLogger(SLOW_LOG_NAME).info(format.toString());
    }

    /**
     * Log lại thông tin những cmd xử lý chậm.
     *
     * @param message
     */
    public static void logSlowedProcessCommand(int commandId, long processingTime, String username, String clientPhone) {
        StringBuilder format = new StringBuilder("Process \t");
        format.append(commandId);
        format.append("\t");
        format.append(processingTime);
        format.append("\t");
        format.append(username);
        format.append("\t");
        format.append(clientPhone);
        LoggerFactory.getLogger(SLOW_LOG_NAME).info(format.toString());
    }

    /**
     * Log lại thông tin những hàm xử lý chậm của MoneyDatabase class.
     *
     * @param functionName function name.
     * @param processingTime processing time.
     * @param message message.
     */
    public static void logSlowedMoneyDBFunction(String functionName, long processingTime, String message) {
        StringBuilder format = new StringBuilder("MoneyDatabase \t");
        format.append(functionName);
        format.append("\t");
        format.append(processingTime);
        format.append("\t");
        format.append(message);
        MONEYDB_SLOWLOG.info(format.toString());
    }

    /**
     * Log lại thông tin những hàm xử lý chậm của SwapDatabase class.
     *
     * @param functionName function name.
     * @param processingTime processing time.
     * @param message message.
     */
    public static void logSlowedSwapDBFunction(String functionName, long processingTime, String message) {
        StringBuilder format = new StringBuilder("SwapDatabase \t");
        format.append(functionName);
        format.append("\t");
        format.append(processingTime);
        format.append("\t");
        format.append(message);        
    }

    /**
     * log lai thong tin session cua user, nhu: tien, thoi gian online...
     *
     * @return Logger instance.
     */
    public static Logger getSessionLogger() {
        return LoggerFactory.getLogger("session");
    }

    public static Logger getItemsLogger() {
        return LoggerFactory.getLogger("items");
    }

    public static Logger getNewPhomLogger() {
        return LoggerFactory.getLogger("newphom");
    }

    public static Logger getCriticalMoneyLogger() {
        return LoggerFactory.getLogger("criticalMoney");
    }

    public static Logger getRequestLogger() {
        return LoggerFactory.getLogger("requestLog");
    }

    public static Logger getResponseLogger() {
        return LoggerFactory.getLogger("responseLog");
    }

    public static Logger getThreadLogger() {
        return LoggerFactory.getLogger("threadLog");
    }

    public static Logger getDebugDBLogger() {
        return LoggerFactory.getLogger("debugDB");
    }

    public static Logger getMoneyLogger() {
        return LoggerFactory.getLogger("money");
    }
}
