/**
 * 
 */
package com.rockitgaming.server.netty.core.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Properties;

import com.rockitgaming.server.lang.IwinGlobals;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * @author thuctvd
 *
 */
public class Utility {
	
	/**
	 * Load 1 file text
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static String loadTextFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return encoding.decode(ByteBuffer.wrap(encoded)).toString();
    }
	
	//load file config java
	public static Properties loadConfigFile(String path) {
        FileInputStream inputStream = null;
        Properties returnProperties = null;
        try {
            returnProperties = new Properties();
            inputStream = new FileInputStream(path);
            returnProperties.load(inputStream);
            inputStream.close();
        } catch (IOException ex) {
            Reporter.debug(ex.getMessage(), ex);
        }
        return returnProperties;
    }
	
	public static boolean checkNameValid(String name){
		return name.matches("[a-zA-Z0-9]*");
	}
	
	public static String getMessageContent(User user, String fileName, String  keyContent) {
		Locale locale = null;
		if (user == null) {
			locale = IwinGlobals.ENGLISH_LOCALE;
		} else {
		}
		
		String filePath = "lang/" + fileName;
		return IwinGlobals.getInstance(locale).getResource(filePath).get(keyContent);
	}

}
