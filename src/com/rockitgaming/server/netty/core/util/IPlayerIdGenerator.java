package com.rockitgaming.server.netty.core.util;

import com.rockitgaming.server.entities.Room;

public abstract interface IPlayerIdGenerator
{
  public abstract void init();
  
  public abstract int getPlayerSlot();
  
  /**
   * Giải phóng 1 slot chỉ định
   * @param paramInt
   */
  public abstract void freePlayerSlot(int paramInt);
  
  public abstract void onRoomResize();
  
  public abstract void setParentRoom(Room paramRoom);
  
  public abstract Room getParentRoom();
}
