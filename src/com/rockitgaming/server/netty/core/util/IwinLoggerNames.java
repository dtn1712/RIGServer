package com.rockitgaming.server.netty.core.util;

/**
 * @author vutp
 *
 */

public class IwinLoggerNames {
	/**
	 * LogMoney name.
	 */
	public static final String MONEY_LOG_NAME = "MONEY";

	/**
	 * Slow logger name
	 */
	public static final String SLOW_LOG_NAME = "SlowLog";

	/**
	 * Db Error log.
	 */
	public static final String ERROR_DB_LOG_NAME = "iwin.db";

	/**
	 * Error log.
	 */
	public static final String ERROR_LOG_NAME = "iwin.error";	
	/**
	 * Error log serverinfo
	 */
	public static final String SERVER_INFO_LOG = "serverinfo";
	
	/**
	 * Error log dbcounter
	 */
	public static final String DB_COUNTER_LOG = "iwin.dbcounter";
	
}
