package com.rockitgaming.server.netty.core.socket;

import com.rockitgaming.server.api.APIManager;
import com.rockitgaming.server.dao.DataManager;
import com.rockitgaming.server.entities.manager.ExtensionManager;
import com.rockitgaming.server.entities.manager.GameManager;
import com.rockitgaming.server.entities.manager.IRoomManager;
import com.rockitgaming.server.entities.manager.IUserManager;
import com.rockitgaming.server.entities.manager.RoomManager;
import com.rockitgaming.server.entities.manager.UserManager;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinCommand;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.lang.IwinGlobals;
import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.netty.core.codec.MessageDecoder;
import com.rockitgaming.server.netty.core.codec.MessageEncoder;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.event.SystemHandlerManager;
import com.rockitgaming.server.netty.core.event.service.MessageExecutor;
import com.rockitgaming.server.netty.core.event.service.SystemMessageExecutor;
import com.rockitgaming.server.netty.core.gate.wood.ChannelService;
import com.rockitgaming.server.netty.core.gate.wood.ConstantMessageContentInterpreter;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.MessageWriter;
import com.rockitgaming.server.netty.core.util.Reporter;
import com.rockitgaming.server.netty.core.util.Utility;
import com.rockitgaming.server.proxy.ClusterProxy;
import com.rockitgaming.server.services.CouchbaseService;
import com.rockitgaming.server.services.IClusterService;
import com.rockitgaming.server.services.IgniteService;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.GenericFutureListener;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Proxy;
import java.net.SocketAddress;
import java.util.Properties;

/**
 * @author lamhm
 *
 */
public class SocketServer {
	private static final Logger LOG = LoggerFactory.getLogger(SocketServer.class);
	private static SocketServer instance;
	private int port;
	private MessageHandler messageHandler = new MessageHandler();
	private IUserManager userManager;
	private IRoomManager roomManager;
	private APIManager apiManager;
	private ExtensionManager extensionManager;
	private SystemHandlerManager systemHandlerManager;
	private IClusterService clusterService;
	private CouchbaseService couchbaseService;
	private SystemMessageExecutor messageExecutor;


	public static SocketServer getInstance() {
		if (instance == null) {
			instance = new SocketServer();
		}
		return instance;
	}


	private SocketServer() {
		initConfig();
		setPort(ServerConfig.serverPort);

		userManager = new UserManager();
		roomManager = new RoomManager(this);
		apiManager = new APIManager(this);
		extensionManager = new ExtensionManager();
	}


	private void start() throws InterruptedException {
		LOG.info("====================== PREPARE SERVER ======================= ");
		initServerService();
		// connection incoming (chứa danh sách đang kết nối)
		EventLoopGroup bossGroup = new NioEventLoopGroup();

		// xử ly các event sau khi đã kết nối (xử lý IO cho một channel)
		// EventLoop handle những event cho channel
		// EventLoopGroup có thể chứ nhiều hơn 1 EventLoop
		// có thể hiểu EventLoops như những thread xử lý task cho một channel

		// Khi một channel được đăng ký, Netty sẽ bind nó với một single
		// EventLoop(single thread) cho lifetime của Channel. Do đó ứng dụng
		// không cần phải Sync vì tất cả IO của channel sẽ luôn luôn thực thi
		// trên cùng một thread
		EventLoopGroup workerGroup = new NioEventLoopGroup();// chứa danh sách
		// đã kết nối
		try {
			ServerBootstrap bootstrap = new ServerBootstrap();
			// Channel có thể hiểu như một socket connection
			bootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					ch.pipeline().addLast(new MessageDecoder(), new MessageEncoder(), messageHandler);
				}

			})
			// Unpooled buffer sẽ có rủi ro cấp phát/giải phóng buffer chậm
					.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
					// max incoming trong queue, Option apply cho serverchannel
					.option(ChannelOption.SO_BACKLOG, ServerConfig.soBackLog).option(ChannelOption.CONNECT_TIMEOUT_MILLIS, ServerConfig.connectTimeoutMillis)
					// chấp nhận incoming connection. Child Option sẽ apply trên
					// các channel đã được accept.
					.childOption(ChannelOption.SO_KEEPALIVE, ServerConfig.soKeepAlive);

			// ChannelFuture tất cả các hành động IO của Netty là bất đồng bộ
			// do đó không biết được khi nào một hành động kết thúc
			// ChannelFuture sẽ giúp listen sau một hành động kết thúc
			// bind và start chấp nhận incoming connection
			ChannelFuture future = bootstrap.bind(ServerConfig.serverIp, port).sync();
			future.addListener(new GenericFutureListener<ChannelFuture>() {
				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (future.isSuccess()) {
						SocketAddress localAddress = future.channel().localAddress();
						LOG.info("SERVER INFO:" + localAddress.toString());
						LOG.info("====================== SERVER STARTED =======================");
					} else {
						LOG.error("Bound attempt failed! ", future.cause().toString());
					}
				}
			});
			// chờ cho đới khi server socket đóng
			future.channel().closeFuture().sync();

		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}

	}


	public void initConfig() {
		// load server config
		Reporter.infor("********** Load Server Config **************");
		Properties prop = Utility.loadConfigFile("configs/serverconfig.properties");
		ServerConfig.init(prop);
	}


	public void initServerService() {
		// init DB connection
		Reporter.infor("********** Load DataBase Config **************");
		DataManager.getInstance();

		// Init Multi Language
		Reporter.serverInfo("********** Init Multi Language **************");
		IwinGlobals.getInstance();

		// init thread pool
		// Reporter.serverInfo("********** Init Thread Pool Manager **************");
		// ThreadPoolManager.getInstance();

		// init queue service
		// Reporter.serverInfo("********** Init Queue Service **************");
		// QueueService.init(prop);
		systemHandlerManager = new SystemHandlerManager();
		systemHandlerManager.init(userManager, new MessageWriter(messageHandler));

		messageExecutor = new SystemMessageExecutor();
		messageExecutor.initSystemHandlerManager(systemHandlerManager);

		messageHandler.setSystemMessageExecutor(messageExecutor);
		messageHandler.setMessageExecutor(new MessageExecutor());

		// có thể bỏ luôn ChannelService
		messageHandler.setChannelService(new ChannelService());

		// cluster
		clusterService = (IClusterService) Proxy.newProxyInstance(IClusterService.class.getClassLoader(), new Class[] { IClusterService.class },
				new ClusterProxy(new IgniteService()));
		roomManager.setClusterService(clusterService);

		// cache
		couchbaseService = new CouchbaseService();

		// trace message
		Message.setIntepreter(new ConstantMessageContentInterpreter(SystemEventType.class, IwinCommand.class, IwinKey.class));

		Reporter.infor("********** Initialize rooms from config **************");
		GameManager.getInstance();

		Reporter.infor("********** Everything Is Completed! **************");
	}


	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public ExtensionManager getExtensionManager() {
		return extensionManager;
	}


	public APIManager getAPIManager() {
		return apiManager;
	}


	public IUserManager getUserManager() {
		return userManager;
	}


	public IRoomManager getRoomManager() {
		return roomManager;
	}


	public SystemHandlerManager getSystemHandlerManager() {
		return systemHandlerManager;
	}


	public MessageHandler getMessageHandler() {
		return messageHandler;
	}


	public IClusterService getClusterService() {
		return clusterService;
	}


	public CouchbaseService getCouchbaseService() {
		return couchbaseService;
	}


	public SystemMessageExecutor getSystemMessageExecutor() {
		return messageExecutor;
	}


	public static void main(String[] args) throws Exception {
		PropertyConfigurator.configure("configs/log4j.properties");
		SocketServer.getInstance().start();
	}
}
