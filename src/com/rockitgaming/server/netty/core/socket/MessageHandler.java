package com.rockitgaming.server.netty.core.socket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.net.SocketAddress;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.netty.core.event.service.MessageExecutor;
import com.rockitgaming.server.netty.core.event.service.SystemMessageExecutor;
import com.rockitgaming.server.netty.core.gate.IMessage;
import com.rockitgaming.server.netty.core.gate.IUser;
import com.rockitgaming.server.netty.core.gate.wood.ChannelService;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.gate.wood.User;

/**
 * Class tiếp nhận message từ client. Xử lý business logic.<br>
 * Share giữa các channel giúp giảm thiểu resource (chú ý Channel Handler phải
 * là stateless).<br>
 * inbound là data từ ứng dụng đến server(remote peer)<br>
 * outbound là data từ server(remote peer) đến ứng dụng (ví dụ như hành động
 * write)
 * 
 * @author lamhm
 */
@Sharable
public class MessageHandler extends SimpleChannelInboundHandler<Message> {
	private static final Logger LOG = LoggerFactory.getLogger(MessageHandler.class);
	private static final AtomicLong nextSessionId = new AtomicLong(System.currentTimeMillis());

	private SystemMessageExecutor systemMessageExecutor;
	private MessageExecutor messageExecutor;
	private ChannelService channelService;


	@Override
	public void channelActive(final ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		SocketAddress remoteAddress = channel.remoteAddress();

		synchronized (nextSessionId) {
			long sessionId = nextSessionId.getAndIncrement();
			channelService.connect(sessionId, channel);
			// Xử lý client connected
			systemMessageExecutor.addSystemMessage(DefaultMessageFactory.createConnectMessage(sessionId));
			LOG.info("[INFO] [CLIENT] - " + remoteAddress.toString() + " connected -" + "sessionId:" + sessionId);
		}

	}


	/*
	 * Chú ý khi xử lý message là có nhiều thread xử lý IO, do đó cố gắng không
	 * Block IO Thread có thể có vấn đề về performance vì phải duyệt sâu đối với
	 * những môi trường throughout cao. Netty hỗ trợ EventExecutorGroup để giải
	 * quyết vấn đề này khi add vào ChannelHandlers. Nó sẽ sử dụng EventExecutor
	 * thực thi tất các phương thức của ChannelHandler. EventExecutor sẽ sử dụng
	 * một thread khác để xử lý IO sau đó giải phóng EventLoop.
	 */
	@Override
	protected void channelRead0(final ChannelHandlerContext ctx, final Message message) throws Exception {
		Channel channel = ctx.channel();
		User user = channelService.getUser(channel);
		message.setSessionId(user.getSessionId());

		// trường hợp đã login
		if (user.getUserId() != -1) {
			user = SocketServer.getInstance().getUserManager().getUserById(user.getUserId());
		}
		message.setUser(user);

		// những command < 50 add vào system command
		if (message.getCommandId() < 50) {
			systemMessageExecutor.addSystemMessage(message);
		} else {
			messageExecutor.addExtensionMessage(message);
		}

		LOG.debug(message.toString());
	}


	/**
	 * @param receiver người nhận
	 * @param message
	 */
	public void send(IUser receiver, final IMessage message) {
		Channel channel = channelService.getChannel(receiver.getSessionId());
		ChannelFuture future = channel.writeAndFlush(message);

		future.addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				if (future.isSuccess() && LOG.isDebugEnabled()) {
					LOG.debug("[DEBUG] [RESPONSE]\n" + message.toString());
				}
			}
		});
	}


	/**
	 * Send cho nhóm user
	 * 
	 * @param receivers danh sách người nhận
	 * @param message
	 */
	public void send(List<User> receivers, final IMessage message) {
		for (IUser receiver : receivers) {
			send(receiver, message);
		}
	}


	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		// flush tất cả những message trước đó (những message đang pending) đến
		// remote peer, và đóng channel sau khi write hoàn thành
		// ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
	}


	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}


	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		// trường hợp disconnect/logout
		Channel channel = ctx.channel();

		// disconnectRequestHandler xử lý xóa thông tin của user này
		User user = channelService.getUser(channel);
		channelService.remove(channel);
		systemMessageExecutor.addSystemMessage(DefaultMessageFactory.createDisconnectMessage(user));
	}


	public void removeUser(User user) {
		channelService.remove(user);
	}


	public void setSystemMessageExecutor(SystemMessageExecutor systemMessageExecutor) {
		this.systemMessageExecutor = systemMessageExecutor;
	}


	public void setChannelService(ChannelService channelService) {
		this.channelService = channelService;
	}


	public void setMessageExecutor(MessageExecutor messageExecutor) {
		this.messageExecutor = messageExecutor;
	}

}
