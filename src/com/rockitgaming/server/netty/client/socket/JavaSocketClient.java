package com.rockitgaming.server.netty.client.socket;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

import com.rockitgaming.server.netty.client.codec.MessageDecoder;
import com.rockitgaming.server.netty.client.codec.MessageEncoder;
import com.rockitgaming.server.netty.client.gate.wood.ClientMessageFactory;
import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.wood.Message;
import com.rockitgaming.server.netty.core.util.IOUtils;

/**
 * @author lamhm
 *
 */
public class JavaSocketClient implements Runnable {
	private static final int PORT = 8080;
	private Socket socket = null;
	private Thread thread = null;
	private BufferedReader console = null;
	private DataOutputStream streamOut = null;
	private ChatClientThread client = null;


	public JavaSocketClient() {
		System.out.println("Establishing connection. Please wait ...");
		try {
			socket = new Socket("localhost", PORT);
			System.out.println("Connected: " + socket);
			start();
		} catch (UnknownHostException uhe) {
			System.out.println("Host unknown: " + uhe.getMessage());
		} catch (IOException ioe) {
			System.out.println("Unexpected exception: " + ioe.getMessage());
		}
	}


	public void run() {
		while (thread != null) {
			try {
				console.readLine();
				sendMessage(ClientMessageFactory.createRequestLoginMessage((byte) 1, "lamhm", "123456"));
			} catch (IOException ioe) {
				System.out.println("Sending error: " + ioe.getMessage());
				stop();
			}
		}
	}


	public void sendMessage(Message message) {
		try {
			byte[] encode = MessageEncoder.encode(message);
			System.out.println(Arrays.toString(encode));
			streamOut.write(encode);
			streamOut.flush();
		} catch (IOException ioe) {
			System.out.println("Sending error: " + ioe.getMessage());
			stop();
		}
	}


	public void receiveMessage(byte[] msg) {
		Message message = MessageDecoder.decode(msg);
		System.out.println("[FATAL] received " + message.toString());

		switch (message.getCommandId()) {
		case SystemEventType.COMMAND_USER_CONNECT:
			System.out.println("[FATAL] [RESPONSE-CONNECT]");
			break;
		case SystemEventType.COMMAND_USER_LOGIN:
			System.out.println("[FATAL] [RESPONSE-LOGIN]");
			break;

		default:
			break;
		}
	}


	public void start() throws IOException {
		console = new BufferedReader(new InputStreamReader(System.in));
		streamOut = new DataOutputStream(socket.getOutputStream());
		if (thread == null) {
			client = new ChatClientThread(this, socket);
			thread = new Thread(this);
			thread.start();
		}
	}


	public void stop() {
		if (thread != null) {
			thread = null;
		}

		try {
			if (console != null)
				console.close();
			if (streamOut != null)
				streamOut.close();
			if (socket != null)
				socket.close();
		} catch (IOException ioe) {
			System.out.println("Error closing ...");
		}
		client.close();
	}


	public static void main(String args[]) {
		new JavaSocketClient();
	}

	public class ChatClientThread extends Thread {
		private Socket socket = null;
		private JavaSocketClient client = null;
		private InputStream streamIn = null;


		public ChatClientThread(JavaSocketClient _client, Socket _socket) {
			client = _client;
			socket = _socket;
			open();
			start();
		}


		public void open() {
			try {
				streamIn = socket.getInputStream();
			} catch (IOException ioe) {
				System.out.println("Error getting input stream: " + ioe);
				client.stop();
			}
		}


		public void close() {
			try {
				if (streamIn != null)
					streamIn.close();
			} catch (IOException ioe) {
				System.out.println("Error closing input stream: " + ioe);
			}
		}


		public void run() {
			while (true) {
				try {
					int size = streamIn.available();
					if (size > 0) {
						receiveMessage(IOUtils.toByteArray(streamIn));
					}
				} catch (IOException ioe) {
					System.out.println("Listening error: " + ioe.getMessage());
					client.stop();
				}
			}
		}
	}

}
