package com.rockitgaming.server.netty.client.codec;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import com.rockitgaming.server.netty.core.gate.wood.Message;

/**
 * @author lamhm
 *
 */
public class MessageEncoder {

	public static byte[] encode(Message message) {
		ByteArrayOutputStream bos = null;
		DataOutputStream bodyDOS = null;

		try {
			List<Short> keyList = message.getKeyList();
			byte[] body = null;
			if (keyList != null && keyList.size() > 0) {
				bos = new ByteArrayOutputStream();
				bodyDOS = new DataOutputStream(bos);
				int size = keyList.size();
				for (int i = 0; i < size; i++) {
					Short key = keyList.get(i);
					byte[] value = message.getBytesAt(i);
					bodyDOS.writeShort(key.shortValue());
					bodyDOS.writeInt(value.length);
					bodyDOS.write(value);
				}
				bodyDOS.flush();
				body = bos.toByteArray();
			}

			int bodyLength = body != null ? body.length : 0;
			ByteBuffer msgBody = ByteBuffer.allocate(7 + bodyLength);
			msgBody.put(message.getProtocolVersion());
			msgBody.putShort(message.getCommandId());
			msgBody.putInt(bodyLength);
			if (bodyLength > 0) {
				// body data
				msgBody.put(body);
			}

			return msgBody.array();
		} catch (IOException e) {
			throw new RuntimeException("Invalid messsage");
		} finally {
			try {
				if (bodyDOS != null) {
					bodyDOS.close();
				}
				if (bos != null) {
					bos.close();
				}
			} catch (Exception localException1) {
			}
		}
	}
}
