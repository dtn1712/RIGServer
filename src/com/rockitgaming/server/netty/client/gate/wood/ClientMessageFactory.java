package com.rockitgaming.server.netty.client.gate.wood;

import com.rockitgaming.server.netty.core.event.SystemEventType;
import com.rockitgaming.server.netty.core.gate.wood.DefaultMessageFactory;
import com.rockitgaming.server.netty.core.gate.wood.Message;

/**
 * @author lamhm
 *
 */
public class ClientMessageFactory {

	public static Message createRequestLoginMessage(byte protocolVersion, String userName, String password) {
		Message message = DefaultMessageFactory.createMessage(SystemEventType.COMMAND_USER_LOGIN);
		message.putString((short) 2, userName);
		message.putString((short) 3, password);
		message.putString((short) 4, "lamha1");
		message.putString((short) 4, "lamha2");
		message.putString((short) 4, "lamha3");
		return message;
	}


	public static Message createMessage(byte protocolVersion, short commandId) {
		return DefaultMessageFactory.createMessage(commandId);
	}
}
