package com.rockitgaming.server.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.model.ServerConfig;

/**
 * @author lamhm
 *
 */
public class ClusterProxy implements InvocationHandler {
	private static final Logger LOG = LoggerFactory.getLogger(ClusterProxy.class);
	private Object obj;


	public ClusterProxy(Object obj) {
		this.obj = obj;
	}


	@Override
	public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
		// trường hợp tắt tính năng cluster thì không process j hết
		if (!ServerConfig.enableCluster)
			return null;

		Object result = null;
		try {
			result = m.invoke(obj, args);
		} catch (InvocationTargetException e) {
			LOG.error("Invocation Target Exception: ", e);
		} catch (Exception e) {
			LOG.error("Invocation Target Exception: ", e);
		}
		return result;
	}

}
