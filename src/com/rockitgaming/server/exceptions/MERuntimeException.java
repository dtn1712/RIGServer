package com.rockitgaming.server.exceptions;

public class MERuntimeException extends RuntimeException {

	private static final long serialVersionUID = -6433280068682786254L;

	public MERuntimeException() {
	}

	public MERuntimeException(String message) {
		super(message);
	}

	public MERuntimeException(Throwable t) {
		super(t);
	}

}
