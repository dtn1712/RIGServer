package com.rockitgaming.server.exceptions;

public class MECreateRoomException extends MEException {
	private static final long serialVersionUID = 4751733417642191809L;

	public MECreateRoomException() {
	}

	public MECreateRoomException(String message) {
		super(message);
	}

	public MECreateRoomException(String message, MEErrorData data) {
		super(message, data);
	}
}
