package com.rockitgaming.server.exceptions;

public abstract interface IErrorCode
{
  public abstract short getId();
}