package com.rockitgaming.server.exceptions;

public class MEJoinRoomException extends MEException {
	private static final long serialVersionUID = 6384101728401558209L;

	public MEJoinRoomException() {
	}

	public MEJoinRoomException(String message) {
		super(message);
	}

	public MEJoinRoomException(String message, MEErrorData data) {
		super(message, data);
	}
}
