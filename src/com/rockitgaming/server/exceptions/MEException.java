package com.rockitgaming.server.exceptions;

public class MEException extends Exception {
	private static final long serialVersionUID = 6052949605652105170L;
	MEErrorData errorData;

	public MEException() {
		this.errorData = null;
	}

	public MEException(String message) {
		super(message);
		this.errorData = null;
	}

	public MEException(String message, MEErrorData data) {
		super(message);
		this.errorData = data;
	}

	public MEException(Throwable t) {
		super(t);
		this.errorData = null;
	}

	public MEErrorData getErrorData() {
		return this.errorData;
	}
}
