package com.rockitgaming.server.exceptions;

public class MEExtensionException extends MEException {

	private static final long serialVersionUID = -5341357290605347548L;

	public MEExtensionException() {
	}

	public MEExtensionException(String message) {
		super(message);
	}

	public MEExtensionException(String message, MEErrorData data) {
		super(message, data);
	}
}
