package com.rockitgaming.server.exceptions;

public class MERoomException extends MEException {

	private static final long serialVersionUID = -6274741862565682229L;

	public MERoomException() {
	}

	public MERoomException(String message) {
		super(message);
	}

	public MERoomException(String message, MEErrorData data) {
		super(message, data);
	}
}