package com.rockitgaming.server.entities;

import java.util.List;
import java.util.Set;

import com.rockitgaming.server.entities.manager.IUserManager;
import com.rockitgaming.server.exceptions.MEJoinRoomException;
import com.rockitgaming.server.exceptions.MERoomException;
import com.rockitgaming.server.extensions.ISFSExtension;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.util.IPlayerIdGenerator;

public abstract interface Room {
	public abstract int getId();


	public abstract void setGameId(byte gameId);


	public abstract byte getGameId();


	public abstract String getGroupId();


	public abstract void setGroupId(String paramString);


	public abstract String getName();


	public abstract void setName(String paramString);


	public abstract String getPassword();


	public abstract void setPassword(String paramString);


	public abstract boolean isPasswordProtected();


	public abstract boolean isPublic();


	public abstract int getCapacity();


	public abstract void setCapacity(int paramInt1, int paramInt2);


	public abstract int getMaxUsers();


	public abstract void setMaxUsers(int paramInt);


	public abstract int getMaxSpectators();


	public abstract void setMaxSpectators(int paramInt);


	public abstract User getOwner();


	public abstract void setOwner(User paramUser);


	public abstract RoomSize getSize();


	public abstract IUserManager getUserManager();


	public abstract void setUserManager(IUserManager paramIUserManager);


	public abstract boolean isDynamic();


	public abstract boolean isGame();


	public abstract boolean isHidden();


	public abstract void setDynamic(boolean paramBoolean);


	public abstract void setGame(boolean paramBoolean);


	public abstract void setGame(boolean paramBoolean, Class<? extends IPlayerIdGenerator> paramClass);


	public abstract void setHidden(boolean paramBoolean);


	public abstract void setFlags(Set<RoomSettings> paramSet);


	public abstract void setFlag(RoomSettings paramRoomSettings, boolean paramBoolean);


	public abstract boolean isFlagSet(RoomSettings paramSFSRoomSettings);


	public abstract RoomRemoveMode getAutoRemoveMode();


	public abstract void setAutoRemoveMode(RoomRemoveMode paramSFSRoomRemoveMode);


	public abstract boolean isEmpty();


	public abstract boolean isFull();


	public abstract boolean isActive();


	public abstract void setActive(boolean paramBoolean);


	public abstract ISFSExtension getExtension();


	public abstract void setExtension(ISFSExtension paramISFSExtension);


	public abstract Object getProperty(Object paramObject);


	public abstract void setProperty(Object paramObject1, Object paramObject2);


	public abstract boolean containsProperty(Object paramObject);


	public abstract void removeProperty(Object paramObject);


	public abstract User getUserById(int paramInt);


	public abstract User getUserByName(String paramString);


	public abstract User getUserByPlayerId(int paramInt);


	/**
	 * Lấy danh sách user trong phòng
	 * 
	 * @return
	 */
	public abstract List<User> getUserList();


	/**
	 * Lấy danh sách người chơi trong phòng
	 * 
	 * @return
	 */
	public abstract List<User> getPlayersList();


	/**
	 * Lấy danh sách người xem trong phòng
	 * 
	 * @return
	 */
	public abstract List<User> getSpectatorsList();


	public abstract void addUser(User paramUser, boolean paramBoolean) throws MEJoinRoomException;


	public abstract void addUser(User paramUser) throws MEJoinRoomException;


	public abstract void removeUser(User paramUser);


	public abstract boolean containsUser(User paramUser);


	public abstract boolean containsUser(String paramString);


	public abstract void switchPlayerToSpectator(User paramUser) throws MERoomException;


	public abstract void switchSpectatorToPlayer(User paramUser) throws MERoomException;


	public abstract long getLifeTime();


	public abstract String getDump();


	public abstract void destroy();


	public abstract String getPlayerIdGeneratorClassName();


	public abstract String getHost();
}
