package com.rockitgaming.server.entities;

public enum RoomRemoveMode
{
  DEFAULT,  WHEN_EMPTY,  NEVER_REMOVE;
  
  public static RoomRemoveMode fromString(String id)
  {
    return valueOf(id.toUpperCase());
  }
}
