package com.rockitgaming.server.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.entities.manager.IUserManager;
import com.rockitgaming.server.entities.manager.UserManager;
import com.rockitgaming.server.exceptions.MEErrorCode;
import com.rockitgaming.server.exceptions.MEErrorData;
import com.rockitgaming.server.exceptions.MEJoinRoomException;
import com.rockitgaming.server.exceptions.MERoomException;
import com.rockitgaming.server.extensions.ISFSExtension;
import com.rockitgaming.server.model.ServerConfig;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.util.IPlayerIdGenerator;

public class MERoom implements Room {
	private static AtomicInteger autoID = new AtomicInteger(0);
	private int id;
	private String groupId;
	private String name;
	private byte gameId;
	private String password;
	private boolean passwordProtected;
	private int maxUsers;
	private int maxSpectators;
	private User owner;
	private String host;
	private IUserManager userManager;
	private volatile ISFSExtension extension;
	private boolean dynamic;
	private boolean game;
	private boolean hidden;
	private volatile boolean active;
	private RoomRemoveMode autoRemoveMode;
	private IPlayerIdGenerator playerIdGenerator;
	private final long lifeTime;
	private final Lock switchUserLock;
	private final Map<Object, Object> properties;
	private Set<RoomSettings> flags;
	protected Logger logger;
	private boolean isGameFlagInited = false;


	private static int getNewID() {
		return autoID.getAndIncrement();
	}


	public MERoom(String name) {
		this(name, null);
	}


	public MERoom(String name, Class<?> customPlayerIdGeneratorClass) {
		this.id = getNewID();
		this.name = name;
		this.active = false;

		this.logger = LoggerFactory.getLogger(getClass());
		this.properties = new ConcurrentHashMap<Object, Object>();
		this.userManager = new UserManager();

		this.switchUserLock = new ReentrantLock();
		this.lifeTime = System.currentTimeMillis();
	}


	public int getId() {
		return this.id;
	}


	public String getGroupId() {
		if ((this.groupId != null) && (this.groupId.length() > 0)) {
			return this.groupId;
		}
		return "default";
	}


	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public byte getGameId() {
		return gameId;
	}


	public void setGameId(byte gameId) {
		this.gameId = gameId;
	}


	public String getPassword() {
		return this.password;
	}


	public void setPassword(String password) {
		this.password = password;
		if ((this.password != null) && (this.password.length() > 0)) {
			this.passwordProtected = true;
		} else {
			this.passwordProtected = false;
		}
	}


	public boolean isPasswordProtected() {
		return this.passwordProtected;
	}


	public boolean isPublic() {
		return !this.passwordProtected;
	}


	public int getMaxUsers() {
		return this.maxUsers;
	}


	public void setMaxUsers(int maxUsers) {
		this.maxUsers = maxUsers;
		if ((isGame()) && (this.playerIdGenerator != null)) {
			this.playerIdGenerator.onRoomResize();
		}
	}


	public int getMaxSpectators() {
		return this.maxSpectators;
	}


	public void setMaxSpectators(int maxSpectators) {
		this.maxSpectators = maxSpectators;
	}


	public User getOwner() {
		return this.owner;
	}


	public void setOwner(User owner) {
		this.owner = owner;
	}


	public IUserManager getUserManager() {
		return this.userManager;
	}


	public void setUserManager(IUserManager userManager) {
		this.userManager = userManager;
	}


	public boolean isDynamic() {
		return this.dynamic;
	}


	public void setDynamic(boolean dynamic) {
		this.dynamic = dynamic;
	}


	public boolean isGame() {
		return this.game;
	}


	public void setGame(boolean game, Class<? extends IPlayerIdGenerator> customPlayerIdGeneratorClass) {
		if (this.isGameFlagInited) {
			throw new IllegalStateException(toString() + ", isGame flag cannot be reset");
		}
		this.game = game;
		this.isGameFlagInited = true;
		if (this.game) {
			try {
				this.playerIdGenerator = ((IPlayerIdGenerator) customPlayerIdGeneratorClass.newInstance());

				this.playerIdGenerator.setParentRoom(this);
				this.playerIdGenerator.init();
			} catch (InstantiationException err) {
				this.logger.warn(

				String.format(

				"Cannot instantiate Player ID Generator: %s, Reason: %s -- Room might not function correctly.", new Object[] { customPlayerIdGeneratorClass,
						err }));
			} catch (IllegalAccessException err) {
				this.logger.warn(

				String.format(

				"Illegal Access to Player ID Generator Class: %s, Reason: %s -- Room might not function correctly.", new Object[] {
						customPlayerIdGeneratorClass, err }));
			}
		}
	}


	public void setGame(boolean game) {
		setGame(game, null);
	}


	public boolean isHidden() {
		return this.hidden;
	}


	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean flag) {
		this.active = flag;
	}


	public RoomRemoveMode getAutoRemoveMode() {
		return this.autoRemoveMode;
	}


	public void setAutoRemoveMode(RoomRemoveMode autoRemoveMode) {
		this.autoRemoveMode = autoRemoveMode;
	}


	public List<User> getPlayersList() {
		List<User> playerList = new ArrayList<User>();
		for (User user : this.userManager.getAllUsers()) {
			if (user.isPlayer(this)) {
				playerList.add(user);
			}
		}
		return playerList;
	}


	public Object getProperty(Object key) {
		return this.properties.get(key);
	}


	public RoomSize getSize() {
		int uCount = 0;
		int sCount = 0;
		if (this.game) {
			for (User user : this.userManager.getAllUsers()) {
				if (user.isSpectator(this)) {
					sCount++;
				} else {
					uCount++;
				}
			}
		} else {
			uCount = this.userManager.getUserCount();
		}
		return new RoomSize(uCount, sCount);
	}


	public void removeProperty(Object key) {
		this.properties.remove(key);
	}


	public List<User> getSpectatorsList() {
		List<User> specList = new ArrayList<User>();
		for (User user : this.userManager.getAllUsers()) {
			if (user.isSpectator(this)) {
				specList.add(user);
			}
		}
		return specList;
	}


	public User getUserById(int id) {
		return this.userManager.getUserById(id);
	}


	public User getUserByName(String name) {
		return this.userManager.getUserByName(name);
	}


	public User getUserByPlayerId(int playerId) {
		User user = null;
		for (User u : this.userManager.getAllUsers()) {
			if (u.getPlayerId(this) == playerId) {
				user = u;
				break;
			}
		}
		return user;
	}


	public List<User> getUserList() {
		return this.userManager.getAllUsers();
	}


	public boolean containsProperty(Object key) {
		return this.properties.containsKey(key);
	}


	public int getCapacity() {
		return this.maxUsers + this.maxSpectators;
	}


	public void setCapacity(int maxUser, int maxSpectators) {
		this.maxUsers = maxUser;
		this.maxSpectators = maxSpectators;
	}


	public void setFlags(Set<RoomSettings> settings) {
		this.flags = settings;
	}


	public boolean isFlagSet(RoomSettings flag) {
		return this.flags.contains(flag);
	}


	public void setFlag(RoomSettings flag, boolean state) {
		if (state) {
			this.flags.add(flag);
		} else {
			this.flags.remove(flag);
		}
	}


	public void setProperty(Object key, Object value) {
		this.properties.put(key, value);
	}


	public void destroy() {
	}


	public boolean containsUser(String name) {
		return this.userManager.containsName(name);
	}


	public boolean containsUser(User user) {
		return this.userManager.containsUser(user);
	}


	public void addUser(User user) throws MEJoinRoomException {
		addUser(user, false);
	}


	public void addUser(User user, boolean asSpectator) throws MEJoinRoomException {
		if (this.userManager.containsId(user.getUserId())) {
			String message = String.format("User already joined: %s, Room: %s", new Object[] { user, this });
			MEErrorData data = new MEErrorData(MEErrorCode.JOIN_ALREADY_JOINED);
			data.addParameter(this.name);

			throw new MEJoinRoomException(message, data);
		}
		boolean okToAdd = false;
		synchronized (this) {
			RoomSize roomSize = getSize();
			if ((isGame()) && (asSpectator)) {
				okToAdd = roomSize.getSpectatorCount() < this.maxSpectators;
			} else {
				okToAdd = roomSize.getUserCount() < this.maxUsers;
			}
			if (!okToAdd) {
				String message = String.format("Room is full: %s - Can't add User: %s ", new Object[] { this.name, user });
				MEErrorData data = new MEErrorData(MEErrorCode.JOIN_ROOM_FULL);
				data.addParameter(this.name);

				throw new MEJoinRoomException(message, data);
			}
			this.userManager.addUser(user);
		}
		user.setLastJoinedRoom(this);
		if (isGame()) {
			if (asSpectator) {
				user.setPlayerId(-1, this);
			} else {
				user.setPlayerId(this.playerIdGenerator.getPlayerSlot(), this);
			}
		} else {
			user.setPlayerId(0, this);
		}
	}


	public void removeUser(User user) {
		if (isGame()) {
			this.playerIdGenerator.freePlayerSlot(user.getPlayerId(this));
		}
		this.userManager.removeUser(user);
		user.removeLastJoinedRoom();
	}


	public void switchPlayerToSpectator(User user) throws MERoomException {
		if (!isGame()) {
			MEErrorData errData = new MEErrorData(MEErrorCode.SWITCH_NOT_A_GAME_ROOM);
			errData.addParameter(this.name);

			throw new MERoomException("Not supported in a non-game room", errData);
		}
		if (!this.userManager.containsUser(user)) {
			MEErrorData errData = new MEErrorData(MEErrorCode.SWITCH_NOT_JOINED_IN_ROOM);
			errData.addParameter(this.name);

			throw new MERoomException(String.format("%s is not joined in %s", new Object[] { user, this }));
		}
		if (user.isSpectator(this)) {
			this.logger.warn(String.format("PlayerToSpectator refused, %s is already a spectator in %s", new Object[] { user, this }));
			return;
		}
		// this.switchUserLock.lock();
		try {
			if (getSize().getSpectatorCount() < this.maxSpectators) {
				int currentPlayerId = user.getPlayerId(this);

				user.setPlayerId(-1, this);

				this.playerIdGenerator.freePlayerSlot(currentPlayerId);
			} else {
				MEErrorData errData = new MEErrorData(MEErrorCode.SWITCH_NO_SPECTATOR_SLOTS_AVAILABLE);
				errData.addParameter(this.name);

				throw new MERoomException("All Spectators slots are already occupied!", errData);
			}
		} finally {
			// this.switchUserLock.unlock();
		}
		// this.switchUserLock.unlock();
	}


	public void switchSpectatorToPlayer(User user) throws MERoomException {
		if (!isGame()) {
			MEErrorData errData = new MEErrorData(MEErrorCode.SWITCH_NOT_A_GAME_ROOM);
			errData.addParameter(this.name);

			throw new MERoomException("Not supported in a non-game room", errData);
		}
		if (!this.userManager.containsUser(user)) {
			MEErrorData errData = new MEErrorData(MEErrorCode.SWITCH_NOT_JOINED_IN_ROOM);
			errData.addParameter(this.name);

			throw new MERoomException(String.format("%s is not joined in %s", new Object[] { user, this }));
		}
		if (user.isPlayer(this)) {
			this.logger.warn(String.format("SpectatorToPlayer refused, %s is already a player in %s", new Object[] { user, this }));
			return;
		}
		// this.switchUserLock.lock();
		try {
			if (getSize().getUserCount() < this.maxUsers) {
				user.setPlayerId(this.playerIdGenerator.getPlayerSlot(), this);
			} else {
				MEErrorData errData = new MEErrorData(MEErrorCode.SWITCH_NO_PLAYER_SLOTS_AVAILABLE);
				errData.addParameter(this.name);

				throw new MERoomException("All Player slots are already occupied!", errData);
			}
		} finally {
			// this.switchUserLock.unlock();
		}
		// this.switchUserLock.unlock();
	}


	public long getLifeTime() {
		return System.currentTimeMillis() - this.lifeTime;
	}


	public boolean isEmpty() {
		return this.userManager.getUserCount() == 0;
	}


	public boolean isFull() {
		if (isGame()) {
			return getSize().getUserCount() == this.maxUsers;
		}
		return this.userManager.getUserCount() == this.maxUsers;
	}


	public String toString() {
		return String.format("[ Room: %s, Id: %s, Group: %s, isGame: %s ]",
				new Object[] { this.name, Integer.valueOf(this.id), this.groupId, Boolean.valueOf(this.game) });
	}


	public boolean equals(Object obj) {
		if (!(obj instanceof Room)) {
			return false;
		}
		Room room = (Room) obj;
		boolean isEqual = false;
		if (room.getId() == this.id) {
			isEqual = true;
		}
		return isEqual;
	}


	public ISFSExtension getExtension() {
		return this.extension;
	}


	public void setExtension(ISFSExtension extension) {
		this.extension = extension;
	}


	public String getDump() {
		StringBuilder sb = new StringBuilder("/////////////// Room Dump ////////////////").append("\n");
		sb.append("\tName: ").append(this.name).append("\n").append("\tId: ").append(this.id).append("\n").append("\tGroupId: ").append(this.groupId)
				.append("\n").append("\tPassword: ").append(this.password).append("\n").append("\tOwner: ")
				.append(this.owner == null ? "[[ SERVER ]]" : this.owner.toString()).append("\n").append("\tisDynamic: ").append(this.dynamic).append("\n")
				.append("\tisGame: ").append(this.game).append("\n").append("\tisHidden: ").append(this.hidden).append("\n").append("\tsize: ")
				.append(getSize()).append("\n").append("\tMaxUser: ").append(this.maxUsers).append("\n").append("\tMaxSpect: ").append(this.maxSpectators)
				.append("\n").append("\tMaxVars: ").append("\tRemoveMode: ").append(this.autoRemoveMode).append("\n").append("\tPlayerIdGen: ")
				.append(this.playerIdGenerator).append("\n").append("\tSettings: ").append("\n");
		for (RoomSettings setting : RoomSettings.values()) {
			sb.append("\t\t").append(setting).append(": ").append(this.flags.contains(setting)).append("\n");
		}

		if (this.properties.size() > 0) {
			sb.append("\tProperties: ").append("\n");
			for (Object key : this.properties.keySet()) {
				sb.append("\t\t").append(key).append(": ").append(this.properties.get(key)).append("\n");
			}
		}
		if (this.extension != null) {
			sb.append("\tExtension: ").append("\n");

			sb.append("\t\t").append("Name: ").append(this.extension.getName()).append("\n");
			sb.append("\t\t").append("Class: ").append(this.extension.getExtensionFileName()).append("\n");
			sb.append("\t\t").append("Props: ").append(this.extension.getPropertiesFileName()).append("\n");
		}
		sb.append("/////////////// End Dump /////////////////").append("\n");

		return sb.toString();
	}


	public String getPlayerIdGeneratorClassName() {
		return this.playerIdGenerator.getClass().getName();
	}


	@Override
	public String getHost() {
		return ServerConfig.getServerHost();
	}


	public void setProperties(Map<Object, Object> props) {
		this.properties.clear();
		this.properties.putAll(props);
	}


	private void instantiateRoomIdGenerator() {
		String className = getPlayerIdGeneratorClassName();
		if (className == null) {
			className = "com.me.netty.core.util.DefaultPlayerIdGenerator";
		}
		try {
			Class<?> theClass = Class.forName(className);
			this.playerIdGenerator = ((IPlayerIdGenerator) theClass.newInstance());
		} catch (Exception e) {
			this.logger.error("Could not instantiate the IPlayerIdGenerator object. Room: " + this + ", class: " + className + ", err: " + e);
		}
	}


	private void populateTransientFields() {
		this.logger = LoggerFactory.getLogger(getClass());

		this.extension = null;
	}

}
