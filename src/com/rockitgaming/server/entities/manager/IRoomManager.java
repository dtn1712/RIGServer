package com.rockitgaming.server.entities.manager;

import java.util.List;

import com.rockitgaming.server.entities.CreateRoomSettings;
import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.exceptions.MECreateRoomException;
import com.rockitgaming.server.exceptions.MERoomException;
import com.rockitgaming.server.model.RoomPage;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.util.IPlayerIdGenerator;
import com.rockitgaming.server.services.IClusterService;

public abstract interface IRoomManager

{
	public abstract void addRoom(Room room);


	public abstract Room createRoom(CreateRoomSettings roomSetting) throws MECreateRoomException;


	public abstract Room createRoom(CreateRoomSettings roomSetting, User user) throws MECreateRoomException;


	public abstract List<String> getGroups();


	public abstract void addGroup(String groupName);


	public abstract void removeGroup(String groupName);


	public abstract boolean containsGroup(String groupName);


	public abstract boolean containsRoom(int roomId);


	public abstract boolean containsRoom(String roomName);


	public abstract boolean containsRoom(Room room);


	public abstract boolean containsRoom(int roomId, String roomName);


	public abstract boolean containsRoom(String name, String groupId);


	public abstract boolean containsRoom(Room room, String groupId);


	public abstract Room getRoomById(int roomId);


	public abstract Room getRoomByName(String name);


	public abstract List<Room> getRoomList();


	public abstract RoomPage<Room> getRoomPage(int page, boolean getClusterRoom);


	public abstract List<Room> getRoomListFromGroup(String groupName);


	public abstract int getGameRoomCount();


	public abstract int getTotalRoomCount();


	public abstract void setDefaultRoomPlayerIdGeneratorClass(Class<? extends IPlayerIdGenerator> playerGeneratorId);


	public abstract Class<? extends IPlayerIdGenerator> getDefaultRoomPlayerIdGenerator();


	public abstract void checkAndRemove(Room room);


	public abstract void removeRoom(Room room);


	public abstract void removeRoom(int roomId);


	public abstract void removeRoom(String roomName);


	public abstract void removeUser(User user);


	public abstract void removeUser(User user, Room room);


	public abstract void changeRoomName(Room room, String roomName) throws MERoomException;


	public abstract void changeRoomPasswordState(Room room, String password);


	public abstract void changeRoomCapacity(Room room, int newMaxUsers, int newMaxSpect);


	public abstract void setClusterService(IClusterService clusterService);
}
