package com.rockitgaming.server.entities.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.netty.core.gate.wood.User;

public final class UserManager implements IUserManager {
	private final ConcurrentMap<String, User> usersByName;
	private final ConcurrentMap<Integer, User> usersById;
	private Logger logger;


	public UserManager() {
		this.logger = LoggerFactory.getLogger(getClass());
		this.usersByName = new ConcurrentHashMap<String, User>();
		this.usersById = new ConcurrentHashMap<Integer, User>();

	}


	public void addUser(User user) {
		if (containsId(user.getUserId())) {
			throw new RuntimeException("Can't add User: " + user.getUserName() + " - Already exists in Room: ");
		}
		this.usersById.put(Integer.valueOf(user.getUserId()), user);
		this.usersByName.put(user.getUserName(), user);
	}


	public User getUserById(int id) {
		return (User) this.usersById.get(Integer.valueOf(id));
	}


	public User getUserByName(String name) {
		return (User) this.usersByName.get(name);
	}


	public void removeUser(int userId) {
		User user = (User) this.usersById.get(Integer.valueOf(userId));
		if (user == null) {
			this.logger.warn("Can't remove user with ID: " + userId + ". User was not found.");
		} else {
			removeUser(user);
		}
	}


	public void removeUser(String name) {
		User user = (User) this.usersByName.get(name);
		if (user == null) {
			this.logger.warn("Can't remove user with name: " + name + ". User was not found.");
		} else {
			removeUser(user);
		}
	}


	public void removeUser(User user) {
		this.usersById.remove(Integer.valueOf(user.getUserId()));
		this.usersByName.remove(user.getUserName());
	}


	public boolean containsId(int userId) {
		return this.usersById.containsKey(Integer.valueOf(userId));
	}


	public boolean containsName(String name) {
		return this.usersByName.containsKey(name);
	}


	public boolean containsUser(User user) {
		return this.usersById.containsValue(user);
	}


	public List<User> getAllUsers() {
		return new ArrayList<User>(this.usersById.values());
	}


	public Collection<User> getDirectUserList() {
		return Collections.unmodifiableCollection(this.usersById.values());
	}


	public int getUserCount() {
		return this.usersById.values().size();
	}


	public void disconnectUser(int userId) {
		User user = (User) this.usersById.get(Integer.valueOf(userId));
		if (user == null) {
			this.logger.warn("Can't disconnect user with id: " + userId + ". User was not found.");
		} else {
			disconnectUser(user);
		}
	}


	public void disconnectUser(String name) {
		User user = (User) this.usersByName.get(name);
		if (user == null) {
			this.logger.warn("Can't disconnect user with name: " + name + ". User was not found.");
		} else {
			disconnectUser(user);
		}
	}


	public void disconnectUser(User user) {
		removeUser(user);
	}

}
