package com.rockitgaming.server.entities.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.rockitgaming.server.entities.CreateRoomSettings;
import com.rockitgaming.server.entities.CreateRoomSettings.RoomExtensionSettings;
import com.rockitgaming.server.entities.GameSettings;
import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.entities.RoomRemoveMode;
import com.rockitgaming.server.exceptions.MEException;
import com.rockitgaming.server.exceptions.MERuntimeException;
import com.rockitgaming.server.gamelib.key.IwinMessageExchange.IwinKey;
import com.rockitgaming.server.gamelib.model.RoomInfo;
import com.rockitgaming.server.netty.core.socket.SocketServer;
import com.rockitgaming.server.netty.core.util.Reporter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class GameManager {

	private static GameManager instance = null;
	private SocketServer socketServer;
	private final Map<Byte, GameSettings> gameSettings;
	private final Map<Byte, Room> lobbyByGameId;
	
	
	public static GameManager getInstance() {
		if (instance == null) {
			instance = new GameManager();
		}
		return instance;
	}


	private GameManager() {
		socketServer = SocketServer.getInstance();
		gameSettings = new ConcurrentHashMap<>();
		lobbyByGameId = new ConcurrentHashMap<>();
		initializeGames();
	}


	private synchronized void initializeGames() {
		try {
			Room room;
			RoomInfo roomInfo;
			CreateRoomSettings roomSetting;
			List<GameSettings> gameSettings = loadZonesConfiguration();
			for (GameSettings settings : gameSettings) {
				// create room lobby
				settings.lobby.setExtension(settings.extension);
				room = socketServer.getAPIManager().getMEApi().createRoom(settings.lobby, null);
				addLobbyGame(settings.gameId, room);
				// create room default
				for (int i = 0; i < settings.quantityRoom; i++) {
					roomSetting = new CreateRoomSettings();
					roomSetting.setName(settings.gameNameEn + "_" + i);
					roomSetting.setGroupId(settings.groupId);
					roomSetting.setMaxUsers(settings.maxUserInGame);
					roomSetting.setMaxSpectators(settings.maxSpectatorInGame);
					roomSetting.setDynamic(true);
					roomSetting.setGame(true);
					roomSetting.setHidden(false);
					roomSetting.setAutoRemoveMode(RoomRemoveMode.NEVER_REMOVE);
					roomSetting.setExtension(settings.extension);
					roomSetting.setGameId(settings.gameId);
					room = socketServer.getAPIManager().getMEApi().createRoom(roomSetting, null);
					roomInfo = new RoomInfo(settings.gameId);
					// FIXME lấy theo config tiền cược cho phòng
					roomInfo.setBetCoin(10);
					roomInfo.setTimeOut(30);
					room.setProperty(IwinKey.ROOM_INFO, roomInfo);
				}
				
				addGameSettings(settings);
			}
		} catch (MEException e) {
			Reporter.getErrorLogger().error("Init games failed, " + e.toString());
		}
	}


	public synchronized List<GameSettings> loadZonesConfiguration() throws MEException {
		List<GameSettings> gameSettings = new ArrayList<GameSettings>();
		List<File> zoneDefinitionFiles = getZoneDefinitionFiles("gameconfig/");
		for (File file : zoneDefinitionFiles) {
			try {
				FileInputStream inStream = new FileInputStream(file);
				Reporter.infor("Loading: " + file.toString());
				gameSettings.add((GameSettings) getZonesXStreamDefinitions().fromXML(inStream));
			} catch (FileNotFoundException e) {
				throw new MERuntimeException("Could not locate Zone definition file: " + file.getAbsolutePath());
			}
		}
		return gameSettings;
	}


	private List<File> getZoneDefinitionFiles(String path) throws MEException {
		List<File> files = new ArrayList<File>();
		File currDir = new File(path);
		if (currDir.isDirectory()) {
			for (File f : currDir.listFiles()) {
				if (f.getName().endsWith(".xml")) {
					files.add(f);
				}
			}
		} else {
			throw new MEException("Invalid zones definition folder: " + currDir);
		}
		return files;
	}


	private XStream getZonesXStreamDefinitions() {
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("game", GameSettings.class);
		xstream.alias("extension", RoomExtensionSettings.class);
		xstream.alias("lobby", CreateRoomSettings.class);
		return xstream;
	}
	
	public GameSettings getGameSettings(byte gameId) {
		return gameSettings.get(gameId);
	}
	
	public void addGameSettings(GameSettings settings) {
		gameSettings.put(settings.gameId, settings);
	}
	
	public List<GameSettings> getAllGameSettings() {
		return new ArrayList<>(gameSettings.values());
	}
	
	public void addLobbyGame(byte gameId, Room room) {
		if (lobbyByGameId.get(Byte.valueOf(gameId)) == null) {
			lobbyByGameId.put(Byte.valueOf(gameId), room);
		} else {
			//config có nhiều hơn 1 lobby
			Reporter.getErrorLogger().info("Lobby already existed, gameId: " + gameId );
		}
	}
	
	public Room getLobbyGame(byte gameId) {
		return lobbyByGameId.get(Byte.valueOf(gameId));
	}
	
	public List<Room> getAllRoomGame(byte gameId) {
		GameSettings settings = gameSettings.get(Byte.valueOf(gameId));
		if (settings != null) {
			return socketServer.getRoomManager().getRoomListFromGroup(settings.groupId);
		}
		return new ArrayList<>();
	}


	public List<Room> getAvailableRooms(byte gameId, long userMoney) {
		//TODO lấy room theo tiền của user
		List<Room> rooms = getAllRoomGame(gameId);
		if (!rooms.isEmpty()) {
			Iterator<Room> roomsIterator = rooms.iterator();
			Room room;
			while (roomsIterator.hasNext()) {
				room = roomsIterator.next();
				if (!room.isGame()) {
					roomsIterator.remove();
				}
			}
		}
		return rooms;
	}


	public boolean isGameExtRunning(Byte gameId) {
		// TODO check game extension còn chạy hay không
		return true;
	}

}
