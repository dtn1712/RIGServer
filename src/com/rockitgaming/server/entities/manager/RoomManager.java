package com.rockitgaming.server.entities.manager;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.entities.CreateRoomSettings;
import com.rockitgaming.server.entities.MERoom;
import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.entities.RoomSettings;
import com.rockitgaming.server.exceptions.MECreateRoomException;
import com.rockitgaming.server.exceptions.MEErrorCode;
import com.rockitgaming.server.exceptions.MEErrorData;
import com.rockitgaming.server.exceptions.MEExtensionException;
import com.rockitgaming.server.exceptions.MERoomException;
import com.rockitgaming.server.exceptions.MERuntimeException;
import com.rockitgaming.server.extensions.ExtensionLevel;
import com.rockitgaming.server.extensions.ExtensionSettings;
import com.rockitgaming.server.extensions.ExtensionType;
import com.rockitgaming.server.extensions.ISFSExtension;
import com.rockitgaming.server.model.RoomPage;
import com.rockitgaming.server.model.cluster.ClusterRoom;
import com.rockitgaming.server.netty.core.gate.wood.User;
import com.rockitgaming.server.netty.core.socket.SocketServer;
import com.rockitgaming.server.netty.core.util.DefaultPlayerIdGenerator;
import com.rockitgaming.server.netty.core.util.IPlayerIdGenerator;
import com.rockitgaming.server.services.IClusterService;

public final class RoomManager implements IRoomManager {
	private final Map<Integer, Room> roomsById;
	private final Map<String, Room> roomsByName;
	private final Map<String, List<Room>> roomsByGroup;
	private final List<String> groups;
	private final AtomicInteger gameRoomCounter;
	private Logger logger;
	private SocketServer sfs;
	private IClusterService clusterService;

	private Class<? extends IPlayerIdGenerator> playerIdGeneratorClass = DefaultPlayerIdGenerator.class;


	public RoomManager(SocketServer socketServer) {
		// this.sfs = SocketServer.getInstance();
		this.sfs = socketServer; // cheat để có thể gọi được
		this.logger = LoggerFactory.getLogger(getClass());

		this.roomsById = new ConcurrentHashMap<Integer, Room>();
		this.roomsByName = new ConcurrentHashMap<String, Room>();
		this.roomsByGroup = new ConcurrentHashMap<String, List<Room>>();
		this.groups = new ArrayList<String>();
		this.gameRoomCounter = new AtomicInteger();
	}


	public Room createRoom(CreateRoomSettings params) throws MECreateRoomException {
		return createRoom(params, null);
	}


	public Room createRoom(CreateRoomSettings roomSetting, User owner) throws MECreateRoomException {
		String roomName = roomSetting.getName();
		try {
			validateRoomName(roomName);
		} catch (MERoomException roomExc) {
			throw new MECreateRoomException(roomExc.getMessage(), roomExc.getErrorData());
		}
		MERoom newRoom = new MERoom(roomName);
		newRoom.setGameId(roomSetting.getGameId());
		newRoom.setGroupId(roomSetting.getGroupId());
		newRoom.setPassword(roomSetting.getPassword());
		newRoom.setDynamic(roomSetting.isDynamic());
		newRoom.setHidden(roomSetting.isHidden());
		newRoom.setMaxUsers(roomSetting.getMaxUsers());
		if (roomSetting.isGame()) {
			newRoom.setMaxSpectators(roomSetting.getMaxSpectators());
		} else {
			newRoom.setMaxSpectators(0);
		}
		newRoom.setGame(

		roomSetting.isGame(),
				roomSetting.getCustomPlayerIdGeneratorClass() == null ? this.playerIdGeneratorClass : roomSetting.getCustomPlayerIdGeneratorClass());

		Set<RoomSettings> roomSettings = roomSetting.getRoomSettings();
		if (roomSettings == null) {

			roomSettings = EnumSet.of(

			RoomSettings.USER_ENTER_EVENT, RoomSettings.USER_EXIT_EVENT, RoomSettings.USER_COUNT_CHANGE_EVENT, RoomSettings.PUBLIC_MESSAGES);

		}

		newRoom.setFlags(roomSettings);
		newRoom.setOwner(owner);
		newRoom.setAutoRemoveMode(roomSetting.getAutoRemoveMode());
		if (roomSetting.getRoomProperties() != null) {
			((MERoom) newRoom).setProperties(roomSetting.getRoomProperties());
		}
		addRoom(newRoom);

		newRoom.setActive(true);
		if ((roomSetting.getExtension() != null) && (roomSetting.getExtension().getFileName() != null)
				&& (roomSetting.getExtension().getFileName().length() > 0)) {
			try {
				createRoomExtension(newRoom, roomSetting.getExtension());
			} catch (MEExtensionException e) {
				this.logger.warn("Failure while creating room extension!!!, " + e.toString());
			}
		}
		if (newRoom.isGame()) {
			this.gameRoomCounter.incrementAndGet();
		}
		this.logger.info(

		String.format(

		"Room created:  %s", new Object[] { newRoom.toString() }));

		return newRoom;
	}


	private void createRoomExtension(Room room, CreateRoomSettings.RoomExtensionSettings roomExtSetting) throws MEExtensionException {
		if (roomExtSetting == null) {
			return;
		}

		String className = roomExtSetting.getClassName();
		ExtensionSettings extSettings = new ExtensionSettings();
		extSettings.name = roomExtSetting.getFileName();
		extSettings.file = className;
		extSettings.propertiesFile = roomExtSetting.getPropertiesFile();
		extSettings.reloadMode = "AUTO";
		extSettings.type = (ExtensionType.JAVA).toString();
		this.sfs.getExtensionManager().createExtension(extSettings, ExtensionLevel.ROOM, room);
	}


	public Class<? extends IPlayerIdGenerator> getDefaultRoomPlayerIdGenerator() {
		return this.playerIdGeneratorClass;
	}


	public void setDefaultRoomPlayerIdGeneratorClass(Class<? extends IPlayerIdGenerator> customIdGeneratorClass) {
		this.playerIdGeneratorClass = customIdGeneratorClass;
	}


	public void addGroup(String groupId) {
		synchronized (this.groups) {
			this.groups.add(groupId);
		}
	}


	public void addRoom(Room room) {
		this.roomsById.put(Integer.valueOf(room.getId()), room);
		this.roomsByName.put(room.getName(), room);
		synchronized (this.groups) {
			if (!this.groups.contains(room.getGroupId())) {
				this.groups.add(room.getGroupId());
			}
		}

		if (room.isGame())
			clusterService.saveRoom(new ClusterRoom(room));
		addRoomToGroup(room);
	}


	public boolean containsGroup(String groupId) {
		boolean flag = false;
		synchronized (this.groups) {
			flag = this.groups.contains(groupId);
		}
		return flag;
	}


	public List<String> getGroups() {
		List<String> groupsCopy = null;
		synchronized (this.groups) {
			groupsCopy = new ArrayList<String>(this.groups);
		}
		return groupsCopy;
	}


	public Room getRoomById(int id) {
		return this.roomsById.get(Integer.valueOf(id));
	}


	public Room getRoomByName(String name) {
		return this.roomsByName.get(name);
	}


	public List<Room> getRoomList() {
		return new ArrayList<Room>(this.roomsById.values());
	}


	@Override
	public RoomPage<Room> getRoomPage(int nextIndex, boolean getClusterRoom) {
		List<Room> roomList = getRoomList();
		// TODO filter room list
		if (roomList != null) {
			RoomPage<Room> roomPage = new RoomPage<Room>();
			roomPage.setRooms(roomList);
			roomPage.setNextPage(roomList.size());
			roomPage.setGetOnCluster(false);
			return roomPage;
		}

		return null;
	}


	public List<Room> getRoomListFromGroup(String groupId) {
		List<Room> roomList = this.roomsByGroup.get(groupId);
		List<Room> copyOfRoomList = null;
		if (roomList != null) {
			synchronized (roomList) {
				copyOfRoomList = new ArrayList<Room>(roomList);
			}
		}
		return copyOfRoomList;
	}


	public int getGameRoomCount() {
		return this.gameRoomCounter.get();
	}


	public int getTotalRoomCount() {
		return this.roomsById.size();
	}


	public void removeGroup(String groupId) {
		synchronized (this.groups) {
			this.groups.remove(groupId);
		}
	}


	public void removeRoom(int roomId) {
		Room room = this.roomsById.get(Integer.valueOf(roomId));
		if (room == null) {
			this.logger.warn("Can't remove requested room. ID = " + roomId + ". Room was not found.");
		} else {
			removeRoom(room);
			clusterService.removeRoom(ClusterRoom.getClusterRoomId(room));
		}
	}


	public void removeRoom(String name) {
		Room room = this.roomsByName.get(name);
		if (room == null) {
			this.logger.warn("Can't remove requested room. Name = " + name + ". Room was not found.");
		} else {
			removeRoom(room);
		}
	}


	public void removeRoom(Room room) {
		boolean wasRemoved;
		try {
			ISFSExtension roomExtension = room.getExtension();
			if (roomExtension != null) {
				this.sfs.getExtensionManager().destroyExtension(roomExtension);
			}
		} finally {
			room.destroy();
			room.setActive(false);

			wasRemoved = this.roomsById.remove(Integer.valueOf(room.getId())) != null;
			this.roomsByName.remove(room.getName());
			removeRoomFromGroup(room);
			if ((wasRemoved) && (room.isGame())) {
				this.gameRoomCounter.decrementAndGet();
			}
			this.logger.info(

			String.format(

			"Room removed: %s, Duration: %s", new Object[] { room.toString(), Long.valueOf(room.getLifeTime()) }));
		}
	}


	public boolean containsRoom(int id, String groupId) {
		Room room = (Room) this.roomsById.get(Integer.valueOf(id));
		return isRoomContainedInGroup(room, groupId);
	}


	public boolean containsRoom(int id) {
		return this.roomsById.containsKey(Integer.valueOf(id));
	}


	public boolean containsRoom(Room room, String groupId) {
		return isRoomContainedInGroup(room, groupId);
	}


	public boolean containsRoom(Room room) {
		return this.roomsById.containsValue(room);
	}


	public boolean containsRoom(String name, String groupId) {
		Room room = this.roomsByName.get(name);
		return isRoomContainedInGroup(room, groupId);
	}


	public boolean containsRoom(String name) {
		return this.roomsByName.containsKey(name);
	}


	public void removeUser(User user) {
		removeUser(user, user.getLastJoinedRoom());
	}


	public void removeUser(User user, Room room) {
		try {
			if (room.containsUser(user)) {
				room.removeUser(user);
				this.logger.debug("User: " + user.getUserName() + " removed from Room: " + room.getName());
			} else {
				throw new MERuntimeException("Can't remove user: " + user + ", from: " + room);
			}
		} finally {
			handleAutoRemove(room);
		}
		handleAutoRemove(room);
	}


	public void checkAndRemove(Room room) {
		handleAutoRemove(room);
	}


	public void changeRoomName(Room room, String newName) throws MERoomException {
		if (room == null) {
			throw new IllegalArgumentException("Can't change name. Room is Null!");
		}
		validateRoomName(newName);

		String oldName = room.getName();

		room.setName(newName);

		this.roomsByName.put(newName, room);
		this.roomsByName.remove(oldName);
	}


	public void changeRoomPasswordState(Room room, String password) {
		if (room == null) {
			throw new IllegalArgumentException("Can't change password. Room is Null!");
		}
		room.setPassword(password);
	}


	public void changeRoomCapacity(Room room, int newMaxUsers, int newMaxSpect) {
		if (room == null) {
			throw new IllegalArgumentException("Can't change password. Room is Null!");
		}
		if (newMaxUsers > 0) {
			room.setMaxUsers(newMaxUsers);
		}
		if (newMaxSpect >= 0) {
			room.setMaxSpectators(newMaxSpect);
		}
	}


	private void handleAutoRemove(Room room) {
		if ((room.isEmpty()) && (room.isDynamic())) {
			switch (room.getAutoRemoveMode()) {
			case DEFAULT:
				if (room.isGame()) {
					removeWhenEmpty(room);
				} else {
					removeWhenEmptyAndCreatorIsGone(room);
				}
				break;
			case NEVER_REMOVE:
				removeWhenEmpty(room);
				break;
			case WHEN_EMPTY:
				removeWhenEmptyAndCreatorIsGone(room);
			}
		}
	}


	private void removeWhenEmpty(Room room) {
		if (room.isEmpty()) {
			this.sfs.getAPIManager().getMEApi().removeRoom(room);
		}
	}


	private void removeWhenEmptyAndCreatorIsGone(Room room) {
		User owner = room.getOwner();
		if ((owner != null) && (!owner.isConnected())) {
			this.sfs.getAPIManager().getMEApi().removeRoom(room);
		}
	}


	private boolean isRoomContainedInGroup(Room room, String groupId) {
		boolean flag = false;
		if ((room != null) && (room.getGroupId().equals(groupId)) && (containsGroup(groupId))) {
			flag = true;
		}
		return flag;
	}


	private void addRoomToGroup(Room room) {
		String groupId = room.getGroupId();

		List<Room> roomList = (List<Room>) this.roomsByGroup.get(groupId);
		if (roomList == null) {
			roomList = new ArrayList<Room>();
			this.roomsByGroup.put(groupId, roomList);
		}
		synchronized (roomList) {
			roomList.add(room);
		}
	}


	private void removeRoomFromGroup(Room room) {
		List<Room> roomList = this.roomsByGroup.get(room.getGroupId());
		if (roomList != null) {
			synchronized (roomList) {
				roomList.remove(room);
			}
		}
		this.logger.info("Cannot remove room: " + room.getName() + " from it's group: " + room.getGroupId() + ". The group was not found.");
	}


	private void validateRoomName(String roomName) throws MERoomException {
		if (containsRoom(roomName)) {
			MEErrorData errorData = new MEErrorData(MEErrorCode.ROOM_DUPLICATE_NAME);
			errorData.addParameter(roomName);

			String message = String.format("A room with the same name already exists: %s", new Object[] { roomName });
			throw new MERoomException(message, errorData);
		}
		int nameLen = roomName.length();
		int minLen = this.getMinRoomNameChars();
		int maxLen = this.getMaxRoomNameChars();
		if ((nameLen < minLen) || (nameLen > maxLen)) {
			MEErrorData errorData = new MEErrorData(MEErrorCode.ROOM_NAME_BAD_SIZE);
			errorData.addParameter(String.valueOf(minLen));
			errorData.addParameter(String.valueOf(maxLen));
			errorData.addParameter(String.valueOf(nameLen));

			String message = String.format("Room name length is out of valid range. Min: %s, Max: %s, Found: %s (%s)", new Object[] { Integer.valueOf(minLen),
					Integer.valueOf(maxLen), Integer.valueOf(nameLen), roomName });
			throw new MERoomException(message, errorData);
		}
	}


	private int getMaxRoomNameChars() {
		return 20;
	}


	private int getMinRoomNameChars() {
		return 1;
	}


	public void setClusterService(IClusterService clusterService) {
		this.clusterService = clusterService;
	}

}
