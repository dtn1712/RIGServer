package com.rockitgaming.server.entities.manager;

import it.gotoandplay.util.launcher.IClassLoader;
import it.gotoandplay.util.launcher.JarLoader;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rockitgaming.server.entities.Room;
import com.rockitgaming.server.exceptions.MEExtensionException;
import com.rockitgaming.server.extensions.ExtensionLevel;
import com.rockitgaming.server.extensions.ExtensionReloadMode;
import com.rockitgaming.server.extensions.ExtensionSettings;
import com.rockitgaming.server.extensions.ExtensionType;
import com.rockitgaming.server.extensions.ISFSExtension;
import com.rockitgaming.server.netty.core.socket.SocketServer;

public final class ExtensionManager {
	private static final String JAR_EXTENSION = "jar";

	private final ConcurrentMap<Room, ISFSExtension> extensionsByRoom;
	// private final Map<Room, Map<SFSEventType, Set<ISFSEventListener>>> listenersByRoom;
	private final Logger logger;
	private final IClassLoader jarLoader; //TODO chạy lib này bị lỗi
	private SocketServer sfs;
	

	public ExtensionManager() {
		this.logger = LoggerFactory.getLogger(getClass());
		this.jarLoader = new JarLoader();
		this.extensionsByRoom = new ConcurrentHashMap<Room, ISFSExtension>();
		// this.listenersByRoom = new ConcurrentHashMap();
	}


	public void destroyExtension(ISFSExtension roomExtension) {
		// TODO Auto-generated method stub
		
	}


	public void createExtension(ExtensionSettings settings, ExtensionLevel level, Room room) throws MEExtensionException {
		if ((settings.file == null) || (settings.file.length() == 0)) {
			throw new MEExtensionException("Extension file parameter is missing!");
		}
		if ((settings.name == null) || (settings.name.length() == 0)) {
			throw new MEExtensionException("Extension name parameter is missing!");
		}
		if (settings.type == null) {
			throw new MEExtensionException("Extension type was not specified: " + settings.name);
		}
		if (settings.reloadMode == null) {
			settings.reloadMode = "";
		}
		ExtensionReloadMode reloadMode = ExtensionReloadMode.valueOf(settings.reloadMode.toUpperCase());
		if (reloadMode == null) {
			reloadMode = ExtensionReloadMode.MANUAL;
		}
		ExtensionType extensionType = ExtensionType.valueOf(settings.type.toUpperCase());
		ISFSExtension extension;
		if (extensionType == ExtensionType.JAVA) {
			extension = createJavaExtension(settings);
		} else {
			throw new MEExtensionException("Extension type not supported: " + extensionType);
		}
		extension.setName(settings.name);
		extension.setExtensionFileName(settings.file);
		extension.setReloadMode(reloadMode);
		extension.setCurrentRoom(room);
		try {
			if (settings.propertiesFile != null) {
				if ((settings.propertiesFile.startsWith("../")) || (settings.propertiesFile.startsWith("/"))) {
					throw new MEExtensionException("Illegal path for Extension property file. File path outside the extensions/ folder is not valid: " + settings.propertiesFile);
				}
			}
			extension.setPropertiesFileName(settings.propertiesFile);
		} catch (IOException e) {
			throw new MEExtensionException("Unable to load extension properties file: " + settings.propertiesFile);
		}
		
		try {
			extension.init();
			addExtension(extension);
			if (room != null) {
				room.setExtension(extension);
			}
		} catch (Exception err) {
			//ExceptionMessageComposer msg = new ExceptionMessageComposer(err);
			//msg.setDescription("Extension initialization failed.");
			//this.logger.error(msg.toString());
			this.logger.error("Extension initialization failed. ", err.getMessage());
		}
	}


	private ISFSExtension createJavaExtension(ExtensionSettings settings) throws MEExtensionException {
		ISFSExtension extension;
		try {
//			String extensionPath = "extensions/" + settings.name;
//			ClassLoader extensionClassLoader = this.jarLoader.loadClasses(new String[] { extensionPath }, getClass().getClassLoader());
//			Class<?> extensionClass = extensionClassLoader.loadClass(settings.file);
//			if (!ISFSExtension.class.isAssignableFrom(extensionClass)) {
//				throw new MEExtensionException("Extension does not implement ISFSExtension interface: " + settings.name);
//			}
//			extension = (ISFSExtension) extensionClass.newInstance();
			
			File jarFile = new File("extensions/" + settings.name);
            ClassLoader extensionClassLoader = new URLClassLoader(new URL[]{jarFile.toURI().toURL()}, getClass().getClassLoader());
            Class extensionClass = extensionClassLoader.loadClass(settings.file);
            extension = (ISFSExtension) extensionClass.newInstance();
//        } catch (BootException e) {
//			throw new MEExtensionException("Extension boot error. " + e.getMessage());
		} catch (IllegalAccessException e) {
			throw new MEExtensionException("Illegal access while instantiating class: " + settings.file);
		} catch (InstantiationException e) {
			throw new MEExtensionException("Cannot instantiate class: " + settings.file);
		} catch (ClassNotFoundException e) {
			throw new MEExtensionException("Class not found: " + settings.file);
		} catch (MalformedURLException e) {
			throw new MEExtensionException("MalformedURLException: " + e.toString());
		}
		return extension;
	}


	private void addExtension(ISFSExtension extension) {
		this.extensionsByRoom.put(extension.getCurrentRoom(), extension);
	}
	
}
